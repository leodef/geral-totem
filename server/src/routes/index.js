const express = require('express');

const { ModelFactory } = require('../commons/model-factory');
const apiRoutes = require('./api');

const routes = () => {
  const router = express.Router({ mergeParams: true });
  router.use('/api', apiRoutes());
  router.post('/test', function (req, res, next) {
    // const entregaWrapper = ModelFactory.getModel('Entrega');
    // const entregaModel = entregaWrapper.model;
    //##!! test
  });
  router.use('/', function (req, res, next) {
    res.send('default');
  });
  return router;
};

module.exports = routes;
