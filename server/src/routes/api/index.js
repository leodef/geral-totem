const express = require('express');

const moduloRoutes = require('./modulo');
const authRoutes = require('./auth');

const routes = () => {
  const router = express.Router({ mergeParams: true });
  router.use('/modulo', moduloRoutes());
  router.use('/auth', authRoutes());
  router.use('/', function (req, res, next) {
    res.send('api');
  });
  return router;
};

module.exports = routes;
