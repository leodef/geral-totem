const express = require('express');
const passport = require('passport');
const service = require('../../services/auth');
const Auth = require('../../config/auth');
const authTask = Auth.authTask();
/*
router.get('/accountTypes',service.accountTypes.bind(service));

router.post('/confirm-account/:token', service.confirmAccount.bind(service));

router.post('/forgot-password', service.forgotPassword.bind(service));

router.post('/reset-password/:token', service.resetPassword.bind(service));

router.get('/check-username/:username', service.checkUsername.bind(service));
*/
module.exports = function () {
  const router = express.Router();
  router.post(
    '/login',
    service.login.bind(service)
  );
  router.post(
    '/logout',
    [
      authTask,
      service.logout.bind(service)
    ]
  );
  return router;
};
