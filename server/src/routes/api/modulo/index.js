const express = require('express');

const adminRoutes = require('./admin');

const routes = () => {
  const router = express.Router({ mergeParams: true });
  router.use('/admin', adminRoutes());
  router.use('/', function (req, res, next) {
    res.send('modulo');
  });
  return router;
};

module.exports = routes;
