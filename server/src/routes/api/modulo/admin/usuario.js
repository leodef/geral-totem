
const {
  ModuloCrudRouter,
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService,
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../commons/model-factory');
const {
  ModuloUsuarioService
} = require('../../../../services/modulo/admin/usuario');

class UsuarioRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloUsuarioService.getInstance();
  }

  /*
    getPrevTasks() {
      const salt = env.auth.salt;
      return [
        (req, res, next) => {
          if (!req.body || !req.body.senha) {
            next();
            return;
          }
          bcrypt.hash(req.body.senha, salt, (err, encrypted) => {
            req.body.senha = encrypted;
            next();
          });
        },
      ];
    }
  */
}

module.exports = { UsuarioRouter };
