const {
  ModuloCrudRouter,
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloClienteService
} = require('../../../../../services/modulo/admin/empresa/cliente');

class ClienteRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloClienteService.getInstance();
  }
}

module.exports = { ClienteRouter };
