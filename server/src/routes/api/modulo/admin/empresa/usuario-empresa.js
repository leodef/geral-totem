const {
  ModuloCrudRouter,
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloUsuarioEmpresaService,
} = require('../../../../../services/modulo/admin/empresa/usuario-empresa');

class UsuarioEmpresaRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloUsuarioEmpresaService.getInstance();
  }
}

module.exports = { UsuarioEmpresaRouter };
