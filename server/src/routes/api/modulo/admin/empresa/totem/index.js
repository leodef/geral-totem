const {
  ModuloCrudRouter,
} = require('../../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService,
} = require('../../../../../../abstracts/service/modulo/modulo-crud-service');
//require('../../../../../../abstracts/service/modulo/modulo-crud-service');
const { ModelFactory } = require('../../../../../../commons/model-factory');
const { RespostaRouter } = require('./resposta');

class TotemRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    const modelWrapper = ModelFactory.getModel('Totem');
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
      urlFilters: {
        empresa: { _id: 'empresa' },
      },
    });
  }

  loadChild(router) {
    router.use('/:totem/resposta', new RespostaRouter().router());

    return router;
  }
}

module.exports = { TotemRouter };
