const {
  ModuloCrudRouter,
} = require('../../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloRespostaTotemService,
} = require('../../../../../../services/modulo/admin/empresa/totem/resposta-totem');

class RespostaRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloRespostaTotemService.getInstance();
  }
}

module.exports = { RespostaRouter };
