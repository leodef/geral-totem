const {
  ModuloCrudRouter,
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
const {
  ModuloReceitaService,
} = require('../../../../../services/modulo/admin/empresa/receita');

class ReceitaRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloReceitaService.getInstance();
  }
}

module.exports = { ReceitaRouter };
