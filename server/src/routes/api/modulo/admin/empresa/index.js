const {
  ModuloCrudRouter,
} = require('../../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloDocumentCrudService,
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const { ModelFactory } = require('../../../../../commons/model-factory');
const { ReceitaRouter } = require('./receita');
const { ClienteRouter } = require('./cliente');
const { TotemRouter } = require('./totem');
const { UsuarioEmpresaRouter } = require('./usuario-empresa');

class EmpresaRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    const modelWrapper = ModelFactory.getModel('Empresa');
    return ModuloDocumentCrudService.getInstance({
      persistence: modelWrapper,
    });
  }

  loadChild(router) {
    router.use('/:empresa/cliente', new ClienteRouter().router());

    router.use('/:empresa/receita', new ReceitaRouter().router());

    router.use('/:empresa/totem', new TotemRouter().router());

    router.use('/:empresa/usuario', new UsuarioEmpresaRouter().router());

    return router;
  }
}

module.exports = { EmpresaRouter };
