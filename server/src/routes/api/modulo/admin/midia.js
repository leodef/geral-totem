const {
  ModuloCrudRouter,
} = require('../../../../abstracts/router/modulo/modulo-crud-router');
// ../../../../abstracts/router/modulo/modulo-crud-router
const {
  ModuloMidiaService,
} = require('../../../../services/modulo/admin/empresa/midia');

class MidiaRouter extends ModuloCrudRouter {
  modulo() {
    return 'admin';
  }

  service() {
    return ModuloMidiaService.getInstance();
  }

  loadRouter() {
    const router = this._router || this.createRouter();
    const service = this.service();

    //find
    router.get('/', service.fetch.bind(service));
    //byId
    router.get('/:id', service.find.bind(service));
    //find
    router.post('/fetch', service.fetch.bind(service));
    // fetchOptions
    router.post('/fetchOptions', service.fetch.bind(service));
    //delete
    router.delete(
      '/:id',
      service.preRemove.bind(service),
      service.remove.bind(service)
    );
    //create
    router.post(
      '/',
      service.upload.bind(service),
      ModuloMidiaService.getFileBody,
      service.create.bind(service)
    );
    //update
    router.put(
      '/:id',
      service.upload.bind(service),
      ModuloMidiaService.getFileBody,
      service.preUpdate.bind(service),
      service.update.bind(service)
    );

    router.get('/:opcao/:nomeArquivo', (req, res, next) => {
      req.params.download = req.params.opcao === 'download';
      return service.serveFile(req, res, next);
    });

    router.ws('/ws', function(ws, req) {
      ws.on('message', function(msg) {
        
        ws.send(msg);
      });
    });

    return this.loadChild(router);
  }
}

module.exports = { MidiaRouter };
