const express = require('express');

const { EmpresaRouter } = require('./empresa');
const { MidiaRouter } = require('./midia');
const { UsuarioRouter } = require('./usuario');

const routes = () => {
  const router = express.Router({ mergeParams: true });
  router.use('/empresa', new EmpresaRouter().router());
  router.use('/midia', new MidiaRouter().router());
  router.use('/usuario', new UsuarioRouter().router());
  router.use('/', function (req, res, next) {
    res.send('admin');
  });
  return router;
};

module.exports = routes;
