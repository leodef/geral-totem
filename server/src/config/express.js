const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
const expressWs = require('express-ws');
const Strategy = require('passport-http-bearer').Strategy;
//config
const db = require('./database.js');
const env = require('./env');
const logger = require('./logger');
const routes = require('../routes');
const modelFactory = require('./model-factory')
// const date = require('./date');
//##!!
const Auth = require('./auth');
// const Account = require('./account');
const responseHandler = require('./response-handler');

// app.set('title', 'My Site')
// app.get('title')


const loadApp = () => {
  //1- cria o super usuário se não existir
  //Account.init();

  // 2- carrega a estratégia de autentincação
  passport.use(new Strategy(Auth.authenticate));
  //10- Configura uso de autenticação app.use(passport.initialize()); app.use(passport.session()); passport.authenticate('bearer', { session: false })
  
  //3- Cria Express
  const app = express();
  const appExpressWs = expressWs(app);

  // static
  app.use('public', express.static(path.join(__dirname, 'public/')));

  // Variaveis globais
  app.set('env', env);
  app.set('db', db);
  app.set('logger', logger);
  app.set('modelFactory', modelFactory);

  // 4- Insere configurações na aplicação

  app.use(cors(env.corsOptions));
/*
//CORS
  app.options('', (req, res, next) => {
    var headers = {};
    headers['Access-Control-Allow-Origin'] = '';
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS';
    headers['Access-Control-Allow-Credentials'] = false;
    headers['Access-Control-Max-Age'] = '86400'; // 24 hours
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization, cache-control';
    res.writeHead(200, headers);
    res.end();
  });
*/
  //6- Defini estratégia de logs HTTP
  app.use(morgan('tiny', { 'stream': logger.stream }));

  
  //7- Defini estratégia de conversão de requisição
  app.use(bodyParser.urlencoded(env.bodyParser.urlencoded));
  app.use(bodyParser.json(env.bodyParser.json));
  app.use(methodOverride());
  
  //8- realiza o parser do header de cookies da requisição populando req.cookies e armazena o IDdasessão
  app.use(cookieParser());//req.cookie

  //9- cria por padrão a sessão do usuário em memória
  app.use(session({//req.session
    secret: 'a4f8071f-c873-4447-8ee2',
    cookie: { maxAge: 2628000000 },
    resave: true,
    //garante que as informações da sessão serão acessíveis através decookiesacadarequisição
    saveUninitialized: true
  }));

  //11- Proteção da aplicação usando helmet
  app.use(helmet.ieNoOpen());
  app.use(helmet.frameguard());// impede que a pagina seja acessada por xframes
  app.use(helmet.xssFilter());
  app.use(helmet.noSniff());// impede que o browser use scrpts ou links com arquivos que não sejam css ou js
  app.use(helmet.hidePoweredBy({setTo:'PHP 5.5.14'})); // app.disable('x-powerd-by');

  //12- Configuração do retorno em JSON
  //  app.set('json replacer', date.replacer.bind(date)); 
  //  app.set('json spaces', 2);


  //13- Carrega funções comuns
  app.use(responseHandler(app));

  //14- Carrega rotas
  app.use(routes());
  return app;
};

module.exports = loadApp();
