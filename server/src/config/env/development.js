const appRoot = require('app-root-path');

const env = {
  env: 'development', 
  db: process.env.MONGODB_DB_URL || 'mongodb://localhost/receitas',// mongodb://user:password@sample.com:port/dbname
  port: 3000,
  address: 'localhost',
  domain: 'localhost:3000',
  bodyParser: {
    urlencoded: { extended: false, limit: '50mb' },
    json: {limit: '50mb'}
  },
  datetime: {
    format: {
      date: 'YYYY/MM/DD',
      datetime: 'YYYY/MM/DD hh:mm:ss'
    }
  },
  auth: {
    tokenHeaderKey: 'x-auth-token',
    salt: 10
  },
  log: {
    file: 'server.log',
    combined: 'server.log',
    error: 'error.log'
  },
  urlConfig: {
    host: 'https://localhost:4200',
    confirmAccount: 'https://localhost:4200/confirmAccount/:token',
    forgotPassword: 'https://localhost:4200/forgotPassword/:token'
  },
  file: {
    clientImage: process.env.FILE_CONFIG_CLIENT_IMAGE ||
      `${appRoot}/assets/client-image`
  },
  corsOptions: {
    origin: 'http://localhost:8080',//'http://example.com',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    credentials: true,
    exposedHeaders: 'x-auth-token'
  },
  adminAccount: {
    name: 'Admin',
    username: 'admin',
    email: 'admin@admin.com',
    password: 'admin31@admin23'
  },
  email: {
    transport: {
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: 'gkwaway4abclejwv@ethereal.email',
        pass: 'tqaupaVdyqw1rGHgnJ',
      }
    },
    defaultOptions: {
      from: '"Controle de Produção 👻" <no-reply@prime.com>',
      //to: 'bar@example.com, baz@example.com',
      ///subject: 'Hello ✔', 
      //text: 'Hello world?', 
      //html: '<b>Hello world?</b>'
    }
  },
  recognition: {
    default: {
      state: 'default',
      prefix: 'default__'
    }
  }
};
module.exports = env;