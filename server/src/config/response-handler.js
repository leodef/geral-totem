// respnse-handler
const responseHandler = function(app){
  const method = function(req, res, next){
    const env = req.env = res.env = app.get('env');
    const db = req.db = res.db = app.get('db');
    const logger = req.logger = res.logger = app.get('logger');

    res.handleError  = req.handleError = function(req, res, err, status = 500){
      if(!err){return false;}
      const responseErr = {error:true};
      //if(typeof err == 'string'){
      responseErr.title = err.message || err.title || err;
      logger.error(err);
      logger.info(responseErr);
      if(!res.headersSent){
        res.status(status).json(responseErr);
      }
      return true;
    };

    res.handleSuccess = req.handleSuccess = function(req, res, title, description, data = null, status = 200){
      res.status(status).json({
        title: title,
        description: description,
        data:data
      });
    };

    res.handleResponse = req.handleResponse = function(req, res, data, status = 200){
      res.status(status).json(data); 
      /*
        1- JSON.stringify(object, replacer, space )
        2- res.send() 
      */
    };

    res.handleFile = req.handleFile = function(req, res, data, type = 'image/jpeg', status = 200){
      res.writeHead(status, { 'Content-Type': type }); // 
      res.end(data); // Send the file data to the browser.
    };

    res.forbiddenResponse = req.forbiddenResponse = function(req, res, msg){
      if(!msg) {msg = 'Authentication Falied'; }
      req.handleError(req, res, {title: msg}, 401);
      return false;
    };
    
    res.confirmAuth = req.confirmAuth = function(req, res, next){
      if(!req.user){
        req.handleError(req, res, {title: 'Authentication Falied'}, 401);
        return false;
      }
      return true;      
    };

    res.confirmPermission = req.confirmPermission = function(req, res, user){
      if(!req.confirmAuth(req, res, next)){
        return false;
      }
      var userid;
      userid = user._id ? user._id : user;
      userid = user.id ? user.id : user;
      if(userid != req.user._id){
        req.handleError(req, res, {title: 'Wrong Permission'}, 403);
        return false;
      }
      return true;

    };

    res.confirmBody = req.confirmBody = function(req, res, next){
      if(!req.body){
        req.handleError(req, res, {title: 'Empty body'});
        return false;
      }
      return true;      
    };

    res.confirmResult = req.confirmResult = function(req, res, result){
      if(!result){
        req.handleError(req, res, {title: 'Object Not Found'}, 404);
        return false;
      }
      return true;
    };
    
    res.checkUrl = req.checkUrl = function(url){
      if(!url){return false;}
      return url.startsWith(env.urlConfig.host);
    };

    next();
  }
  return method;
}
module.exports = responseHandler;