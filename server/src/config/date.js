const moment = require('moment');
const env = require('./env');

class DateUtils {

    static stringToDate(param){
        return (param)?
                moment(new Date(param)): // moment(param, env.datetime.format.date).toDate():
                moment().toDate();
         
    }

    static stringToDatetime(param){
        return (param)?
                moment(new Date(param)): // moment(param, env.datetime.format.datetime).toDate():
                moment().toDate();
    }

    static dateToString(param){
        return (param)?
            param.toString(): // moment(param).format(env.datetime.format.datetime):
            new Date().toString(); // moment().format(env.datetime.format.datetime);
    }

    static datetimeToString(param){
        return (param)?
                param.toString(): // moment(param).format(env.datetime.format.datetime):
                new Date().toString(); // moment().format(env.datetime.format.datetime);
    }

    static replacer(key, value) {
        if (value instanceof Date) {
          return this.datetimeToString(value);
        }
        return value;
      }

}

const service = new DateUtils();


module.exports = service;