const winston = require('winston');
const config = require('./config');


const createLogger = () => {
    return winston.createLogger(config);
};

const logger = createLogger();
logger.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};

module.exports = logger;
