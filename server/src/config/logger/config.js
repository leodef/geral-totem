
function getLoggerConfig(){
    const  environment = (process.env.NODE_ENV || 'development');
    const env = require(`./config/${environment}.js`);
    return env;
}
module.exports = getLoggerConfig();