
function getEnv(){
    const  environment = (process.env.NODE_ENV || 'development');
    const env = require(`./env/${environment}.js`);
    return env;
}
module.exports = getEnv();