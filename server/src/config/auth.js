const mongoose = require('mongoose');
const moment = require('moment');
const passport = require('passport');
const { ModelFactory } = require('../commons/model-factory');
const { BusinessAuth } = require('./business-auth');

class Auth {

  static validAuthToken(authToken) {
    if (!authToken) { throw 'Token não encontrado' }
    if(moment() >= authToken.expiryDate){
      // Remove token se expirado
      return authToken.remove(function(err, token){
        if(err) {
          throw err;
        }
        throw 'Sessão expirada';
      });
    } else {
      return authToken;
    }
  }

  // return cb(err);   return cb(authTokenValidate.err, false); 
  static authenticate(token, cb) {

    const authTokenWrapper = ModelFactory.getModel('AuthToken');
    const authTokenModel = authTokenWrapper.model;

    // Procura o token
    authTokenModel.findById(token).exec()
    .then((authToken) => {
      authToken = authToken
      return authToken
    })
    .then((authToken) => 
      Auth.validAuthToken(authToken)
    )
    .then((authToken) =>
      BusinessAuth.authenticate(authToken, cb)
      )
    .then((business) => {
      return business;
    })
    .then((business) => cb(null, business))
    .catch(err => cb(err, false));// find account
    
  }// authenticate

  static authTask() {
    return passport.authenticate('bearer', { session: false })
  }
}// Authentication

module.exports = Auth;


/*


  static getAuthAggregation() {
    return [
      // findUsuarioEmpresa
      {
        $lookup: {
          from: usuarioEmpresaModel.collection.name, // root.key, //modelRoot.collectionName,
          localField: `usuario`,
          foreignField: 'usuario',
          as: `usuarioEmpresa`
        }
      },
      { 
        $unwind: {
          path: `usuarioEmpresa`
        }
      },
      // populate Empresa
      {
        $lookup: {
          from: empresaModel.collection.name, // root.key, //modelRoot.collectionName,
          localField: `empresa`,
          foreignField: '_id',
          as: `empresa`
        }
      },
      // Populate Usuario
      {
        $lookup: {
          from: usuarioModel.collection.name, // root.key, //modelRoot.collectionName,
          localField: `usuario`,
          foreignField: '_id',
          as: `usuario`
        }
      },
      {
        $group: { 
            _id : '$usuario._id',
            empresas: { 
                $push: '$empresa'
            }
        }
      }
    ]
  }
*/