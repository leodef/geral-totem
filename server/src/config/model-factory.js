const {ModelFactory, ModelWrapper} = require('../commons/model-factory');
const schemas = require('../schemas');
const mongoose = require('mongoose');
const config = {list: schemas};

const modelFactory = ModelFactory.getInstance(config);

module.exports = modelFactory;