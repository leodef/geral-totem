const mongoose = require('mongoose');
const env = require('./env');
const logger = require('./logger');


const connect = () => {
  mongoose.Promise = require('bluebird');
  /*
  var config = {
  URI: 'mongodb://mongodb-ip/magic',
  OPTIONS: {
    user: 'tarcher',
    pass: 'SUPERSECRETPASSWORD',
    auth: {
      authdb: 'magic'
    }
  }
}

db.init(config.URI, config.OPTIONS);
  */
  const CONFIG = {
    useUnifiedTopology: true,
    useNewUrlParser: true
    //useMongoClient: true
    //user: 'root',
    //pass: 'pass',
  };


  //mongoose.connect('mongodb://user:password@sample.com:port/dbname', { useNewUrlParser: true })
  const client = mongoose.connect(env.db, CONFIG);
  
  if(env.env == 'development'){
    mongoose.set('debug', false); // true 
  } // if(env.env = 'development')
  
  /*
  if(!connection){
    connection = mongoose.createConnection(uri);
  }
  */
  
  mongoose.connection.on('connected', function(){
    logger.info('mongoose connected');
  }); // connected
  
  mongoose.connection.on('disconnected', function(){
    logger.info('mongoose disconnected');
  }); // disconnected
  
  mongoose.connection.on('error', function(err){
    logger.info('mongoose error: '+err);
  }); // error
  
  //close db section when app closed
  process.on('SIGINT', function(){
    mongoose.connection.close(function(){
      logger.info('Mongoose, desconnected by application');
    });
  }); // SIGINT
  
  return { client };
}; // function

module.exports = connect();