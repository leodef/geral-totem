const mongoose = require('mongoose');
const moment = require('moment');
const { ModelFactory } = require('../commons/model-factory');

class BusinessAuth {

  static authenticate(authToken, cb) {
    const {
      usuario
    } = (authToken || {}) 

    const usuarioEmpresaWrapper = ModelFactory.getModel('UsuarioEmpresa');
    const usuarioEmpresaModel = usuarioEmpresaWrapper.model;

    return usuarioEmpresaModel
          .findOne({ usuario: (usuario._id || usuario) })
          .populate(
            'empresa',
            // 'empresa.tipo',
            {
              path: 'usuario',
              select: '_id, usuario, senha, nome, status',
            }
          ).then(result => (result || { usuario })
          ).then(result => ({ ...result, token: authToken }))
  }

}// Authentication

module.exports = { BusinessAuth };


