const fs = require('fs-extra');
const mongoose = require('mongoose');
const { TopologicalSortShortestPaths } = require('js-graph-algorithms');
const { isArguments } = require('lodash');

class ModelFactory {
  constructor(config) {
    this._models = {};
    this._waiting = {};
    this._depedencies = {};
    if (config.list) {
      this.loadList(config.list);
    }
    if (config.folder) {
      //this.loadFolder(config.folder);
    }
  }

  get models() {
    return this.cleanObj(this._models);
  }

  get waiting() {
    return this.cleanObj(this._waiting);
  }

  get depedencies() {
    return this.cleanObj(this._depedencies);
  }

  cleanObj(obj) {
    const result = {};
    for (const index in obj) {
      let val = obj[index];
      if (!!val) {
        val = typeof val === 'object' ? this.cleanObj(val) : val;
        result[index] = val;
      }
    }
    return result;
  }

  static getInstance(config) {
    return (ModelFactory.instance =
      !!ModelFactory.instance && !config
        ? ModelFactory.instance
        : new ModelFactory(config));
  }

  static getModel(model) {
    return !!ModelFactory.instance
      ? ModelFactory.instance.getModel(model)
      : null;
  }

  getModel(model) {
    return this._models[model.key || model];
  }

  loadList(param) {
    for (let key in param) {
      const schema = param[key];
      key = Number.isNaN(key) ? key : schema.key;
      this.loadSchema({ ...schema, key });
    }
  }

  loadSchema(schemaWrapper) {
    const {
      key,
      schemaFunc,
      depedencies,
      depedencieKeys,
      collectionName,
      populate,
    } = schemaWrapper;
    const model = new ModelWrapper(
      key,
      schemaFunc,
      depedencies || depedencieKeys,
      collectionName,
      this,
      populate
    );
    this.loadModel(model);
  }

  loadModel(model) {
    if (!this.checkAndLoadDepedencies(model)) {
      return;
    }
    return this.setModel(model);
  }

  setModel(model) {
    if (this._models[model.key]) {
      return;
    }
    model.init(this);
    this._waiting[model.key] = null;
    this._models[model.key] = model;
    const depedencies = this._depedencies[model.key];
    if (depedencies) {
      for (const key in depedencies) {
        const obj = depedencies[key];
        if (obj) {
          this.loadModel(obj);
        }
      }
    }
    return model;
  }

  checkAndLoadDepedencies(model) {
    let result = true;
    for (const key in model.depedencies) {
      const obj = model.depedencies[key];
      if (!obj) {
        this._depedencies[key] = this._depedencies[key] || {};
        this._depedencies[key][model.key] = model;
        this._waiting[model.key] = model;
        result = false;
      }
    }
    return result;
  }
}

class ModelWrapper {
  constructor(
    key,
    schemaFunc,
    depedencieKeys,
    collectionName,
    factory,
    populate
  ) {
    this.key = key;
    this.schemaFunc = schemaFunc;
    this.depedencieKeys = depedencieKeys || {};
    this.collectionName = collectionName;
    this.factory = factory;
    this.model = null;
    this.schema = null;
    this.populate = populate;
  }

  get depedencies() {
    const obj = {};
    const factory = this.factory;
    (this.depedencieKeys || []).forEach((key) => {
      obj[key] = factory._models[key];
    });
    return obj;
  }

  init() {
    const depedencies = this.depedencies;
    const key = this.key;
    const schema = this.schemaFunc({ ...this, depedencies });
    const populate = (this.populate = this.loadPopulate(this.populate, schema));
    const autoPopulate = this.getAutoPopulate(populate);
    if (autoPopulate) {
      schema.pre('findOne', autoPopulate).pre('find', autoPopulate);
    }
    this.schema = schema;
    const collectionName = this.collectionName;
    try {
      if (!!collectionName) {
        this.model = mongoose.model(key, schema, collectionName);
      } else {
        this.model = mongoose.model(key, schema);
      }
    } catch (err) {
      this.model = mongoose.model(key);
    }
    return this;
  }

  loadPopulate(populate, schema) {
    if (!populate) {
      return null;
    }
    const result = populate.map((obj) => {
      if (typeof obj === 'string') {
        obj = { path: obj };
      }
      if (obj.collectionName) {
        return obj;
      }
      const ref = obj.ref || schema.path(obj.path) ? schema.path(obj.path).options.ref : null;
      const wrapper = this.depedencies[ref];
      const collectionName = wrapper
        ? wrapper.model.collection.collectionName
        : null;
      return {
        ...obj,
        collectionName,
      };
    });

    return result;
  }
  getAutoPopulate(populate) {
    if (!populate) {
      return null;
    }
    return function (next) {
      if (populate) {
        populate.forEach((obj) => {
          try {
            this.populate(obj.path);
          } catch (err) {}
        });
      }
      next();
    };
  }
}

module.exports = {
  ModelFactory,
  ModelWrapper,
};

/*
  Model.find(query, fields, options, callback)// fields and options can be omitted
  Model.find({}, 'first last', function (err, docs)
  Model.count(conditions, callback);
  Model.remove(conditions, callback);
  Model.distinct(field, conditions, callback);
  Model.update(conditions, update, options, callback);

  Model
  .where('age').gte(25)
  .where('tags').in(['movie', 'music', 'art'])
  .select('name', 'age', 'tags')
  .skip(20)
  .limit(10)
  .asc('age')
  .slaveOk()
  .hint({ age: 1, name: 1 })
  .exec(callback);

  Model.$where('this.firstname === this.lastname').exec(callback)

  var query = Model.find({});
  query.where('field', 5);query.limit(5);query.skip(100);
  query.exec(function (err, docs))
*/

/*
  async loadFolder(path){
    const resolvedPath = require.resolve(path);
    const files = await fs.readdir(resolvedPath);
    for(const fileName of files){
      const file = `${path}/${fileName}`;
      const resolvedFile = require.resolve(file);
      const stat = await fs.stat(resolvedFile);
      if(stat.isDirectory()){
        await this.loadFolder(file);
      } else {
        await this.loadSchema(require(file));
      }
    }
    return true;
  }
*/
