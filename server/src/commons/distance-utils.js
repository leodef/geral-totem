const _ = require('lodash');
class DistanceUtils {
  getDistances(distances, node, path) {
    return distances[node].map((distance, node) =>
      _.includes(path, node) ? Infinity : distance
    ); // remove visited nodes
  }

  getCloserNode(distances) {
    let closerDistance = _.min(distances);

    if (closerDistance === Infinity) {
      throw new Error('Path not found');
    }

    return _.findIndex(distances, (x) => x === closerDistance);
  }

  greedyCalculate(startNode, distances) {
    let currentNode = startNode;
    let path = [startNode];

    while (path.length !== distances.length) {
      let distancesFromCurrentNode = this.getDistances(
        distances,
        currentNode,
        path
      );
      let closerNode = this.getCloserNode(distancesFromCurrentNode);

      currentNode = closerNode;
      path.push(closerNode);
    }

    path.push(startNode);

    return this.pathToSolution(path, distances);
  }

  calculateTotalDistance(path, distances) {
    return _(path)
      .initial()
      .map((x, i) => {
        let current = path[i],
          next = path[i + 1];
        return distances[current][next];
      })
      .sum();
  }

  performRotations(sourcePath, rotationSize) {
    let paths = [];
    let max = sourcePath.length - rotationSize;

    for (let i = 1; i < max; i++) {
      let path = _.clone(sourcePath);
      let portion = path.slice(i, i + rotationSize).reverse();

      // inject portion into path
      for (let j = i, k = 0; k < rotationSize; j++, k++) {
        path[j] = portion[k];
      }

      paths.push(path);
    }

    return paths;
  }

  pathToSolution(path, distances) {
    return {
      path: path,
      distance: this.calculateTotalDistance(path, distances),
    };
  }

  optimizeSolution(initialSolution, distances) {
    let bestSolution = initialSolution;
    const mapFunc = (path) => {
      return this.pathToSolution(path, distances);
    };
    for (
      let rotationSize = 2;
      rotationSize < distances.length;
      rotationSize++
    ) {
      let paths = this.performRotations(bestSolution.path, rotationSize);
      let solutions = paths.map(mapFunc);
      let currentBestSolution = _.minBy(solutions, 'distance');

      if (currentBestSolution.distance < bestSolution.distance) {
        bestSolution = currentBestSolution;
      }
    }

    return bestSolution;
  }

  calculate(startNode, distances) {
    let initialSolution = this.greedyCalculate(startNode, distances);
    let finalSolution = this.optimizeSolution(initialSolution, distances);

    return finalSolution;
  }
}

class PathCalculate {
  mountTuples(str) {
    let lines = str
      .trim()
      .split('\n')
      .map((x) => x.trim());
    let tuples = lines.map((x) => x.split(/\s+/));

    return tuples;
  }

  getDistance(x1, y1, x2, y2, dist = 0) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)) + dist;
  }

  mountDistanceMatrix(tuples) {
    let matrix = [];

    tuples.forEach((tuple1, i) => {
      let line = [];

      tuples.forEach((tuple2, j) => {
        if (i === j) {
          line.push(Infinity);
        } else {
          let id1 = tuple1[0],
            x1 = tuple1[1],
            y1 = tuple1[2];
          let x2 = tuple2[1],
            y2 = tuple2[2],
            d = tuple2[3];
          if (!!d) {
            let dX = d[1],
              dY = d[2],
              distDToY = d[3];
            const isDepend = d[0] === id1;
            const isInfinity = distDToY === Infinity;
            // Se a distancia for infinita e for dependente, quer dizer que nao é possivel fazer esta rota
            if (isInfinity && isDepend) {
              line.push(Infinity);
              // Se não for infinito e não for o dependente, quer dizer que precisa passar pelo dependente
            } else if (!isInfinity && !isDepend) {
              line.push(this.getDistance(x1, y1, dX, dY, distDToY));
            } else {
              line.push(this.getDistance(x1, y1, x2, y2));
            }
          } else {
            const dist = this.getDistance(x1, y1, x2, y2);
            line.push(dist);
          }
        }
      });

      matrix.push(line);
    });

    return matrix;
  }

  addNodeNamesTo(result, tuples) {
    let nodeNames = tuples.map((x) => x[0]);
    result.path = result.path.map((x) => nodeNames[x]);
  }

  shorterDistance(value, tuples) {
    const result = tuples
      .map((tuple) => {
        const val = {
          tuple,
          distance: this.getDistance(value[1], value[2], tuple[1], tuple[2]),
        };
        return val;
      })
      .sort((a, b) => {
        return a.distance - b.distance;
      });
    return result;
  }

  generatePath(content) {
    let tuples =
      typeof content === 'string' ? this.mountTuples(content) : content;
    let distances = this.mountDistanceMatrix(tuples);
    let result = new DistanceUtils().calculate(0, distances);

    this.addNodeNamesTo(result, tuples);
    return { path: result.path, distance: result.distance };
  }
}

module.exports = { DistanceUtils, PathCalculate };
