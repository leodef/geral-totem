const Persistence = require('../persistence');
const _ = require('lodash');

class SubDocumentPersistence extends Persistence {

  constructor(
    persistence,
    parentField,
    childPersistence,
    queries) {
    super(
      persistence,
      queries);
    this.parentField = parentField;
    this.childPersistence = childPersistence;
  }

  // subdoc
  create(parentId, query) {
    const { model } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    query = this.id(query);
    // schemas
    const filterQuery = { _id: _parentId };
 
    const updateQuery = { $push: {
      [`${parentField}`]: this.loadItem(query)
    } };
    return model
      .updateOne(
        // findOneAndUpdate
        filterQuery,
        updateQuery,
        { new: true }
      )
      .then((result) => {
        // Retorna o objeto criado
        const lastItemQuery = this.getLastQuery(
          this.getFindQuery(parentId, query),
          parentField
        );
        return model
          .aggregate(lastItemQuery)
          .then((resp) => (Array.isArray(resp) ? resp[0] : resp));
      });
  }

  update(parentId, query, child) {
    child = this.loadItem(child)
    const { model } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    // schemas
    query = this.id(query);
    if (!!query && !!query._id) {
      query._id = this.id(query._id);
    }
    const filterQuery = { 
      _id: _parentId,
      [`${parentField}`]: { $elemMatch: query }
    }; // { _id,: _id, `${field}._id`: id},
    filterQuery;

    const updateSetQuery = {}; // { '$set': { `${field}.$`: body } }

    for (const childKey in child) {
      updateSetQuery[`${parentField}.$.${childKey}`] = child[childKey];
    }
    const updateQuery = { $set: updateSetQuery };

    return model
      .updateOne(
        // findOneAndUpdate
        filterQuery,
        updateQuery,
        { new: true }
      )
      .then((val) =>
        // Retorna o objeto alterado
        this.find(parentId, query)
      );
  }

  remove(parentId, query) {
    const { model } = this.persistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);
    query = this.id(query);

    // schemas
    const filterQuery = { _id: _parentId }; // {_id,: _id, `${field}._id`: id},

    const updateQuery = {
      $pull: {
        [`${parentField}`]: query
      }
    };
    const findObj = this.find(_parentId, query);
    return model
      .updateOne(
        // findOneAndUpdate
        filterQuery,
        updateQuery,
        { new: true }
      )
      .then((resp) => findObj);
  }

  find(parentId, query, unique = true) {
    const { model } = this.persistence;
    const findQuery = this.getFindQuery(parentId, query);
    return model.aggregate(findQuery).then((result) => {
      return unique && Array.isArray(result) ? result[0] : result;
    });
  }

  fetch(parentId, query) {
    return find(parentId, query, false);
  }

  fill(parentId, child = null) {
    const childById = (!!child && !!child._id);
    // aggregate - retorna o resultado da consulta, se o resultado for unico, ou seja, pelo id, e retornado apenas o primeiro resultado
    const fillQuery = this.getFillQuery(parentId, child);
    return this.persistence.model
      .aggregate(fillQuery)
      .then((result) => (childById ? result[0] : result));
  }

  getLastQuery(query, parentField) {
      return [
        ...query,
        { $sort: { 'createdAt': 1 } },
        { $limit: 1 }
      ];
  }

  getFindQuery(parentId, query = null) {
    const parentField = this.parentField;
    const _parentId = this.id(parentId);

    // schemas
    // 1 - matchIdSchema - Encontra o documento pai
    const matchParentIdSchema = ([{ $match: { _id: _parentId } }]);

    // 2 - unwind - Divide os documentos princiais, separando um para cada subdocumento dentro da array
    const unwind = ([{ $unwind: { path: `$${parentField}` } }]);

    // 3 - replaceRoot - Joga o documento do campo filho como documento principal
    const replaceRoot = ([{ $replaceRoot: { newRoot: `$${parentField}` } }]);

    // 4 - matchField - Caso seja passado um filtro para o filho
    let matchChild = ((!!query && !_.isEmpty(query)) ? [{ $match: query }] : []);

    // 5 - populateQuery - Popula campos filhos
    let populateQuery = this.getPopulateQuery();

    // aggregate input - Junta os campos para serem executados em sequencia, retirando os nulos
    return [
      ...matchParentIdSchema,
      ...unwind,
      ...replaceRoot,
      ...matchChild,
      ...populateQuery
    ];
  }

  getFetchQuery(parentId, query = null) {
    return this.getFindQuery(parentId, query);
  }

  getFillQuery(parentId, query = null) {
    const { model } = this.childPersistence;
    const parentField = this.parentField;
    const _parentId = this.id(parentId);

    // schemas
    // 1 - matchIdSchema - Encontra o documento pai
    const matchParentIdSchema = ([{ $match:  { _id: _parentId } }]);

    // 2 - lookupSchema - Encontra os documentos filhos para os ids na array
    const lookupFiledArrayIdsSchema = ([
      {
          $lookup: {
            from: model.collection.name, // root.key, //modelRoot.collectionName,
            localField: `${parentField}`,
            foreignField: '_id',
            as: `${parentField}`
          }
      }]);

    // 3 - unwind - Divide os documentos princiais, separando um para cada subdocumento dentro da array
    const unwind = ([{ $unwind: { path: `$${parentField}` } }]);

    // 4 - replaceRoot - Joga o documento do campo filho como documento principal
    const replaceRoot = ([{ $replaceRoot: { newRoot: `$${parentField}` } }]);

    // 5 - matchField - Caso seja passado um filtro para o filho
    let matchChild = ((!!query && !_.isEmpty(query)) ? [{ $match: query }] : []);

    // 6 - populateQuery - Popula campos filhos
    let populateQuery = this.getPopulateQuery();

    // aggregate input - Junta os campos para serem executados em sequencia, retirando os nulos
    return [
      ...matchParentIdSchema,
      ...lookupFiledArrayIdsSchema,
      ...unwind,
      ...replaceRoot,
      ...matchChild,
      ...populateQuery
    ];
  }
}
module.exports = SubDocumentPersistence;
