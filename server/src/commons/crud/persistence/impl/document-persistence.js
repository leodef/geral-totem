const Persistence = require('../persistence');

class DocumentPersistence extends Persistence {
  constructor(persistence, queries) {
    super(persistence, queries);
  }

  // create
  create(body) {
    const persistence = this.persistence;
    const { model } = persistence;
    return model.create(this.loadItem(body));
  }

  // update
  update(query, body) {
    const persistence = this.persistence;
    const { model } = persistence;
    return model.findOneAndUpdate(query, this.loadItem(body), { new: true });
  }

  // remove
  remove(query) {
    const persistence = this.persistence;
    const { model } = persistence;
    return model.findOneAndDelete(query);
  }

  find(query, unique = true) {
    const persistence = this.persistence;
    const { model } = persistence;
    return (unique ? model.findOne(query) : model.find(query));
  }

  fetch(query) {
    return this.find(query, false);
  }

}
module.exports = DocumentPersistence;
