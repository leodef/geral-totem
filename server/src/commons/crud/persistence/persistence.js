const mongoose = require('mongoose');

class Persistence {
  constructor(persistence, queries) {
    this.persistence = persistence;
    const {
      prevQuery,
      nextQuery,
      filterQuery,
      resolveItem
    } = queries || {};
    this.prevQuery = prevQuery;
    this.nextQuery = nextQuery;
    this.filterQuery = filterQuery;
    this.resolveItem = resolveItem;
  }

  id(val, isId = true) {
    return typeof val === 'string' && isId ? mongoose.Types.ObjectId(val) : val;
  }

  aggregate(query) {
    const persistence = this.persistence;
    const { model, populate } = persistence;
    return model.aggregate([query]);
  }

  loadItem(item) {
    const resolveItem = this.resolveItem;
    item = (resolveItem ? resolveItem(item, this) : item);
    return item;
  }

  loadQuery(query, params, prev = []) {
    const prevQuery = this.getPrevQuery(params);
    const nextQuery = this.getNextQuery(params);
    const populateQuery = this.getPopulateQuery(params);
    return [...prevQuery, ...prev, ...query, ...populateQuery, ...nextQuery];
  }

  getPrevQuery(params) {
    const prevQuery = this.prevQuery;
    return prevQuery ? prevQuery(params, this) || [] : [];
  }

  getNextQuery(params) {
    const nextQuery = this.nextQuery;
    return nextQuery ? nextQuery(params, this) || [] : [];
  }

  getFilterQuery(filter, params, prev = []) {
    const filterQuery = this.filterQuery;
    return (
      (filterQuery
        ? filterQuery(filter, params, prev, this)
        : this.getDefaultFilterQuery(filter, params, prev)) || []
    );
  }

  getDefaultFilterQuery(filter, params, prev = []) {
    /*{ 
      fieldName: urlParamName,
      fieldName2: { _id: urlParamName }
    }*/
    if (!filter) {
      return [...(prev || [])];
    } else {
      const prevQuery = this.getPrevQuery(params);
      const nextQuery = this.getNextQuery(params);

      const filterQuery = Object.keys(filter).reduce((result, fieldName) => {
        const filterValue = filter[fieldName];
        if (!filterValue) {
          return null;
        }
        const isIdParam = filterValue._id;
        const urlParamName = isIdParam ? filterValue._id : filterValue;
        let urlParamValue = this.id(params[urlParamName], isIdParam);
        result[fieldName] = urlParamValue;
        return result;
      }, {});
      return [...(prev || []), { $match: filterQuery }];
    }
  }

  getPopulateQuery(prevQuery = []) {
    return [];
    /*
    const { schema, populate } = this.persistence;
    const query = populate ? populate
      .reduce( (prev, curr) => {
        let { path, collectionName } = curr;
 
        return [
          ...prev,
          {
            $lookup: {
                from: collectionName,
                localField: path,
                foreignField: '_id',
                as: path
              }
            },
            {
              $project: {
                [path]: {
                  $arrayElemAt: [ `$${path}`, 0 ]
                }
              }
            }]
      }, []) : []
    return [
      ...prevQuery,
      ...query
    ]
    */
  }

  /*
  getPopulateQueryFor(params) {
     if(Array.isArray()) {
        return params.map( (param) => this.getPopulateQueryFor(param));
     }
    const {collectionName, field} = params;
    return {
        $lookup: {
          from: model.collection.name, // root.key, //modelRoot.collectionName,
          localField: `${parentField}`,
          foreignField: '_id',
          as: `${parentField}`
        }
    }
  }
  */
}

module.exports = Persistence;
