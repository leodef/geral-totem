const express = require('express');

class CrudRouter {
  constructor(routerConfig) {
    routerConfig = routerConfig || {};
    // por padrao se a nao for passado valor para mergeParams
    // para considerar padroes de outros mapeamentos
    if (!routerConfig.mergeParams && routerConfig.mergeParams !== false) {
      routerConfig.mergeParams = true;
    }
    this.routerConfig = routerConfig;
  }

  // cria o objeto router
  createRouter() {
    return express.Router(this.routerConfig);
  }

  // retorna o router carregado
  router() {
    return this.loadRouter();
  }

  // servico que realiza as funções
  service() {
    return null;
  }

  // inserir sub-endpoints
  loadChild(router) {
    return router;
  }

  getPrevTasks(action = null) {
    return [];
  }

  getNextTasks(action = null) {
    return [];
  }

  getAuthTask(action) {
    return [];
  }

  getArray(val) {
    return Array.isArray(val) ? val : [val];
  }

  getTaks(tasks, action = null) {
    return [
      ...this.getArray(this.getAuthTask(action)),
      ...this.getArray(this.getPrevTasks(action)),
      ...this.getArray(tasks),
      ...this.getArray(this.getNextTasks(action)),
    ];
  }

  // cria endpoints par crud
  loadRouter() {
    const router = this._router || this.createRouter();
    const service = this.service();

    //find
    router.get.apply(router, [
      '/',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    //byId
    router.get.apply(router, [
      '/:id',
      ...this.getTaks(service.find.bind(service), 'find'),
    ]);
    //find
    router.post.apply(router, [
      '/fetch',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    // fetchOptions
    router.post.apply(router, [
      '/fetchOptions',
      ...this.getTaks(service.fetch.bind(service), 'fetch'),
    ]);
    //delete
    router.delete.apply(router, [
      '/:id',
      ...this.getTaks(service.remove.bind(service), 'remove'),
    ]);
    //create
    router.post.apply(router, [
      '/',
      ...this.getTaks(service.create.bind(service), 'create'),
    ]);
    //update
    router.put.apply(router, [
      '/:id',
      ...this.getTaks(service.update.bind(service), 'update'),
    ]);

    return this.loadChild(router);
  }
}

module.exports = CrudRouter;
