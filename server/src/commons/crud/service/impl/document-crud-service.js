const CrudService = require('../crud-service');
const DocumentPersistence = require('../../persistence/impl/document-persistence');
const CollectionUtils = require('../../utils/collection-utils');

class DocumentCrudService extends CrudService {
  constructor(args) {
    super(args);
    const { persistence, ...queries } = args;
    if (persistence) {
      this.documentPersistence = new DocumentPersistence(
        persistence,
        queries
      );
    }
  }

  // create - POST /
  create(req, res, next) {
    req.logger.silly(`${typeof this} - create`);
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      this.createItem(body),
      action
    );
  }

  createItem(body) {
    const documentPersistence = this.documentPersistence;
    return documentPersistence.create(body)
  }

  // update - PUT /:id
  update(req, res, next) {
    req.logger.silly(`${typeof this} - update`);
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      this.updateItem(_id, body),
      action
    );
  }

  updateItem(_id, body) {
    _id = _id || body._id;
    const documentPersistence = this.documentPersistence;
    return documentPersistence.update({ _id }, body);
  }

  // remove - POST /:id
  remove(req, res, next) {
    req.logger.silly(`${typeof this} - remove`);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'remove';
    return this.resolveTask(
      req,
      res,
      next,
      this.removeItem(_id),
      action
    );
  }

  removeItem(_id) {
    const documentPersistence = this.documentPersistence;
    return documentPersistence.remove({ _id });
  }

  // find - GET /:id
  find(req, res, next) {
    req.logger.silly(`${typeof this} - find`);
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');

    const action = 'find';
    return this.resolveTask(
      req,
      res,
      next,
      documentPersistence.find({ _id }),
      action
    );
  }

  // fetch - POST /fetch
  fetch(req, res, next) {
    req.logger.silly(`${typeof this} - all`);
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }
    const { params } = req;
    const filterQuery = documentPersistence.getFilterQuery(
      this.getUrlFilters(),
      params
    );
    const config = CollectionUtils.loadConfig(req);
    const query = documentPersistence.loadQuery(filterQuery, params);
    const collectionQuery = CollectionUtils.getCollectionQuery(config, query);

    const action = 'fetch';
    return this.resolveTask(
      req,
      res,
      next,
      CollectionUtils.loadCollectionTaskResponse(
        documentPersistence.aggregate(collectionQuery)
      ),
      action
    );
  }
}
module.exports = DocumentCrudService;
