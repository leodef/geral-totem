const DocumentPersistence = require('../../persistence/impl/document-persistence');
const SubDocumentPersistence = require('../../persistence/impl/sub-document-persistence');
const CrudService = require('../crud-service');
const CollectionUtils = require('../../utils/collection-utils');

class ReferencedDocumentCrudService extends CrudService {
  constructor(args) {
    super(args);
    const { parent, persistence, parentIdUrlName, ...queries } = args;
    this.parentIdUrlName = parentIdUrlName;
    this.documentPersistence = new DocumentPersistence(persistence);
    this.parentDocumentPersistence = new SubDocumentPersistence(
      parent.persistence,
      parent.field,
      persistence,
      queries
    );
  }

  // Create POST /:parent/
  // Encontra documento pai
  // Encontra o campo do documento pai do tipo lista
  // Insere o documento filho
  // Insere o id no campo do tipo lista do documento pai
  create(req, res, next) {
    req.logger.silly(`${typeof this} - create`);
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);

    // _id
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    if (!user) {
      return;
    }

    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      this.createItem(_parentId, body),
      action
    );
  }

  createItem(_parentId, body) {
    const documentPersistence = this.documentPersistence;
    const parentDocumentPersistence = this.parentDocumentPersistence;
    return documentPersistence.create(body).then((response) => {
      const { _id } = response;
      return parentDocumentPersistence
        .create(_parentId, _id)
        .then((resp) => response);
    })
  }

  // update PUT /:parent/:id - POST /:parent/:id
  // Encontra documento pai
  // Encontra o campo do documento pai do tipo lista
  // Atualiza o documento referente ao id informado
  update(req, res, next) {
    req.logger.silly(`${typeof this} - update`);
    const parentIdUrlName = this.parentIdUrlName;
    const body = this.getReqBody(req);

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      this.updateItem(_parentId, _id, body),
      action
    );
  }

  updateItem(_parentId, _id, body) {
    _id = _id || body._id;
    const documentPersistence = this.documentPersistence;
    return documentPersistence.update({ _id }, body);
  }

  // Detele DELETE /:parent/:id
  // Encontra documento pai
  // Encontra o campo do documento pai do tipo lista
  // Deleta o documento referente ao id informado
  remove(req, res, next) {
    req.logger.silly(`${typeof this} - remove`);
    const parentIdUrlName = this.parentIdUrlName;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'remove';
    return this.resolveTask(
      req,
      res,
      next,
      this.removeItem(_parentId, _id),
      action
    );
  }

  removeItem(_parentId, _id) {
    const documentPersistence = this.documentPersistence;
    const parentDocumentPersistence = this.parentDocumentPersistence;
    return documentPersistence.remove({ _id }).then((response) => {
      if (!response) {
        return response;
      }
      return parentDocumentPersistence.remove(_parentId, _id).then((resp) => {
        return response;
      });
    })
  }

  // find GET /:parent/:id
  // Retorna documento encontrado na lista
  find(req, res, next) {
    req.logger.silly(`${typeof this} - find`);
    const parentIdUrlName = this.parentIdUrlName;
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    const action = 'find';
    return this.resolveTask(
      req,
      res,
      next,
      documentPersistence.find({ _id }),
      action
    );
  }

  // fetch POST /:parent/fetch   - GET /:parent/
  // Encontra documento pai
  // Retorna o campo do documento pai do tipo lista
  fetch(req, res, next) {
    req.logger.silly(`${typeof this} - fetch`);
    const parentIdUrlName = this.parentIdUrlName;
    const documentPersistence = this.documentPersistence;
    const parentDocumentPersistence = this.parentDocumentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }
    const { params } = req;
    // _id
    const _id = this.searchReqParam(req, 'id');
    const _parentId = this.searchReqParam(req, parentIdUrlName);

    // query
    const fetchQuery = parentDocumentPersistence.getFillQuery(_parentId);
    const config = CollectionUtils.loadConfig(req);
    const query = documentPersistence.loadQuery(fetchQuery, params);
    const collectionQuery = CollectionUtils.getCollectionQuery(config, query);

    const action = 'fetch';
    return this.resolveTask(
      req,
      res,
      next,
      CollectionUtils.loadCollectionTaskResponse(
        parentDocumentPersistence.aggregate(collectionQuery)
      ),
      action
    );
  }
}
module.exports = ReferencedDocumentCrudService;
