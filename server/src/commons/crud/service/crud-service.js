class CrudService {
  urlFilters = {};

  constructor(args) {
    const {
      urlFilters,
      loadBody,
      prevCallback,
      nextCallback
    } = (args || {});
    this.urlFilters = (urlFilters || {});
    this.loadBody = loadBody || ((body) => body);
    this.prevCallback = prevCallback;
    this.nextCallback = nextCallback;
  }

  auth(req, res, next) {
    return true;
  }

  // Procura parametro no body, params e query
  searchReqParam(req, fieldName) {
    const { body, params, query } = req;
    let field = body ? body[fieldName] : null;
    field = params ? field || params[fieldName] : field;
    field = query ? field || query[fieldName] : field;
    return field;
  }

  resolveTask(req, res, next, task, action) {
    return this.getPrevCallback(req, res, next, action)
      .then((result) => task)
      .then((result) => this.getNextCallback(req, res, next, action, result))
      .then((result) => req.handleResponse(req, res, result))
      .catch((err) => req.handleError(req, res, err));
  }

  getUrlFilters() {
    /*
      { 
        fieldName: urlParamName,
        fieldName2: { _id: { urlParamName } }
      }
    */
    return this.urlFilters;
  }

  getReqBody(req) {
    const { body, params } = req;
    return this.loadBody(
      this.updateBodyWithUrlFilters(
        body,
        params
      ),
      params
    );
  }

  updateBodyWithUrlFilters(body, params) {
    const urlFilters = this.getUrlFilters();
    Object.keys(urlFilters).forEach((fieldName) => {
      let urlParamName = urlFilters[fieldName];
      urlParamName = urlParamName._id || urlParamName;
      const urlVal = params[urlParamName];
      body[fieldName] = urlVal;
    });
    return body;
  }

  getPrevCallback(req, res, next, action) {
    const prevCallback = this.prevCallback;
    return prevCallback
      ? prevCallback(req, res, next, action)
      : Promise.resolve(action);
  }

  getNextCallback(req, res, next, action, result) {
    const nextCallback = this.nextCallback;
    return nextCallback
      ? nextCallback(req, res, next, action)
      : Promise.resolve(result);
  }
}
module.exports = CrudService;
