const { DistanceUtils, PathCalculate } = require('./distance-utils');
const _ = require('lodash');
const pathCalculate = new PathCalculate();
/*
function CreateModel1WithStuff(data, cb) {
    if (!!data.child && mongoose.Types.ObjectId.isValid(data.child)) { // Save child model first
        data.child = Model2(data.child);
        data.child.save(function(err) {
            cb(err, err ? null : Model1(data));
        });
    } else { // Proceed without dealing with child
        cb(null, Model1(data));
    }
}
*/
class EntregaUtils {
  generateTrajeto(entrega) {
    const filialTransportadora = entrega.filialTransportadora;
    const filiais = {};
    filiais[filialTransportadora._id.toString()] = filialTransportadora;
    /*
         acordo: params.depedencies.Acordo.schema,
    tipo: String,
    filial:  params.depedencies.Filial.schema,
       */
    const acordos = entrega.acordos;
    /* acordos.forEach( acordo => {
               filiais[`${acordo._id.toString()}.remetente`] = acordo.remetente;
               filiais[`${acordo._id.toString()}.destinatario`] = acordo.destinatario;
       });*/
    const path = this.generatePath(entrega);
    const pontos = this.optimize(
      path.map((val, index) => {
        const posicao = index + 1;
        let tipo = 'transportadora';
        let acordo = null;
        let filial = null; //_.get(filiais, val.toString());
        let acordoId = null;
        if (val.indexOf('.') >= 0) {
          const sval = val.split('.');
          acordoId = sval[0].toString();
          tipo = sval[1];
          acordo = acordos.find((a) => a._id.toString() === acordoId);
          filial = acordo[tipo];
        } else {
          filial = filiais[val];
        }
        const localizacao = filial.localizacao;
        const referencia = filial._id.toString();
        return {
          tipo,
          acordo,
          filial,
          localizacao,
          posicao,
          referencia,
          acordoId,
        };
      })
    );

    return { pontos };
  }

  generatePath(entrega) {
    const filialTransportadora = entrega.filialTransportadora;
    const ftLocalizacao = filialTransportadora.localizacao;
    const ftId = `${filialTransportadora._id.toString()}`;
    const ftX = ftLocalizacao.latitude; // 1
    const ftY = ftLocalizacao.longitude;
    const ftPonto = [ftId, ftX, ftY];
    let pontos = [ftPonto];
    // cria pontos
    entrega.acordos.forEach((acordo, index) => {
      pontos = [...pontos, ...this.resumeAcordo(acordo)];
    });
    pontos = pontos.sort((a, b) => {
      const aId = a[0];
      const bId = b[0];
      let aIndex = 0;
      let bIndex = 0;
      if (aId.indexOf('remetente') >= 0) {
        aIndex = 1;
      } else if (aId.indexOf('destinatario') >= 0) {
        aIndex = 2;
      } else {
        aIndex = 0;
      }

      if (bId.indexOf('remetente') >= 0) {
        bIndex = 1;
      } else if (bId.indexOf('destinatario') >= 0) {
        bIndex = 2;
      } else {
        bIndex = 0;
      }
      return aIndex - bIndex;
    });
    const path = pathCalculate.generatePath(pontos).path;
    // {path, distance}
    return path;
  }

  resumeAcordo(acordo) {
    const acordoId = acordo._id.toString();
    const remetente = acordo.remetente;
    const rLocalizacao = remetente.localizacao;
    const destinatario = acordo.destinatario;
    const dLocalizacao = destinatario.localizacao;
    const rId = `${acordoId}.remetente`;
    const rX = rLocalizacao.latitude;
    const rY = rLocalizacao.longitude;
    const dId = `${acordoId}.destinatario`;
    const dX = dLocalizacao.latitude;
    const dY = dLocalizacao.longitude;
    const rDist = pathCalculate.getDistance(rX, rY, dX, dY);
    const dDist = rDist; //Infinity;
    // remetente
    const rResume = [rId, rX, rY, [dId, dX, dY, dDist]];
    // destinatario
    const dResume = [dId, dX, dY, [rId, rX, rY, rDist]];
    return [rResume, dResume];
  }

  optimize(path) {
    const result = _.clone(path);
    for (let ai = 0; ai < path.length; ai++) {
      const a = path[ai];
      for (let bi = 0; bi < path.length; bi++) {
        const b = path[bi];
        if (
          ai > bi &&
          a.acordoId === b.acordoId &&
          a.tipo === 'remetente' &&
          b.tipo === 'destinatario'
        ) {
          result[ai] = b;
          result[bi] = a;
        }
      }
    }
    return result;
  }
}

module.exports = { EntregaUtils };

/*entrega
           filialTransportadora: { type: Schema.Types.ObjectId, ref: 'Filial' },
           transportadora: { type: Schema.Types.ObjectId, ref: 'Empresa' },
           veiculo: { type: Schema.Types.ObjectId, ref: 'Veiculo' },
           trajeto: { type: Schema.Types.ObjectId, ref: 'Trajeto' },
                   pontos: [{ type: Schema.Types.ObjectId, ref: 'PontoTrajeto' }],
                           localizacao: { type: Schema.Types.ObjectId, ref: 'Localizacao' },
                                 endereco: String,
                                 latitude: String,
                                 longitude: String
                           posicao: Number,
                           referencia: {type: Schema.Types.ObjectId}
           acordos: [{ type: Schema.Types.ObjectId, ref: 'Acordo' }],
                   criador: { type: Schema.Types.ObjectId, ref: 'Empresa' },
                   remetente: { type: Schema.Types.ObjectId, ref: 'Filial' },
                   destinatario: { type: Schema.Types.ObjectId, ref: 'Filial' },
                   produto: { type: Schema.Types.ObjectId, ref: 'Produto' },
                   quantidade: params.depedencies.Quantidade.schema
           
           dataFechamento: Date,
           status: String
   */
