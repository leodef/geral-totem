const http = require('http');
const tasks = require('./config/tasks');
const app = require('./config/express');
const logger = require('./config/logger');
/*
  var os = require( 'os' );
  var networkInterfaces = os.networkInterfaces( );
*/
const env = app.get('env');
http.createServer(app).listen(
  env.port, // port
  // hostname
  () => { // callback
  logger.info(`BACKEND is running on port ${env.port}.`);
});
