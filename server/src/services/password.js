
class PasswordService {

    /*

    //confirm-password
    confirmAccount (req, res, next) { //confirm-account/:token.post
        const token = req.params.token;

        accountTokenSchemaWrapper.confirmAccount(token)
        .then(  ( tokenResult ) => {
            if (!req.confirmResult(req, res, tokenResult)) { 
                throw new Error('Token not found');
            }
            const account = tokenResult.account;
            return account.update( {enabled: true} );
        }).then( ( accountResult ) => {
                if (!req.confirmResult(req, res, accountResult)) { 
                    throw new Error('Account not found');
                }
                req.handleSuccess(req, res, 'OK', 'OK');
            })
        .catch( ( err ) => {
            req.handleError(req, res, err);
        });
    }

    //forgot-password
    forgotPassword(req, res, next) { //forgot-password.post
        //body
        if(!req.confirmBody(req, res, next)){return;}
        const body = req.body;
        const url = body.url;
        const email = body.email;

        accountSchemaWrapper.findOne({ email })
        .then( ( account ) => {
            if (!req.confirmResult(req, res, account)) { 
                throw new Error('Account not found');
            }
            return accountTokenSchemaWrapper.createResetPasswordToken(account, url);
        }).then( ( token ) => {
            if (!req.confirmResult(req, res, token)) { 
                throw new Error('Token not found');
            }
            req.handleSuccess(req, res, 'OK', 'OK');
        })
        .catch( ( err ) => {
            req.handleError(req, res, err);
        });
    }

    //reset-password
    resetPassword(req, res, next) {//reset-password:token.post
        //body
        if(!req.confirmBody(req, res, next)){return;}
        const body = req.body;
        const resetPasswordDTO = accountTokenSchemaWrapper.getResetPasswordDTO(body);
        if(resetPasswordDTO.password !== resetPasswordDTO.confirmPassword){
            req.handleError('password and confirmPasword must match', req, res);
            return;
        }

        accountTokenSchemaWrapper.resetPassword(token)
        .then( ( token ) => {
            if (!req.confirmResult(req, res, token)) { 
                throw new Error('Token not found');
            }
            const account = token.account;
            account.password = resetPasswordDTO.password;
            return account.save();
        }).then( ( account ) => {
            if (!req.confirmResult(req, res, account)) { 
                throw new Error('Account not found');
            }
            req.handleSuccess(req, res, 'OK', 'OK');
        })
        .catch ( ( err ) => {
            req.handleError(req, res, err);
        });
    }

    //check-username
    checkUsername(req, res, next) { //check-username/:username.get
        var username = req.params.username || '';
        return accountSchemaWrapper.findOne(
            {username: username})
            .then( ( result ) => {
                if( result ){
                    return {isValid: false};
                }
                return {isValid: true};
        })
        .then( ( result ) => req.handleResponse(req, res, result) )
        .catch ( ( err ) => req.handleError(req, res, err) );
    }
*/
}

  
const service = new PasswordService();
module.exports = service;
