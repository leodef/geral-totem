//frameworks
const _ = require('lodash');
const { ModelFactory } = require('../commons/model-factory');
const passwordUtilsService = require('./password-utils').service;
const authTokenService = require('./auth-token').service;
const businessAuthService = require('./business-auth').service;
var url = require('url');

class AuthService {

  validarUsuario(usr, auth, req, res, next) {
    const { password } = auth
    if (!usr) {
      req.forbiddenResponse(req, res, 'Usuário inválido');
      throw null;
    }

    // check password
    if (!passwordUtilsService.comparePassword(password, usr.senha)) {
      req.forbiddenResponse(req, res, 'Senha inválida');
      throw null;
    }
    return usr;
  }

  validarAuthToken(obj, req, res, next) {
    if (!req.confirmResult(req, res, obj)) {
      throw new Error('Token não criado');
    }
    return obj;
  }

  // API
  //login
  login(req, res, next) {
    //login.post
    //schemas
    const usuarioWrapper = ModelFactory.getModel('Usuario');
    const usuarioModel = usuarioWrapper.model;

    const authTokenWrapper = ModelFactory.getModel('AuthToken');
    const authTokenModel = authTokenWrapper.model;

    const auth = this.getLoginParams(req);
    const { username } = auth
    let usuario = null;
    let authToken = null;
    let business = null;

    usuarioModel
      .findOne({ $or: [{ usuario: username }] })
      // Validate auth infos
      .then((usr) => {
        // check username
        usuario = this.validarUsuario(
          usr,
          auth,
          req,
          res,
          next) 
      })
      // Create auth token
      .then((obj) => {
        // usuario, validade
        const autheTokenDTO = authTokenService.getAuthTokenDTO(usuario);
        // cria ou atualiza o token
        return authTokenModel
          .findOneAndUpdate(
            { usuario: (usuario._id || usuario) }, // id
            autheTokenDTO,
            { upsert: true, new: true, runValidators: true,  }
          ).populate('usuario');
      })
      // Confirm token was created
      .then((obj) => {
        authToken = this.validarAuthToken(obj, req, res, next);
      })
      // Create business object
      .then((obj) =>
        businessAuthService.login(
          authToken,
          req,
          res,
          next)
      )
      .then((obj) => {
        business = obj
      })
      // set token response
      .then((obj) => {
        res.setHeader(req.env.auth.tokenHeaderKey, authToken.id);
        res.handleResponse(req, res, business);
        return;
      })
      .catch((err) => {
        if (err) {
          req.handleError(req, res, err);
        }
      });
  }

  //logout
  logout(req, res, next) {

    // const authTokenWrapper = ModelFactory.getModel('AuthToken');
    // const authTokenModel = authTokenWrapper.model;

    //user
    const business = req.user;
    if (!business) {
      return req.forbiddenResponse(req, res, business);
    }

    const { token } = (business || {})
    if (!req.confirmResult(req, res, token)) {
      throw new Error('Invalid token');
    }
    const { _id } = (token || {});
    token.remove(function (err, result) {
      if(!result || !result.$isDeleted()){
        throw new Error('Authenticate token not created');
      }
      req.handleSuccess(req, res, 'OK', `Token ${_id} deleted` );
    })
    // delete token
  }

  // AuthToken


  // getParams
  getLoginParams(req) {
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
      return _.defaults(
        this.getPostLoginParams(req),
        this.getURLLoginParams(req)
      );
    }
    return this.getBasicAuthLoginParams(req);
  }

  getPostLoginParams(req) {
    const body = req.body;
    const username = (body.usuario || body.username);
    const password =  (body.senha || body.password);
    const result = { username, password };
    return result;
  }

  getBasicAuthLoginParams(req){
    const base64Credentials =  (req.headers.authorization || '').split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');
    const result = { username, password };
    return result;
  }

  getURLLoginParams(req) {
    var q = url.parse(req.url, true);
    const { username, password } = q;
    const result = { username, password };
    return result;
  }
}

const service = new AuthService();
module.exports = service;
