const {
  ModuloDocumentCrudService,
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
//require('../../../../../../abstracts/service/modulo/modulo-crud-service');
const { ModelFactory } = require('../../../../commons/model-factory');
const DocumentPersistence = require('../../../../commons/crud/persistence/impl/document-persistence');

class ModuloReceitaService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloReceitaService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Receita');
    super({
      urlFilters: {
        empresa: { _id: 'empresa' }
      }
    });
    this.documentPersistence = new DocumentPersistence(
      modelWrapper,
      {
        nextQuery: this.nextQuery.bind(this)
      }
    );
  }

  modulo() {
    return 'admin';
  }

  nextQuery(params) {
    return [
      {
        '$lookup': {
          'from': 'midias', 
          'localField': 'video', 
          'foreignField': '_id', 
          'as': 'video'
        }
      },
      {
        '$unwind': '$video'
      },
      {
        '$lookup': {
          'from': 'midias', 
          'localField': 'image', 
          'foreignField': '_id', 
          'as': 'image'
        }
      },
      {
        '$unwind': '$image'
      },
      {
        '$lookup': {
          'from': 'empresas', 
          'localField': 'empresa', 
          'foreignField': '_id', 
          'as': 'empresa'
        }
      },
      {
        '$unwind': '$empresa'
      }
    ]
  }
}

module.exports = { ModuloReceitaService };
