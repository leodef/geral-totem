const {
  ModuloDocumentCrudService,
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModuloClienteService
} = require('../cliente');
const {
  ModuloReceitaRespostaTotemService
} = require('./receita-resposta-totem');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');

const
  DocumentPersistence
= require('../../../../../commons/crud/persistence/impl/document-persistence');

class ModuloRespostaTotemService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloRespostaTotemService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('RespostaTotem');
    super({
      urlFilters: {
        totem: { _id: 'totem' },
      }
    });
    this.clienteService = ModuloClienteService.getInstance();
    this.receitaRespostaTotemService = ModuloReceitaRespostaTotemService.getInstance();
    this.documentPersistence = new DocumentPersistence(
      modelWrapper,
      { nextQuery: this.nextQuery.bind(this) }
    );
  }

  modulo() {
    return 'admin';
  }

  nextQuery(params) {
    return [
      {
        '$lookup': {
          'from': 'clientes', 
          'localField': 'cliente', 
          'foreignField': '_id', 
          'as': 'cliente'
        }
      },
      {
        '$unwind': '$cliente'
      },
      {
        '$lookup': {
          'from': 'totems', 
          'localField': 'totem', 
          'foreignField': '_id', 
          'as': 'totem'
        }
      },
      {
        '$unwind': '$totem'
      }
    ]
  }

  create(req, res, next) {
    const create = super.create.bind(this);
    const clienteService = this.clienteService;
    const receitaRespostaTotemService = this.receitaRespostaTotemService;
    const body = this.getReqBody(req);
    const { receita, cliente } = body;
    // cria cliente
    const task = clienteService.createItem(cliente)
      .then((result) => {
        body.cliente = result;
        return body;
      }).then((result) =>
        this.createItem(result)
      ).then( (resposta) => (
        receita ?
          receitaRespostaTotemService.createItem({
            resposta,
            receita
          }).then((result) => resposta) :
          resposta)
      );
    const action = 'create';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  update(req, res, next) {
    const update = super.update.bind(this);
    const clienteService = this.clienteService;
    const receitaRespostaTotemService = this.receitaRespostaTotemService;
    const body = this.getReqBody(req);
    const { cliente, receita } = body;
    const task = clienteService.updateItem(cliente)
      .then((result) => {
        body.cliente = result;
        return body;
      }).then((result) =>
        update(req, res, next)
      ).then( (response) => (
        receita ?
          receitaRespostaTotemService.updateItem({
            response,
            receita
          }).then((result) => response) :
          response)
      );
    const action = 'update';
    return this.resolveTask(
      req,
      res,
      next,
      task,
      action
    );
  }

  remove(req, res, next) {
    const remove = super.remove.bind(this);
    const clienteService = this.clienteService;
    const receitaRespostaTotemService = this.receitaRespostaTotemService;
    let response = null
    const task = remove(
        req,
        res,
        next
      ).then( (result) =>
        response = result
      ).then((result) =>
        response && response.cliente ?
          clienteService
            .removeItem(response.cliente)
            .then( (result) => response) :
          response
      ).then((result) =>
        response && response.receita ?
          receitaRespostaTotemService
            .removeItem(response.receita)
            .then( (result) => response) :
          response
      );
      const action = 'remove';
      return this.resolveTask(
        req,
        res,
        next,
        task,
        action
      );
  }
}
module.exports = { ModuloRespostaTotemService };
