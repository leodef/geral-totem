const {
  ModuloDocumentCrudService,
} = require('../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../../commons/model-factory');
const
  DocumentPersistence
= require('../../../../../commons/crud/persistence/impl/document-persistence');

class ModuloReceitaRespostaTotemService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloReceitaRespostaTotemService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('ReceitaRespostaTotem');
    super({
      urlFilters: {
        totem: { _id: 'totem' },
      }
    });
    this.documentPersistence = new DocumentPersistence(
      modelWrapper,
      { nextQuery: this.nextQuery.bind(this) }
    );
  }

  modulo() {
    return 'admin';
  }

  nextQuery(params) {
    return [
      {
        '$lookup': {
          'from': 'respostas', 
          'localField': 'resposta', 
          'foreignField': '_id', 
          'as': 'resposta'
        }
      },
      {
        '$unwind': '$resposta'
      },
      {
        '$lookup': {
          'from': 'receitas', 
          'localField': 'receita', 
          'foreignField': '_id', 
          'as': 'receita'
        }
      },
      {
        '$unwind': '$receita'
      }
    ]
  }

}
module.exports = { ModuloReceitaRespostaTotemService };
