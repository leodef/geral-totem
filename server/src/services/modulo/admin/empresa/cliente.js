const {
  ModuloDocumentCrudService,
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../../commons/model-factory');

class ModuloClienteService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloClienteService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Cliente');
    super({
      persistence: modelWrapper,
      urlFilters: {
        empresa: { _id: 'empresa' }
      }
    });
  }

  modulo() {
    return 'admin';
  }

}
module.exports = { ModuloClienteService };
