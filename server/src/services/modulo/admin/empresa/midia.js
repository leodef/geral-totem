const multer = require('multer');
const pathLib = require('path');
const mime = require('mime');
const fs = require('fs');
const _ = require('lodash');

const {
  ModuloDocumentCrudService,
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
//require('../../../../../../abstracts/service/modulo/modulo-crud-service');
const { ModelFactory } = require('../../../../commons/model-factory');
const { FileService } = require('../../../../services/file');
const { isDuration } = require('moment');

class ModuloMidiaService extends ModuloDocumentCrudService {

  static getInstance(config) {
    return new ModuloMidiaService(config);
  }

  static getApiDownloadPath(path) {
    return `/modulo/admin/midia/download/${path || ''}`;
  }

  static getFileName(name) {
    return `${Date.now()}-${name}`;
  }

  static getApiViewPath(path) {
    return `/modulo/admin/midia/view/${path || ''}`;
  }

  static getUploadFolter(path, user) {
    return FileService.getUploadFolter(
      `midia/${user && user._id ? user._id + '/' : 'no_user'}${
        path ? path + '/' : ''
      }`
    );
  }

  static getFileBody(req, res, next) {
    let { body, file, params } = req || {};
    let { titulo, uri, formType } = body || {};
    let { dest } = params;
    let {
      encoding,
      mimetype,
      destination,
      filename,
      originalname,
      path,
      size,
      buffer,
    } = file || {};
    const isUri = formType === 'uri';
    const isArquivo = formType === 'arquivo';
    /*
               nomeArquivo: '',
            encoding: '',
            mime: '',
            pasta: '',
            arquivo: '',
            local: '',
            tamanho: '',
            uri: String,
            uriPath: String,
            downloadUri: String,
            downloadPath: String,
            extensao: '',*/
    const nomeArquivo =
      isUri && uri
        ? pathLib.basename(uri)
        : filename || ModuloMidiaService.getFileName(originalname);
    titulo = titulo || nomeArquivo;
    const uriPath = ModuloMidiaService.getApiViewPath(nomeArquivo);
    uri = uri || (isUri && uri) ? uri : FileService.getApiUri(uriPath);
    const downloadPath = ModuloMidiaService.getApiDownloadPath(nomeArquivo);
    const downloadUri =
      uri || isUri ? uri : FileService.getApiUri(downloadPath);
    mimetype = isUri && uri ? mime.lookup(uri) : mimetype;
    const extensao = FileService.getExtension(nomeArquivo);
    const result = {
      formType,
      titulo,
      nomeArquivo,
      mime: mimetype,
      uriPath,
      uri,
      downloadPath,
      downloadUri,
      extensao,
      ...(isArquivo
        ? {
            ...(dest
              ? {
                  local: path,
                  pasta: destination,
                }
              : {}),
            encoding: encoding,
            tamanho: size,
            arquivo: buffer,
          }
        : {}),
    };
    const newBody = _.pickBy(result, _.identity);
    req.body = newBody;
    if (next) {
      next();
    }
  }

  static getUploadStorage(req, user) {
    const dest = ModuloMidiaService.getUploadFolter(null, user);
    fs.mkdirSync(dest, { recursive: true });
    var storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, dest);
      },
      filename: function (req, file, cb) {
        cb(null, ModuloMidiaService.getFileName(file.originalname));
      },
    });
    return { storage, dest };
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Midia');
    super({
      persistence: modelWrapper,
      urlFilters: {
        empresa: { _id: 'empresa' },
      },
      nextQuery: (params, context) => {
        return [{ $project: { ['arquivo']: 0 } }];
      },
    });
  }

  upload(req, res, next) {
    const user = this.auth(req, res, next);
    const { storage, dest } = ModuloMidiaService.getUploadStorage(req, user);
    req.params.dest = dest;
    const upload = multer({ storage }); //##!! storage
    const fUpload = upload.single('arquivo');
    return fUpload(req, res, next);
  }

  modulo() {
    return 'admin';
  }

  preUpdate(req, res, next) {
    req.logger.silly(`${typeof this} - preUpdate`);
    const documentPersistence = this.documentPersistence;
    const body = this.getReqBody(req);
    const { uri } = body;
    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    return documentPersistence
      .find({ _id })
      .then((midia) => {
        if (midia && midia.uri !== uri) {
          return this.removeFile(req, midia, true);
        }
        return midia;
      })
      .then(() => {
        if (next) {
          next();
        }
      });
  }

  preRemove(req, res, next) {
    req.logger.silly(`${typeof this} - preRemove`);
    const documentPersistence = this.documentPersistence;

    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    return documentPersistence
      .find({ _id })
      .then((midia) => {
        return this.removeFile(req, midia);
      })
      .then(() => {
        if (next) {
          next();
        }
      });
  }

  async removeFile(req, midia, update = false) {
    if (!midia || !midia.local) {
      return null;
    }
    const { local, _id } = midia;
    // FileService.getProjectPath(midia.local);

    try {
      await fs.unlinkSync(local);

      if (update) {
        const documentPersistence = this.documentPersistence;
        await documentPersistence.update(
          { _id },
          {
            $unset: {
              nomeArquivo: '',
              encoding: '',
              mime: '',
              pasta: '',
              arquivo: '',
              local: '',
              tamanho: '',
              uriPath: '',
              uri: '',
              downloadPath: '',
              downloadUri: '',
              extensao: '',
            },
          }
        );
      }
    } catch (err) {}
    return midia;
  }

  serveFile(req, res, next) {
    req.logger.silly(`${typeof this} - download`);
    const documentPersistence = this.documentPersistence;
    // user
    const user = this.auth(req, res, next);
    if (!user) {
      return;
    }

    // _id
    const _id = this.searchReqParam(req, 'id');
    const nomeArquivo = this.searchReqParam(req, 'nomeArquivo');
    const download = this.searchReqParam(req, 'download');
    return documentPersistence.find({ nomeArquivo }).then((file) => {
      if (file) {
        FileService.serverFile(file, res, download);
      }
    });
  }
}

module.exports = { ModuloMidiaService };
