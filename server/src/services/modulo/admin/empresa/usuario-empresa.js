const {
  ModuloDocumentCrudService,
} = require('../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModuloUsuarioService
} = require('../usuario');
const {
  ModelFactory
} = require('../../../../commons/model-factory');
const DocumentPersistence = require('../../../../commons/crud/persistence/impl/document-persistence');


class ModuloUsuarioEmpresaService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloUsuarioEmpresaService(config);
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('UsuarioEmpresa');
    super({
        urlFilters: {
          empresa: { _id: 'empresa' }
        }
      });
    this.usuarioService = ModuloUsuarioService.getInstance();
    this.documentPersistence = new DocumentPersistence(
      modelWrapper,
      {
        nextQuery: this.nextQuery.bind(this)
        // prevCallback: this.prevCallbackFunc.bind(this)
      }
    );
  }

  modulo() {
    return 'admin';
  }

  nextQuery(params) {
    return [
      {
        '$lookup': {
          'from': 'usuarios', 
          'localField': 'usuario', 
          'foreignField': '_id', 
          'as': 'usuario'
        }
      },
      {
        '$unwind': '$usuario'
      },
      {
        '$lookup': {
          'from': 'empresas', 
          'localField': 'empresa', 
          'foreignField': '_id', 
          'as': 'empresa'
        }
      },
      {
        '$unwind': '$empresa'
      }
    ]
  }

  create(req, res, next) {
    const create = super.create.bind(this);
    const usuarioService = this.usuarioService;
    const body = this.getReqBody(req);
    usuarioService.createItem(body.usuario)
    .then((result) => {
      body.usuario = result;
      return req.body = body;
    }).then((result) =>
      create(req, res, next)
    )
  }

  update(req, res, next) {
    const update = super.update.bind(this);
    const usuarioService = this.usuarioService;
    const body = this.getReqBody(req);

    usuarioService.updateItem(body.usuario).then((result) => {
      body.usuario = result;
      return req.body = body;
    }).then((result) =>
      update(req, res, next)
    )
  }

  remove(req, res, next) {
    const remove = super.remove.bind(this);
    const usuarioService = this.usuarioService;
    let response = null
    remove(req, res, next)
    .then( (result) =>
      response = result
    ).then((result) => 
      usuarioService.removeItem(result.usuario)
    ).then( (result) => response)
  }
}
module.exports = { ModuloUsuarioEmpresaService };
