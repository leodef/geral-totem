const bcrypt = require('bcrypt');
const env = require('../../../config/env');
const {
  ModuloDocumentCrudService,
} = require('../../../abstracts/service/modulo/modulo-crud-service');
//require('../../../../../../abstracts/service/modulo/modulo-crud-service');
const {
  ModelFactory
} = require('../../../commons/model-factory');
const passwordUtilsService = require('../../../services/password-utils').service;


function loadUsuario(body, context) {
  if (!body || !body.senha) {
    return body;
  }
  body.senha = passwordUtilsService.cryptPassword(body.senha);
  return body;
}



class ModuloUsuarioService extends ModuloDocumentCrudService {
  static getInstance(config) {
    return new ModuloUsuarioService(config);
  }

  modulo() {
    return 'admin';
  }

  constructor(config) {
    const modelWrapper = ModelFactory.getModel('Usuario');
    super({
      persistence: modelWrapper,
      resolveItem: loadUsuario,
      ...(config || {})
    });
  }

}

module.exports = { ModuloUsuarioService, loadUsuario };
