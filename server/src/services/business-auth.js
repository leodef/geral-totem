const { ModelFactory } = require('../commons/model-factory');

class BusinessAuthService {
  // API
  //login
  login(authToken, req, res, next) {
    const {
      usuario
    } = authToken
    const {
      senha,
      ...usuarioResult
    } = usuario
    const usuarioEmpresaWrapper = ModelFactory.getModel('UsuarioEmpresa');
    const usuarioEmpresaModel = usuarioEmpresaWrapper.model;
    return usuarioEmpresaModel
          .findOne({ usuario: (usuario._id || usuario) })
          .populate(
            'empresa',
            // 'empresa.tipo',
            {
              path: 'usuario',
              select: { '_id': 1, 'usuario':1, 'nome':1, 'status':1}
            }
          ).then(result => (result || { usuario: usuarioResult }))
  }

  //logout
  logout(logoutParams, req, res, next) {
    return logoutParams;
  }
}

  
const service = new BusinessAuthService();
module.exports = { BusinessAuthService, service };
