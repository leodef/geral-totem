const bcrypt = require('bcrypt');
const generator = require('generate-password');
const env = require('../config/env');

class PasswordUtilsService {
  // password 
  generateRandomPassword() {
    return generator.generate({ length: 10, numbers: true });
  }

  generateRandomCryptPassword() {
    const password = this.generateRandomPassword();
    const crypt = this.cryptPassword(password);
    return { password, crypt };
  }

  cryptPassword(password) {
    const salt = env.auth.salt;
    return bcrypt.hashSync(password, salt);
    // return password;
  }

  comparePassword(pass, crypt) {
    // const salt = env.auth.salt;
    return bcrypt.compareSync(pass, crypt);
    // return !!pass && pass === crypt;
  }
}

  
const service = new PasswordUtilsService();
module.exports = { PasswordUtilsService, service };
