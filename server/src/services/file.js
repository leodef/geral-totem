const fs = require('fs');
const env = require('../config/env');
const stream = require('stream');

class FileService {
  static getExtension(name) {
    if (!name) {
      return '';
    }
    const splited = name.split('.');
    return splited[splited.length - 1];
  }

  static getUri(path) {
    return FileService.clearUri(`${env.domain}/${path || ''}`);
  }
  static getApiUri(path) {
    return FileService.clearUri(`${env.domain}/api/${path || ''}`);
  }

  static clearUri(uri) {
    if (!uri) {
      return '';
    }
    return uri.split('//').join('/').split('/./').join('/');
  }
  static getUploadFolter(path) {
    return `./public/uploads/${path ? path + '/' : ''}`;
  }

  static getProjectPath(path) {
    return `${process.cwd()}/${path ? path + '/' : ''}`;
  }

  static replaceFile(config) {
    const { upload, remove } = config;
    return FileService.removeFile(remove).then((result) =>
      FileService.uploadFile(upload)
    );
  }

  static uploadFile(config) {
    const { oldpath, newpath } = config;
    return fs.rename(oldpath, newpath).then((err) => {
      if (err) throw err;
      const upload = true;
      return config;
    });
  }

  static removeFile(config) {
    const { path } = config;
    return fs.unlink(path, (err) => {
      if (err) throw err;
      return config;
    });
  }

  static serverFile(param, res, download = false) {
    if (param.arquivo) {
      return FileService.serveFileFromBuffer(param, res, download);
    }
    return FileService.serveFileFromPath(param, res, download);
  }
  static serveFileFromPath(param, res, download = false) {
    const { local, nomeArquivo, mime } = param;
    if (download) {
      res.setHeader(
        'Content-disposition',
        'attachment; filename=' + nomeArquivo
      );
    }
    res.setHeader('Content-type', mime);

    var filestream = fs.createReadStream(local);
    filestream.pipe(res);
  }

  static serveFileFromBuffer(param, res, download = false) {
    const { local, nomeArquivo, mime, arquivo } = param;
    var fileContents = Buffer.from(arquivo, 'base64');

    var readStream = new stream.PassThrough();
    readStream.end(fileContents);

    if (download) {
      res.setHeader(
        'Content-disposition',
        'attachment; filename=' + nomeArquivo
      );
    }
    res.setHeader('Content-type', mime);

    readStream.pipe(res);
  }
}

module.exports = { FileService };
