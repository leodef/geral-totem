const crud = require('../../../commons/crud');
const Auth = require('../../../config/auth');
const authTask = Auth.authTask();

// Roteamento de CRUD com tratamento para telas dos modulos
class ModuloCrudRouter extends crud.CrudRouter {

  getAuthTask() {
    return authTask;
  }

}

module.exports = { ModuloCrudRouter };
