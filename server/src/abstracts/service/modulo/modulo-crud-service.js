const crud = require('../../../commons/crud');
  

function auth(req, res, service) {
  return true;
}

// Serviço de CRUD com tratamento para telas dos modulos
class ModuloDocumentCrudService extends crud.DocumentCrudService {

    constructor(config) { super(config); }

    auth(req, res) { return auth(req, res, this); }

    static getInstance(config) { return new ModuloDocumentCrudService(config); }
}

class ModuloNestedDocumentCrudService extends crud.NestedDocumentCrudService {

  constructor(config) { super(config); }

  auth(req, res) { return auth(req, res, this); }

  static getInstance(config) { return new ModuloNestedDocumentCrudService(config); }
}

class ModuloReferencedDocumentCrudService extends crud.ReferencedDocumentCrudService {

  constructor(config) { super(config); }

  auth(req, res) { return auth(req, res, this); }

  static getInstance(config) { return new ModuloReferencedDocumentCrudService(config); }
}

module.exports = {
  ModuloDocumentCrudService,
  ModuloNestedDocumentCrudService,
  ModuloReferencedDocumentCrudService
};
