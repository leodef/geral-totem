const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Usuario
        usuario: string
        senha: string
        nome: string
        status: string

*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      usuario: String,
      senha: String,
      nome: String,
      // tipo: { type: Schema.Types.ObjectId, ref: 'TipoUsuario' },
      status: String
    },
    { timestamps: true }
  );
  return schema;
};

const key = 'Usuario';
const depedencies = [];
const populate = [];
module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
