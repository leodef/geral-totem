const mongoose = require('mongoose');
const { TIPO_TOKEN_USUARIO } = require('../../enums/tipo-token-usuario.enum');
const Schema = mongoose.Schema;
/*
  var autoPopulate = function (next) {
    this.populate('usuario');
    next();
  };
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId
      validade: Date,
      tipo: { type: String, enum: TIPO_TOKEN_USUARIO },
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    },
    { timestamps: true }
  );
  // schema.pre('findOne', autoPopulate).pre('find', autoPopulate);

  return schema;
};

const key = 'TokenUsuario';
const depedencies = ['Usuario'];
const populate = ['usuario'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
