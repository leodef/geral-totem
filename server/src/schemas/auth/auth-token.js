const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/*
var autoPopulate = function (next) {
  this.populate('usuario');
  // this.populate('usuario.tipo');
  next();
};
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId
      validade: Date,
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    },
    {
      timestamps: true,
      // collection: 'authinfos'
    }
  );
  // schema.pre('findOne', autoPopulate).pre('find', autoPopulate);

  return schema;
};

const key = 'AuthToken';
const depedencies = ['Usuario'];
const populate = ['usuario', 'usuario.tipo']
module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate
};
