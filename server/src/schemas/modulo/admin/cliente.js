const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Cliente
        nome: string
        cpf: string
        email: string
        telefone: string
        empresa: Empresa
        totem: Totem
        // verificar campos cadastro

var autoPopulate = function (next) {
  this.populate('empresa');
  this.populate('totem');
  next();
};
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      nome: String,
      cpf: String,
      email: String,
      telefone: String,
      empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
      totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
    },
    { timestamps: true }
  );
  // schema.
  //     pre('findOne', autoPopulate).
  //     pre('find', autoPopulate);

  return schema;
};
const key = 'Cliente';
const depedencies = ['Empresa', 'Totem'];
const populate = ['empresa', 'totem'];
module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
