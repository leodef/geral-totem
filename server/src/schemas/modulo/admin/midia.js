const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Midia
	  //_id: Schema.Types.ObjectId,
	  titulo: String,
      encoding: String, // encoding: '7bit',
      mime: String, // mimetype: 'image/jpeg',
      pasta: String, //destination: './public/data/uploads/undefined',
      nomeArquivo: String, // filename: '1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      local :String, // path: 'public\\data\\uploads\\undefined\\1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      tamanho: Number, // size: 5348
      uri: String,
      uriPath: String,
      downloadUri: String,
      downloadPath: String,
      extensao: String,
      arquivo: Buffer
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      titulo: String,
      encoding: String, // encoding: '7bit',
      mime: String, // mimetype: 'image/jpeg',
      pasta: String, //destination: './public/data/uploads/undefined',
      nomeArquivo: String, // filename: '1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      local: String, // path: 'public\\data\\uploads\\undefined\\1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      tamanho: Number, // size: 5348
      uri: String,
      uriPath: String,
      downloadUri: String,
      downloadPath: String,
      extensao: String,
      arquivo: Buffer,
    },
    { timestamps: true }
  );

  return schema;
};
const key = 'Midia';
const depedencies = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
};
