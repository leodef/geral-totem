const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Totem
        codigo: string
        titulo: string
        desc: string
        status: string
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      codigo: String,
      titulo: String,
      desc: String,
      status: String,
      empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    },
    { timestamps: true }
  );

  return schema;
};
const key = 'Totem';
const depedencies = [];
const populate = ['empresa'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
