const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
  RespostaTotem
    totem: Totem
    cliente: Cliente
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
      cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
    },
    { timestamps: true }
  );

  return schema;
};
const key = 'RespostaTotem';
const depedencies = ['Totem', 'Cliente'];
const populate = ['totem', 'cliente'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
