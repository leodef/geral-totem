const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
Token
  usuario: Usuario
  token: string
  tipo: stirng
  dataExpiracao: Date

  var autoPopulate = function(next) {
    this.populate('usuario');
    next();
  };
*/
const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
      token: String,
      tipo: String,
      dataExpiracao: Date,
    },
    { timestamps: true }
  );
  // schema.
  //    pre('findOne', autoPopulate).
  //    pre('find', autoPopulate);

  return schema;
};
const key = 'Token';
const depedencies = ['Usuario'];
const populate = ['usuario'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
