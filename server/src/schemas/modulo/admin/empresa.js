const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Empresa
        nome: string
        desc: string
        cnpj: string
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      nome: String,
      desc: String,
      cnpj: String,
    },
    { timestamps: true }
  );

  return schema;
};
const key = 'Empresa';
const depedencies = [];

module.exports = {
  key,
  schemaFunc,
  depedencies,
};
