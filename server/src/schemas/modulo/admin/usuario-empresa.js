const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    UsuarioEmpresa
        empresa: Usuario
        usuario: Empresa
        tipo: string
  var autoPopulate = function (next) {
    this.populate('empresa');
    this.populate('usuario');
    next();
  };
*/


const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
      usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
      tipo: String,
    },
    { timestamps: true }
  );
  return schema;
};
const key = 'UsuarioEmpresa';
const depedencies = ['Empresa', 'Usuario'];
const populate = [
  {path:'empresa', ref: 'Empresa'},
  {path:'usuario', ref: 'Usuario'}

];
module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
