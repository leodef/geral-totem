const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
    Receita
        titulo: string
        desc: string
        pagina: string // gravar uma pagina em html
        video: Midia
        image: Midia
        empresa: Empresa
*/
/*
var autoPopulate = function(next) {
    this.populate('video');
    this.populate('image');
    this.populate('empresa');
    next();
  };
*/
const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      titulo: String,
      desc: String,
      pagina: String,
      video: { type: Schema.Types.ObjectId, ref: 'Midia' },
      image: { type: Schema.Types.ObjectId, ref: 'Midia' },
      empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    },
    { timestamps: true }
  );
  // schema.
  //    pre('findOne', autoPopulate).
  //    pre('find', autoPopulate);

  return schema;
};
const key = 'Receita';
const depedencies = ['Midia', 'Empresa'];
const populate = ['video', 'image', 'empresa'];
module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
