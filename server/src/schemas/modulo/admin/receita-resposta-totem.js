const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/*
  ReceitaRespostaTotem
    resposta: RespostaTotem
    receita: Receita
*/

const schemaFunc = (params) => {
  const schema = Schema(
    {
      //_id: Schema.Types.ObjectId,
      receita: { type: Schema.Types.ObjectId, ref: 'Receita' },
      resposta: { type: Schema.Types.ObjectId, ref: 'RespostaTotem' },
    },
    { timestamps: true }
  );

  return schema;
};
const key = 'ReceitaRespostaTotem';
const depedencies = ['Receita', 'RespostaTotem'];
const populate = ['receita', 'resposta'];

module.exports = {
  key,
  schemaFunc,
  depedencies,
  populate,
};
