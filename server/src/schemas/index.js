module.exports =  [
    // modulo.admin
    require('./modulo/admin/cliente'),
    require('./modulo/admin/empresa'),
    require('./modulo/admin/midia'),
    require('./modulo/admin/receita'),
    require('./modulo/admin/receita-resposta-totem'),
    require('./modulo/admin/resposta-totem'),
    require('./modulo/admin/token'),
    require('./modulo/admin/totem'),
    require('./modulo/admin/usuario-empresa'),
    // auth
    require('./auth/auth-token'),
    require('./auth/token-usuario'),
    require('./auth/usuario')
];