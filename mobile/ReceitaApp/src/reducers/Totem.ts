import { TotemType } from '../types/Totem'

export interface CrudReducerState {
  items: Array<any>;
  item: any;
  loading: boolean;
  error: string | null;
  fetchLoading: boolean;
  updateLoading: boolean;
  fetched: boolean;
}
export const initialState: CrudReducerState = {
  items: [],
  item: null,
  loading: false,
  error: null,
  fetchLoading: false,
  updateLoading: false,
  fetched: false
}

export const reduce = (
  state: CrudReducerState = initialState,
  action: any): CrudReducerState  => {
    const actionType = action.type
    switch (actionType) {
      case TotemType.FETCH:
      case TotemType.UPDATE:
        return state // saga actions
      case TotemType.FETCH_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          fetchLoading: true
        }
      case TotemType.UPDATE_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          updateLoading: true
        }
      case TotemType.FETCH_SUCCESS:
        return {
          ...state,
          items: (action.payload.items || []),
          error: null,
          loading: false,
          fetchLoading: false,
          fetched: true
        }
        case TotemType.UPDATE_SUCCESS:
          return {
            ...state,
            item: action.payload.item,
            error: null,
            loading: false,
            updateLoading: false
          }
      case TotemType.FETCH_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          fetchLoading: false
        }
      case TotemType.UPDATE_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          updateLoading: false
        }
      case TotemType.LOAD:
      case TotemType.SET:
        return state
      case TotemType.LOAD_SUCCESS:
      case TotemType.SET_SUCCESS:
        return {
          ...state,
          item: action.payload
        }
      default:
        return state
    }
}


export default reduce;