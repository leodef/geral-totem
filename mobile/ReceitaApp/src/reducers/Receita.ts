import { ReceitaType } from '../types/Receita'

export interface CrudReducerState {
  items: Array<any>;
  item: any;
  loading: boolean;
  error: string | null;
  fetchLoading: boolean;
  findLoading: boolean;
  fetched: boolean;
}
export const initialState: CrudReducerState = {
  items: [],
  item: null,
  loading: false,
  error: null,
  fetchLoading: false,
  findLoading: false,
  fetched: false
}

export const reduce = (
  state: CrudReducerState = initialState,
  action: any): CrudReducerState  => {
    const actionType = action.type
    switch (actionType) {
      case ReceitaType.FIND:
      case ReceitaType.FETCH:
        return state // saga actions
      // PENDING - FETCH
      case ReceitaType.FIND_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          findLoading: true
        }
      case ReceitaType.FETCH_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          fetchLoading: true
        }
      // SUCCESS - FETCH
      case ReceitaType.FIND_SUCCESS:
        return {
          ...state,
          item: (action.payload.item || state.item),
          error: null,
          loading: false,
          findLoading: false
        }
      case ReceitaType.FETCH_SUCCESS:
        return {
          ...state,
          items: (action.payload.items || []),
          error: null,
          loading: false,
          fetchLoading: false,
          fetched: true
        }
      // FAILURE - FETCH
      case ReceitaType.FIND_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          findLoading: false
        }
      case ReceitaType.FETCH_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          fetchLoading: false
        }
      default:
        return state
    }
}

export default reduce;