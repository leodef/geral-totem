import { combineReducers } from 'redux'

import authReducer from './Auth'
import respostaReducer from './Resposta'
import loadingReducer from './Loading'
import messageReducer from './Message'
import navReducer from './Nav'
import receitaReducer from './Receita'
import totemReducer from './Totem'

const createRootReducer = () =>
  combineReducers({
    auth: authReducer,
    resposta: respostaReducer,
    loading: loadingReducer,
    message: messageReducer,
    nav: navReducer,
    receita: receitaReducer,
    totem: totemReducer
  })

export default createRootReducer
