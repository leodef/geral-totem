import { LoadingType } from '../types/Loading'

export const initialState: any = {
  tasks: []
}

export const reduce = (state = initialState, action: any) => {
  const tasks = state.tasks
  switch (action.type) {
    case LoadingType.START:
      tasks.push((action.payload || tasks.length).toString())
      return {
        tasks
      }
    case LoadingType.STOP:
      return {
        tasks: state.tasks.filter((task: any) =>
          task.toString() !== (action.payload || tasks.length).toString()
        )
      }
    default:
      return state
  }
}

export default reduce;