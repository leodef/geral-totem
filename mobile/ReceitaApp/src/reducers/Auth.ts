import { AuthType } from '../types/Auth'

// AuthType.AUTH_PENDING
// AuthType.AUTH_SUCCESS
// AuthType.AUTH_FAILURE

export const reduce = (state = {
  error: null,
  loading: false,
  user: null,
  token: null
}, action: any) => {
  switch (action.type) {
    case AuthType.LOAD:
    case AuthType.AUTH:
      return state // saga actions
    case AuthType.AUTH_PENDING:
      return {
        ...state,
        error: null,
        loading: true,
        user: null,
        token: null
      }
    case AuthType.LOAD_SUCCESS:
    case AuthType.AUTH_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        user: action.payload ? action.payload.user : null,
        token: action.payload ? action.payload.token : null
      }
      // FAILURE
    case AuthType.AUTH_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
        user: null,
        token: null
      }
    case AuthType.LOGOUT:
      return {
        ...state,
        loading: false,
        user: null,
        token: null
      }
    default:
      return state
  }
}
export default reduce;