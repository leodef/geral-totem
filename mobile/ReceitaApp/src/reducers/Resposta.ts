import { RespostaType } from '../types/Resposta';

export interface CrudReducerState {
  loading: boolean;
  error: string | null;
}
export const initialState: CrudReducerState = {
  loading: false,
  error: null,
};

export const reduce = (
  state: CrudReducerState = initialState,
  action: any
): CrudReducerState => {
  const actionType = action.type;
  switch (actionType) {
    case RespostaType.CREATE:
      return state; // saga actions
    case RespostaType.CREATE_PENDING:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case RespostaType.CREATE_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
      };
    case RespostaType.CREATE_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
      };
    default:
      return state;
  }
};
export default reduce;
