import { NavType } from '../types/Nav'

export interface CrudReducerState {
  tela: any;
}
export const initialState: CrudReducerState = {
  tela: null
}

export const reduce = (
  state: CrudReducerState = initialState,
  action: any): CrudReducerState  => {
    const actionType = action.type
    switch (actionType) {
      case NavType.MAIN:
      case NavType.NAVIGATE:
      case NavType.BACk:
      case NavType.REPLACE:
      case NavType.PUSH:
      case NavType.POP:
      case NavType.POP_TO_TOP:
        return { ... state, tela: action.payload }
      default:
        return state
    }
}

export default reduce;