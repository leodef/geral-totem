import { MessageType } from '../types/Message'

export const initialState: any = {
  messages: []
}

export const reduce = (state = initialState, action: any) => {
  switch (action.type) {
    case MessageType.ADD: {
      return { messages: [...state.messages, action.payload] }
    }
    case MessageType.REMOVE:
      // ##!!
      // const messages = state.messages.filter( (message: any) => action.payload.id !== message.id)
      return { messages: [] }
    default:
      return state
  }
}

export default reduce;