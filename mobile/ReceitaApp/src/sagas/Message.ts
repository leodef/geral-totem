import { takeLatest, put, all, delay } from 'redux-saga/effects' // select, call
import { MessageType, MessageVariant } from '../types/Message'

export class MessageSaga {
  static getSagasFunction () {
    const obj = new MessageSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new MessageSaga()
    return obj.getSagas()
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(MessageType.SHOW, context.show.bind(context)),
      // takeLatest(MessageType.ADD, context.add.bind(context)),
      // takeLatest(MessageType.REMOVE, context.remove.bind(context))
    ])
  }

  public * show (action: any) {
    const message = action.payload.message || action.payload
    const variant = action.payload.variant || MessageVariant.primary
    const id = new Date().getMilliseconds().toString()
    const payload = { message, id, variant }
    try {
      yield put({ type: MessageType.ADD, payload })
      yield delay(1000)
    } finally {
      yield put({ type: MessageType.REMOVE, payload })
    }
  }

  /*
    public *  add (action: any) {
      const message = action.payload.message || action.payload
      const variant = action.payload.variant || MessageVariant.primary
      const id = new Date().getMilliseconds().toString()
      const payload = { message, id, variant }
    
    }

    public * remove (action: any) {
      const message = action.payload.message || action.payload
      const variant = action.payload.variant || MessageVariant.primary
      const id = new Date().getMilliseconds().toString()
      const payload = { message, id, variant }
    }
  */
}
export default MessageSaga.getSagasFunction()
