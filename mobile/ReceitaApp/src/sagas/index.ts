// import { all } from 'redux-saga/effects';
import authSaga from './Auth'
import respostaSaga from './Resposta'
import messageSaga from './Message'
import navSaga from './Nav'
import receitaSaga from './Receita'
import totemSaga from './Totem'

const rootSaga = [
  authSaga,
  respostaSaga,
  messageSaga,
  navSaga,
  receitaSaga,
  totemSaga
]
export default rootSaga
