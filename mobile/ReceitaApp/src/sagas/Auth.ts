import { takeLatest, put, call, all } from 'redux-saga/effects' // select
import { NavType } from '../types/Nav'
import { LoadingType } from '../types/Loading'
import { MessageVariant, MessageType } from '../types/Message'
import { AuthType, loginNav, logoutNav } from '../types/Auth'
import { AuthService } from '../services/Auth'

export class AuthSaga {

  static getSagasFunction () {
    const obj = new AuthSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new AuthSaga()
    return obj.getSagas()
  }

  // eslint-disable-next-line no-useless-constructor
  constructor (
    public service: AuthService = new AuthService()
  ) { }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(AuthType.AUTH, context.login.bind(context)),
      takeLatest(AuthType.LOAD, context.load.bind(context)),
      takeLatest([
        AuthType.LOAD_SUCCESS,
        AuthType.AUTH_SUCCESS,
        AuthType.AUTH_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    let msg = null
    let variant = MessageVariant.primary
    switch (action.type) {
      case AuthType.AUTH_SUCCESS:
        msg = 'Login realizado com sucesso'
        variant = MessageVariant.success
        break
      case AuthType.LOAD_SUCCESS:
          msg = 'Usuário encontrado'
          variant = MessageVariant.success
          break
      case AuthType.AUTH_FAILURE:
        msg = 'Usuário ou senha inválido'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW, payload })
    yield call(console.log, msg)
  }

  public * login (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'login' })
    yield put({ type: AuthType.AUTH_PENDING, payload: action.payload })
    try {
      const postsFromApi = yield call(service.login.bind(service), action.payload)
      yield put({ type: AuthType.AUTH_SUCCESS, payload: postsFromApi })
      yield put({ type: NavType.NAVIGATE, payload: loginNav })
    } catch (error) {
      yield put({ type: AuthType.AUTH_FAILURE, payload: error })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'login' })
    }
  }

  public * logout () {
    yield put({ type: AuthType.LOGOUT })
    //yield put(push('/login'))
      yield put({ type: NavType.NAVIGATE, payload: logoutNav })
  }

  public * load () {
    const context = this
    const service = context.service
    const localData = yield service.load()
    yield put({ type: AuthType.LOAD_SUCCESS, payload: localData })
    return null
  }

}
export default AuthSaga.getSagasFunction()
