import { takeLatest, all } from 'redux-saga/effects' // select, call
import { NavType } from '../types/Nav'
import navService from '../services/Nav'
export class NavSaga {
  static getSagasFunction () {
    const obj = new NavSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new NavSaga()
    return obj.getSagas()
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(NavType.MAIN, context.main.bind(context)),
      takeLatest(NavType.NAVIGATE, context.navigate.bind(context)),
      takeLatest(NavType.BACk, context.goBack.bind(context)),
      takeLatest(NavType.REPLACE, context.replace.bind(context)),
      takeLatest(NavType.PUSH, context.push.bind(context)),
      takeLatest(NavType.POP, context.pop.bind(context)),
      takeLatest(NavType.POP_TO_TOP, context.popToTop.bind(context))
    ])
  }

  public * main (action: any) {
    const params = action.payload
    navService.main(params)
  }

  public * navigate (action: any) {
    const params = action.payload
    navService.navigate(params)
  }

  public * goBack (action: any) {
    navService.goBack()
  }

  public * replace (action: any) {
    const params = action.payload
    navService.replace(params)
  }

  public * push (action: any) {
    const params = action.payload
    navService.push(params)
  }

  public * pop (action: any) {
    navService.pop(action ? action.payload : null)
  }
  
  public * popToTop (action: any) {
    const params = action.payload
    navService.popToTop()
  }
  
}
export default NavSaga.getSagasFunction()
