import { takeLatest, put, call, all, debounce } from 'redux-saga/effects' // select
import { MessageVariant, MessageType } from '../types/Message'
import { LoadingType } from '../types/Loading'
import { ReceitaType } from '../types/Receita'
import { ReceitaService } from '../services/Receita'

export class ReceitaSaga {
  service: ReceitaService

  static getSagasFunction () {
    const obj = new ReceitaSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new ReceitaSaga()
    return obj.getSagas()
  }

  constructor () {
    this.service = new ReceitaService()
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      debounce(500, ReceitaType.FETCH, context.fetch.bind(context)),
      takeLatest(ReceitaType.FIND, context.find.bind(context)),
      takeLatest([
        // SUCCESS
        ReceitaType.FIND_SUCCESS,
        ReceitaType.FETCH_SUCCESS,
        // FAILURE
        ReceitaType.FIND_FAILURE,
        ReceitaType.FETCH_FAILURE,
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    const context = this
    let variant = MessageVariant.primary
    let msg = null
    switch (action.type) {
      case ReceitaType.FIND_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case ReceitaType.FIND_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
      case ReceitaType.FETCH_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case ReceitaType.FETCH_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW, payload })
    yield call(console.log, msg)
  }

  public * find (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'find' })
    yield put({ type: ReceitaType.FIND_PENDING })
    try {
      const findResult = yield call(service.find.bind(service), action.payload)
      yield put({ type: ReceitaType.FIND_SUCCESS, payload: { item: findResult } })
    } catch (error) {
      yield put({ type: ReceitaType.FIND_FAILURE, payload: { error } })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'find' })
    }
  }

  public * fetch (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'fetch' })
    yield put({ type: ReceitaType.FETCH_PENDING })
    try {
      const postsFromApi = yield call(
        service.fetch.bind(service), action.payload)
      yield put({
        type: ReceitaType.FETCH_SUCCESS,
        payload: postsFromApi
      })
    } catch (error) {
      yield put({ type: ReceitaType.FETCH_FAILURE, payload: { items: [], error } })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'fetch' })
    }
  }
}

export default ReceitaSaga.getSagasFunction()
