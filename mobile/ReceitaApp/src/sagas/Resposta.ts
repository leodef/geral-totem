import { takeLatest, put, call, all, debounce } from 'redux-saga/effects' // select
// eslint-disable-next-line no-unused-vars
// eslint-disable-next-line no-unused-vars
import { MessageVariant, MessageType } from '../types/Message'
import { LoadingType } from '../types/Loading'
import { RespostaType, createNav } from '../types/Resposta'
import { RespostaService } from '../services/Resposta'
import { NavType } from '../types/Nav'

export class RespostaSaga {
  service: RespostaService

  static getSagasFunction () {
    const obj = new RespostaSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new RespostaSaga()
    return obj.getSagas()
  }

  constructor () {
    this.service = new RespostaService();
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(RespostaType.CREATE, context.create.bind(context)),
      takeLatest(RespostaType.CREATE_SUCCESS, context.onCreate.bind(context)),
      takeLatest([
        // SUCCESS
        RespostaType.CREATE_SUCCESS,
        // FAILURE
        RespostaType.CREATE_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    const context = this
    let variant = MessageVariant.primary
    let msg = null
    switch (action.type) {
      case RespostaType.CREATE_SUCCESS:
        msg = 'Cadastrado com sucesso'
        variant = MessageVariant.success
        break
      case RespostaType.CREATE_FAILURE:
        msg = 'Erro ao cadastrar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW, payload })
    yield call(console.log, msg)
  }

  public * create (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'create' })
    yield put({ type: RespostaType.CREATE_PENDING })
    try {
      const { parent, item } = action.payload
      const create = yield call(service.create.bind(service), item, parent)
      yield put({ type: RespostaType.CREATE_SUCCESS, payload: { item: create } })
      yield put({ type: NavType.NAVIGATE, payload: createNav })
    } catch (error) {
      yield put({ type: RespostaType.CREATE_FAILURE, payload: { error } })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'create' })
    }
  }

  public * onCreate (action: any) {

  }
}

export default RespostaSaga.getSagasFunction()
