import { takeLatest, put, call, all, debounce } from 'redux-saga/effects' // select
import { MessageVariant, MessageType } from '../types/Message'
import { LoadingType } from '../types/Loading'
import { TotemType, getOnSetNav } from '../types/Totem'
import { TotemService } from '../services/Totem'
import { NavType } from '../types/Nav'

export class TotemSaga {
  service: TotemService

  static getSagasFunction () {
    const obj = new TotemSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new TotemSaga()
    return obj.getSagas()
  }

  constructor () {
    this.service = new TotemService()
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      debounce(500, TotemType.FETCH, context.fetch.bind(context)),
      takeLatest(TotemType.UPDATE, context.update.bind(context)),
      takeLatest(TotemType.SET, context.set.bind(context)),
      takeLatest(TotemType.SET_SUCCESS, context.onSet.bind(context)),
      takeLatest([
        // SUCCESS
        TotemType.UPDATE_SUCCESS,
        TotemType.FETCH_SUCCESS,
        TotemType.SET_SUCCESS,
        TotemType.LOAD_SUCCESS,
        // FAILURE
        TotemType.UPDATE_FAILURE,
        TotemType.FETCH_FAILURE,
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    const context = this
    let variant = MessageVariant.primary
    let msg = null
    switch (action.type) {
      case TotemType.UPDATE_SUCCESS:
        msg = 'Atualizado com sucesso'
        variant = MessageVariant.success
        break
      case TotemType.SET_SUCCESS:
        msg = 'Totem selecionados'
        variant = MessageVariant.success
        break
      case TotemType.LOAD_SUCCESS:
        msg = 'Totem encontrado'
        variant = MessageVariant.success
        break
      case TotemType.FETCH_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case TotemType.UPDATE_FAILURE:
        msg = 'Erro ao atualizar'
        variant = MessageVariant.danger
        break
      case TotemType.FETCH_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW, payload })
    yield call(console.log, msg)
  }

  public * fetch (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'fetch' })
    yield put({ type: TotemType.FETCH_PENDING })
    try {
      const postsFromApi = yield call(
        service.fetch.bind(service), action.payload)
      yield put({
        type: TotemType.FETCH_SUCCESS,
        payload: postsFromApi
      })
    } catch (error) {
      yield put({ type: TotemType.FETCH_FAILURE, payload: { items: [], error } })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'fetch' })
    }
  }

  public * update (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START, payload: 'update' })
    yield put({ type: TotemType.UPDATE_PENDING })
    try {
      const updatedPost = yield call(service.update.bind(service), action.payload)
      yield put({ type: TotemType.UPDATE_SUCCESS, payload: { item: updatedPost } })
    } catch (error) {
      yield put({ type: TotemType.UPDATE_FAILURE, payload: { error } })
    } finally {
      yield put({ type: LoadingType.STOP, payload: 'update' })
    }
  }

  public * load () {
    const context = this
    const service = context.service

    const localData = yield service.load()
    yield put({ type: TotemType.LOAD_SUCCESS, payload: localData })
  }

  public * set (action: any) {
    const context = this
    const service = context.service
    const totem = action.payload

    const localData = yield service.set(totem)
    yield put({ type: TotemType.SET_SUCCESS, payload: localData })
  }

  public * onSet (action: any) {
    const onSetNav = getOnSetNav(action.payload);
    yield put({
      type: NavType.NAVIGATE,
      payload: onSetNav
    })
  }
  
}

export default TotemSaga.getSagasFunction()
