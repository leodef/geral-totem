import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import { user as apiUser } from '../../mock/Auth/user';

const AUTH_ROOT = 'http://localhost:3000/api'
/**
 * AuthService, class representing a authentication service.
 */
export class AuthService {
  static getUser() {
    return AsyncStorage.getItem('auth')
  }

  static setUser(value: any) {
    return AsyncStorage.setItem('auth', JSON.stringify(value))
  }

  static removeUser() {
    return AsyncStorage.removeItem('auth')
  }
  /**
   * Login, Aunthenticate in server.
   * @param {{username: string, password: string}} auth - Axios http request config.
   * @return {any} Authentication info.
   */
  login(user: { usuario: string, senha: string }): Promise<any> {
    user = _.defaults(user, apiUser)
    // create token
    const randFunc = () => {
      return Math.random().toString(36).substr(2); // remove `0.`
    };
    
    const tokenFunc = () => {
      return randFunc() + randFunc(); // to make it longer
    };

    const token = tokenFunc();

    const auth = {
      token,
      user
    }
    return AuthService.setUser(auth)
      .then((result) => {
        return auth
    })

  }

  /**
   * Load, Use storage info for configure, if it exists.
   * @return {any} Authentication info.
   */
  load() {
    return AuthService.getUser()
  }

  /**
   * Logout, remove authenticaion info from local storage.
   */
  logout() {
    return AuthService.removeUser()
  }
}

export default new AuthService()