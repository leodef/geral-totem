import { AsyncStorage } from 'react-native'
import { ApiService } from '../Api'
import { Buffer } from 'buffer'

const AUTH_ROOT = 'http://localhost:3000/api'
/**
 * AuthService, class representing a authentication service.
 */
export class AuthService extends ApiService {

  static loadHeaders(auth: any) {
    const { token } = (auth || {} as any);
    if(token) {
      const bearer = `Bearer ${token}`;
      ApiService.appendHeaders({ 'Authorization': bearer });
    } else {
      const header = ApiService.getHeaders();
      const { Authorization, ...newHeaders } = header;
      ApiService.setHeaders(newHeaders);
    }
    return auth;
  }

  static getUser() {
    return AsyncStorage.getItem('auth')
  }

  static setUser(value: any) {
    return AsyncStorage.setItem(
      'auth',
      JSON.stringify(value)
    ).then(() => value)
  }

  static removeUser() {
    return AsyncStorage.removeItem('auth')
  }
  /**
   * Login, Aunthenticate in server.
   * @param {{username: string, password: string}} auth - Axios http request config.
   * @return {any} Authentication info.
   */
  login(auth: { username: string, password: string }): Promise<any> {
    const path = 'auth/login'
    const { username, password } = auth
    const base64Auth = Buffer.from(
      `${username}:${password}`
    ).toString('base64')
    const headers = {
      'Authorization': `Basic ${base64Auth}`
    }
    let token = null as any;
    let user = null as any;
    return this.post(path, auth, { headers })
      .then((response: any) => {
       token = response.headers.get('x-auth-token')
       return response.json()
      })
      .then((usr: any) => user = usr)
      .then((usr: any) => ({user, token}) )
      .then((result) => AuthService.loadHeaders(result))
      .then((result: any) =>
        token ?
          AuthService.setUser(result):
          Promise.resolve(result)
      )
  }

  /**
   * LoadStorage, Use storage info for configure, if it exists.
   * @return {any} Authentication info.
   */
  load() {
    return  AuthService.getUser()
      .then((auth) => AuthService.loadHeaders(auth))
  }

  /**
   * Logout, remove authenticaion info from local storage.
   */
  logout() {
    return AuthService.removeUser()
    .then((auth) => AuthService.loadHeaders(null))
  }
}

export default new AuthService()