import * as _ from 'lodash'
import Api from '.';

// const API_ROOT = 'http://localhost:3000/api'
// Casa
// const API_ROOT = 'http://192.168.100.68:3000/api'
// Pais exp://192.168.0.56:19000
const API_ROOT = 'http://192.168.0.56:3000/api'
// Tata
// const API_ROOT = 'http://192.168.15.7:3000/api'
// Chicao
// const API_ROOT = 'http://192.168.100.18:3000/api'


export class ApiService {
  private static defaultHeaders = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
  private static headers = ApiService.defaultHeaders as any;
  public static setHeaders(headers: any) {
    return ApiService.headers = _.defaults(
      headers,
      ApiService.defaultHeaders
    );
  }

  public static appendHeaders(headers: any) {
    ApiService.headers = _.defaults(
      headers,
      ApiService.headers);
    return ApiService.headers;
  }

  public static getHeaders() {
    return ApiService.headers;
  }

  protected defaultPath (parent?: any) {
    return '/'
  }

  protected loadRequestBody (value: any) {
    return value
  }

  protected loadRequestFilter (value: any) {
    return value
  }

  protected path (parent?: any): string {

    return this.defaultPath(parent)
  }

  protected pathSubPath (val?: string, parent?: any) {
    return (val ? `${this.path(parent)}${val}/` : this.path())
  }

  protected pathId (item: any, parent?: any): string {
    return `${this.path(parent)}${item._id || item}/`
  }

  protected cleanRequestFilter (body: any) {
    return _.pickBy(body, _.identity)
  }

  protected getAuthInfo(){

  }

  get(path: string, params?: any, config?: any) {
    return this.fetchApi(
      this.loadUrlParams(path , params),
      {
        method: 'GET',
        ...config
      })
  }
  
  patch(path: string, body?: any, config?: any) {
    return this.fetchApi(path,
      {
        method: 'PATCH',
        body,
        ...config
      }
    )
  }

  post(path: string, body?: any, config?: any) {
    return this.fetchApi(path,
      {
        method: 'POST',
        body,
        ...config
      }
    )
  }

  put(path: string, body?: any, config?: any) {
    return this.fetchApi(path,
      {
        method: 'PUT',
        body,
        ...config
      }
    )
  }

  delete(path: string, config?: any) {
    return this.fetchApi(path,
      {
        method: 'DELETE',
        ...config
      }
    )
  }

  fetchApi(path: string, config?: any) {
    let { headers, host, body, ...others} = (config || {})
    const url = this.getApiUrl(path, host)
    body = (typeof body === 'string') ? body : JSON.stringify(body)
    headers = _.defaults((headers || {}), ApiService.headers)
    const fetchParams = ({
      ...(headers ? {headers} : {}),
      ...(body ? {body} : {}),
      ...others
    })
    return fetch(url, fetchParams)
  }

  getApiUrl(path: string, host:string) {
    host = host || API_ROOT
    return `${host}/${path}`
  }

  loadUrlParams (path: string, obj?: any) {
    if (!obj) {
      return path
    }
    const keys = Object
      .keys(obj).filter(key => obj[key] !== undefined && obj[key] !== null)
    if (keys.length === 0) {
      return path
    }
    const params = keys
      .map(key => `${key}=${obj[key]}`)
      .join('&')
    return `${path}?${params}`
  }

}

export class Pagination {
  count?: number;
  limit?: number;
  page?: number;
  pages?: number;
  constructor (response?: any) {
    const { count, limit, page, pages } = (response || {})
    this.count = count || 0
    this.limit = limit || 0
    this.page = page || 0
    this.pages = pages || 0
  }
}

export class CollectionWithPaginationResponse {
  items: Array<any> = [];
  pagination?: Pagination = new Pagination();

  constructor (response?: any) {
    const { items, pagination } = (response || {})
    this.items = items || []
    this.pagination = new Pagination(pagination)
  }
}

export default new ApiService()
