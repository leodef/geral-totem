
import {
  CommonActions,
  StackActions
} from '@react-navigation/native'
// import { StackActions } from '@react-navigation/native';

export class NavService {
  navigator = null as any;
  isLoaded = false

  setNavigator(nav: any) {
    if (nav) {
      this.navigator = nav;
      this.isLoaded = true
    }
  }

  main(params: any) {
    const name =  params && params.tokem ? params.totem.tela || 'Tokem' : 'Login'
    return this.navigate({ name })
  }

  navigate(params: any) {
    const navigator  = this.navigator;
    if (navigator && params) {
      let action = CommonActions.navigate(params);
      navigator.dispatch(action);
    }
  }

  goBack() {
    const navigator  = this.navigator;
    if (navigator) {
      let action = CommonActions.goBack();
      navigator.dispatch(action);
    }
  }

  replace(params: any, source?: any, target?: any,) {
    const navigator  = this.navigator;
    if (navigator && params) {
      const { name, ...other } = params;
      let action = StackActions.replace(name, other.params || other);
      if( source || target) {
        navigator.dispatch({
          ...action,
          source,
          target
        });
      }
      navigator.dispatch(action)
    }
  }

  push(params: any) {
    const navigator  = this.navigator;
    const { name, ...other } = params;
    if (navigator) {
      let action = StackActions.push(name, other.params || other);
      navigator.dispatch(action);
    }
  }

  pop(params: any) {
    const navigator  = this.navigator;
    if (navigator) {
      const count  = Number(params ? params.count || params || 0 : 0);
      let action = StackActions.pop(count);
      navigator.dispatch(action);
    }
  }

  popToTop() {
    if (navigator) {
      const navigator  = this.navigator;
      let action = StackActions.popToTop();
      navigator.dispatch(action);
    }
  }
}
const navService = new NavService();
export default navService;