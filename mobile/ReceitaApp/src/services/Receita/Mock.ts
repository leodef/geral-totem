import { ApiService } from '../Api'
import { CollectionWithPaginationResponse } from '../Api/Api'
import { receitas } from '../../mock/Receita/receitas'


/**
 * ReceitaService, class representing a receitaentication service.
 */
export class ReceitaService extends ApiService {

  fetch (empresa?: any): Promise<any> {
    return Promise.resolve(
      new CollectionWithPaginationResponse({
        items: receitas,
        pagination: {
          count: receitas.length,
          limit: receitas.length,
          page: 1,
          pages: 1
        }
      })
  )
  }

  find (id: any, empresa?: any): Promise<any> {
    return Promise.resolve(receitas[0])
  }

}

export default new ReceitaService()