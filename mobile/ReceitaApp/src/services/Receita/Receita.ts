import { ApiService } from '../Api'
import { CollectionWithPaginationResponse } from '../Api/Api'

/**
 * ReceitaService, class representing a receitaentication service.
 */
export class ReceitaService extends ApiService {

  protected path (parent?: any): string {
    const empresa = parent && parent.user ? parent.user.empresa : null
    if (empresa && empresa._id) {
      return this.empresaPath(empresa)
    }
    return this.defaultPath()
  }

  protected empresaPath (empresa: any): string {
    return `modulo/admin/empresa/${empresa._id}/receita/`
  }

  protected defaultPath (): string {
    return 'modulo/admin/receita/'
  }

  fetch (parent?: any): Promise<any> {
    const path = this.pathSubPath('fetch', parent)
    return this.post( path, {})
    .then((response: any) => response.json())
    .then((response: any) =>
      new CollectionWithPaginationResponse(response)
    )
  }

  find (id: any, parent?: any): Promise<any> {
    const url = this.pathId(id, parent)
    return this.get(url)
  }

}

export default new ReceitaService()