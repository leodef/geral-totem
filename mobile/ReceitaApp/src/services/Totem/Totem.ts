import { ApiService } from '../Api'
import { CollectionWithPaginationResponse } from '../Api/Api'
import { AsyncStorage } from 'react-native'

/**
 * TotemService, class representing a receitaentication service.
 */
export class TotemService extends ApiService {

  static getTotem() {
    return AsyncStorage.getItem('totem')
  }

  static setTotem(value: any) {
    return AsyncStorage.setItem(
      'totem',
      JSON.stringify(value)
    ).then(() => value)
  }

  static removeTotem() {
    return AsyncStorage.removeItem('totem')
  }

  protected path (parent?: any): string {
    const empresa = parent && parent.user ? parent.user.empresa : null
    if (empresa && empresa._id) {
      return this.empresaPath(empresa)
    }
    return this.defaultPath()
  }

  protected empresaPath (empresa: any): string {
    return `modulo/admin/empresa/${empresa._id}/totem/`
  }

  protected defaultPath (): string {
    return 'modulo/admin/totem/'
  }

  update (value: any, parent?: any): Promise<any> {
    const url = this.path(parent)
    return this.put(url, this.loadRequestBody(value))
  }

  fetch (parent?: any): Promise<any> {
    const path = this.pathSubPath('fetch', parent)
    return this.post(path, {})
    .then((response: any) => response.json())
    .then((response: any) =>
      new CollectionWithPaginationResponse(response)
    )
  }

  // storage -> reducer (app)
  load() {
    return TotemService.getTotem()
  }

  // (app) value -> storage
  set(totem: any) {
    return TotemService.setTotem(totem)
  }

}

export default new TotemService()