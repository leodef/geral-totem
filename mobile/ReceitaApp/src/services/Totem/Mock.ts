import { ApiService } from '../Api'
import { AsyncStorage } from 'react-native'
import { totems } from '../../mock/Totem/totem'

/**
 * TotemService, class representing a receitaentication service.
 */
export class TotemService extends ApiService {

  static getTotem() {
    return AsyncStorage.getItem('totem')
  }

  static setTotem(value: any) {
    return AsyncStorage.setItem(
      'totem',
      JSON.stringify(value)
    ).then(() => value)
  }

  static removeTotem() {
    return AsyncStorage.removeItem('totem')
  }

  fetch (value: any, empresa?: any): Promise<any> {
    return Promise.resolve(totems) // (cliente)
  }

  update (value: any, empresa?: any): Promise<any> {
    return Promise.resolve(value)
  }

  // storage -> reducer (app)
  load() {
    return TotemService.getTotem()
  }

  // (app) value -> storage
  set(totem: any) {
    return TotemService.setTotem(totem)
  }

}

export default new TotemService()