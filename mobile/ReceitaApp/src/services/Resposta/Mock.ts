import { ApiService } from '../Api'
// import { respostas } from '../../mock/Resposta/resposta'

/**
 * RespostaService, class representing a receitaentication service.
 */
export class RespostaService extends ApiService {

  create (value: any, parent?: any): Promise<any> {
       return Promise.resolve(value)
  }
}

export default new RespostaService()