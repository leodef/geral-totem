import { ApiService } from '../Api'

/**
 * RespostaService, class representing a receitaentication service.
 */
export class RespostaService extends ApiService {

  protected path (parent?: any) {
    const empresa = parent && parent.user ? parent.user.empresa : null
    const totem = parent ? parent.totem : null
    if (empresa && empresa._id && totem && totem._id) {
      return this.empresaPath(empresa, totem)
    }
    return this.defaultPath()
  }

  protected empresaPath (empresa: any, totem?: any): string {
    return `modulo/admin/empresa/${empresa._id}/totem/${totem._id}/resposta/`
  }

  protected defaultPath (): string {
    return 'modulo/admin/resposta/'
  }
  
  create (value: any, parent?: any): Promise<any> {
    const url = this.path(parent)
    return this.post(url, this.loadRequestBody(value))
  }


}

export default new RespostaService()