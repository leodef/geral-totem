import { Platform, Dimensions } from 'react-native';
const windowDimensions = Dimensions.get('window');


const constants = {
  WIDTH: windowDimensions.width,
  HEIGHT: windowDimensions.height,
  IMAGE_LOGO_HEIGHT: windowDimensions.width / 4,
  IMAGE_LOGO_HEIGHT_SMALL: windowDimensions.width / 12,
  IS_ENV_DEVELOPMENT: __DEV__,
  IS_ANDROID: Platform.OS === 'android',
  IS_IOS: Platform.OS === 'ios',
  IS_DEBUG_MODE_ENABLED: Boolean(window.navigator.userAgent)
};

export default constants;
