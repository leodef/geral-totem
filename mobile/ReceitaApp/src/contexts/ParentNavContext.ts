import React from 'react'

export class ParentNavContextValue {
  onBackPress: any
}
const parentNavContextValueDefault: ParentNavContextValue = {
  onBackPress: null
}

const ParentNavContext = React.createContext(parentNavContextValueDefault)

export { ParentNavContext }

