/*
  Empresa
    nome: string
    desc: string
    cnpj: string
*/
export class Empresa {
  nome: String | null = null;
  desc: String | null = null;
  cnpj: String | null = null;
}

