
export enum TotemType {
  FETCH = 'TOTEM_FETCH',
  FETCH_PENDING = 'TOTEM_FETCH_PENDING',
  FETCH_SUCCESS = 'TOTEM_FETCH_SUCCESS',
  FETCH_FAILURE = 'TOTEM_FETCH_FAILURE',
  UPDATE = 'TOTEM_UPDATE',
  UPDATE_PENDING = 'TOTEM_UPDATE_PENDING',
  UPDATE_SUCCESS = 'TOTEM_UPDATE_SUCCESS',
  UPDATE_FAILURE = 'TOTEM_UPDATE_FAILURE',
  LOAD = 'TOTEM_LOAD',
  LOAD_SUCCESS = 'TOTEM_LOAD_SUCCESS',
  SET = 'TOTEM_SET',
  SET_SUCCESS = 'TOTEM_SET_SUCCESS',
}
/*
  Totem
    codigo: string
    titulo: string
    desc: string
    status: string
    empresa: string
*/
export class Totem {
  codigo: String | null = null;
  titulo: String | null = null;
  desc: String | null = null;
  status: Number | null = null;
  empresa: String | null = null
}


export const getOnSetNav = (totem: any) => ({ name: 'Receitas', params: { totem }})