export enum LoadingType {
    START = 'LOADING_START',
    STOP = 'LOADING_STOP'
}