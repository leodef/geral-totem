export enum MessageType {
    SHOW = 'MESSAGE_SHOW',
    ADD = 'MESSAGE_ADD',
    REMOVE = 'MESSAGE_REMOVE'
}

export enum MessageVariant {
    primary = 'primary',
    secondary = 'secondary',
    success = 'success',
    danger = 'danger',
    warning= 'warning',
    info= 'info',
    light = 'light',
    dark = 'dark',
}
