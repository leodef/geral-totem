
import {
  // eslint-disable-next-line no-unused-vars
  Receita
} from './Receita'
// eslint-disable-next-line no-unused-vars
import {
  // eslint-disable-next-line no-unused-vars
  RespostaTotem
} from './RespostaTotem'

/*
  ReceitaRespostaRespostaTotem
    resposta: RespostaRespostaTotem
    receita: Receita
*/

export class ReceitaRespostaRespostaTotem {
  resposta?: RespostaTotem
  receita?: Receita
}

export const initialValues = {
  resposta: '',
  receita: ''
}
