export enum AuthType {
    LOAD = 'AUTH_LOAD',
    LOAD_SUCCESS = 'AUTH_LOAD_SUCCESS',
    AUTH = 'AUTH',
    AUTH_PENDING = 'AUTH_PENDING',
    AUTH_SUCCESS = 'AUTH_SUCCESS',
    AUTH_FAILURE = 'AUTH_FAILURE',
    LOGOUT = 'AUTH_LOGOUT'
}

export const loginNav = { name: 'Totem'};
export const logoutNav = { name: 'Login'};