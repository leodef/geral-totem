// eslint-disable-next-line no-unused-vars
import { Midia } from './Midia'
// eslint-disable-next-line no-unused-vars
import { Empresa } from './Empresa'

/*
  Receita
    titulo: string
    desc: string
    pagina: string // gravar uma pagina em html
    video: Midia
    image: Midia
    empresa: Empresa
*/

export enum ReceitaType {
  FETCH = 'RECEITA_FETCH',
  FETCH_PENDING = 'RECEITA_FETCH_PENDING',
  FETCH_SUCCESS = 'RECEITA_FETCH_SUCCESS',
  FETCH_FAILURE = 'RECEITA_FETCH_FAILURE',
  FIND = 'RECEITA_FIND',
  FIND_PENDING = 'RECEITA_FIND_PENDING',
  FIND_SUCCESS = 'RECEITA_FIND_SUCCESS',
  FIND_FAILURE = 'RECEITA_FIND_FAILURE',
}

export class Receita {
  titulo: String | null = null;
  desc: String | null = null;
  pagina: String | null = null;
  video: Midia | null = null;
  image: Midia | null = null;
  empresa: Empresa | null = null;
}
