import * as Yup from 'yup'
import {
  ClienteSchema,
  initialValues as clienteInitialValues,
  // eslint-disable-next-line no-unused-vars
  Cliente
} from './Cliente'
// eslint-disable-next-line no-unused-vars
import { Totem } from './Totem'

/*
  RespostaTotem
    totem: Totem
    cliente: Cliente
*/

export class RespostaTotem {
  totem?: Totem
  cliente?: Cliente
}

export const RespostaTotemSchema = Yup.object().shape({
  totem: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  cleinte: ClienteSchema
})

export const initialValues = {
  totem: '',
  cliente: clienteInitialValues
}
