import { Cliente } from './Cliente'
import { Receita } from './Receita'

export enum RespostaType {
  CREATE = 'RESPOSTA_CREATE',
  CREATE_PENDING = 'RESPOSTA_CREATE_PENDING',
  CREATE_SUCCESS = 'RESPOSTA_CREATE_SUCCESS',
  CREATE_FAILURE = 'RESPOSTA_CREATE_FAILURE',
}

export class Resposta {
    cliente?: Cliente
    receita?: Receita
}


export const createNav = { name: 'Receitas'};
