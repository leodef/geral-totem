import * as Yup from 'yup';

export const LoginSchema = Yup.object().shape({
  usuario: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  senha: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
});


export const initialValues = {
    usuario: '',
    senha: ''
  }