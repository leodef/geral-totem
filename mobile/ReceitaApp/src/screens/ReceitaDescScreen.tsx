import * as React from 'react'
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  BackHandler,
  KeyboardAvoidingView,
  ImageBackground,
  Alert,
} from 'react-native'
import {
  WebView
} from 'react-native-webview';
import {
  ScrollView
} from 'react-native-gesture-handler'
import {
  useCallback,
  useContext,
  useMemo,
  useEffect,
  useState,
  useRef
} from 'react';
import {
  Card,
  Button
} from 'react-native-elements';
import TextAvatar from 'react-native-text-avatar'
import { useFocusEffect } from '@react-navigation/native';
import backgroundImage from '../assets/images/bg.png';
import { ParentNavContext } from '../contexts/ParentNavContext';
import constants from '../config/constants';

const TIMER_DURATION = 20
export const ReceitaDescScreen = (props: any) => {
  const scrollRef = useRef(null as any)
  const scalesPageToFit = Platform.OS === 'android';
  const { onBackPress } = useContext(ParentNavContext)
  const { navigation, route } = props
  const { receita, image } = route.params || {} as any
  const [timer, setTimer] = useState(TIMER_DURATION)

  // hardwareBackPress 
  useFocusEffect(
    useCallback(() => {
      const onBackPressFunc = (event?:any) => {
        if(onBackPress) {
          onBackPress(event)
          return true
        }
        return false
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPressFunc);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPressFunc);
    }, [])
  );

  const receberReceita = useCallback(
    (event?: any) =>  navigation.navigate('Cadastro'),
    [navigation, receita],
  )

  const receberReceitaAlert = useCallback( (event?: any) => {
      Alert.alert(
        'Receber receita',
        'Deseja receber esta receita ?',
        [
          {
            text: 'Não',
            onPress: () => onBackPress(true),
            style: 'cancel'
          },
          { text: 'Sim', onPress: () => receberReceita() }
        ],
        { cancelable: false }
      );
    },[receberReceita, onBackPress])

  // timer
  const decrementTimer = useCallback(
    () => setTimer(timer > 0 ? timer - 1 : 0),
    [setTimer])

  useEffect(() => {
    setTimeout(() => decrementTimer(), 1000)
  }, [decrementTimer]);

  // back on timer finish
  useEffect(() => {
    if(timer === 0) {
      onBackPress(true)
    }
  }, []);

  /*
  const webViewScript = `
    setTimeout(function() { 
      window.postMessage(document.documentElement.scrollHeight); 
    }, 500);
    true; // note: this is required, or you'll sometimes get silent failures
  `;
  */

  const resetTimer = useCallback(() => setTimer(TIMER_DURATION), [setTimer] )

  const receitaImage = React.useMemo(() =>
    image  ?
      (<Image
        source={{ uri: image.uri }} />) :
      (<TextAvatar
        size={50}
        type={'circle'}>
          {receita.titulo || ''}
      </TextAvatar>)
    , [receita, styles])

  // title=0&byline=0&portrait=0
  // style="white-space: pre-wrap; font-size:2em;">
  const pagina = useMemo(() => 
    `
    <html>
      <head>
        <meta
          name="viewport"
          content="width=device-width,
          initial-scale=1.0">
      </head>
      <body>
        <div
          style="font-size:2em;">
          ${receita.pagina}
        </div>
      </body>
    </html>
  `, [receita])
  return (
    <ImageBackground
      source={backgroundImage}
      style={styles.imageBackground}>
        {/*
            style={styles.scrollView}
          */}
        {/*<ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          ref={scrollRef}
          >
        */}
            {/*onTouchEnd={(event: any) => receberReceitaAlert(event)}*/}
          <Card title={receita.titulo}>
            <Text>
              { receita.desc }
            </Text>
            <View style={styles.image}>
              { receitaImage }
            </View>
          </Card>

          <Card>
            <Button
              title="Receber receita"
              type="clear"
              onPress={(event: any) => receberReceita(event)}
            />
          </Card>
            <View style={styles.page}>
              <WebView
                source={{ html: pagina }}
                originWhitelist={['*']}
                style={{height: 100}}
                />
            </View>
            {/*

              automaticallyAdjustContentInsets={false}

              scrollEnabled={true}

              scalesPageToFit={scalesPageToFit}
              bounces={false}
              scrollEnabled={false}
              showsHorizontalScrollIndicator={false}
              originWhitelist={['*']}


              // Resize
              style={{height: this.state.webheight}}
              automaticallyAdjustContentInsets={false}
              scrollEnabled={false}
              source={{uri: "http://<your url>"}}
              onMessage={event => {
                this.setState({webheight: parseInt(event.nativeEvent.data)});
              }}
              javaScriptEnabled={true}
              injectedJavaScript ={webViewScript}
              domStorageEnabled={true}
            */}
          {/*
            <Card>
              <Button
                title="Voltar"
                type="clear"
                onPress={(event: any) => onBackPress(event)}
              />
            </Card>
          */}
          {/*
            </ScrollView>
          */}
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexGrow: 1
  },
  scrollView: {
    marginHorizontal: 20,
  },
  page: {
    // height: 100,
    margin: 15,
    // marginTop: 20,
    minHeight: 300,
    // overflow: 'scroll'
  },
  imageBackground: {
    flex: 1,
    //width: '100%',
    //height: '100%',
    // resizeMode: 'cover',
    // justifyContent: 'center'
  },
  image: {
    flex: 1,
    width: '100%',
    // resizeMode: 'cover',
    justifyContent: 'center'
  },
  button: {
    width: constants.WIDTH -55,
    height: 45,
    borderRadius: 25,
    fontSize: 16
  }
})
