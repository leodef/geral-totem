import * as React from 'react'
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  BackHandler,
} from 'react-native'
import {
  Icon
} from 'react-native-elements'
import { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import TextAvatar from 'react-native-text-avatar'
import { NavigationContainer, useFocusEffect } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavType } from '../types/Nav';
import { ReceitaDescScreen } from './ReceitaDescScreen';
import { ReceitaVideoScreen } from './ReceitaVideoScreen';
import { CadastroClienteScreen } from './CadastroClienteScreen';
import { ParentNavContext } from '../contexts/ParentNavContext'
import { ImageHeader } from '../components/ImageHeader';

const Tab = createBottomTabNavigator();

export const ReceitaScreen = (props: any) => {
  const { navigation, route } = props
  const { receita } = useMemo(
    () => (route.params || {} as any),
    [route])

  const { video, image } = useMemo(
    () => (receita || {} as any),
    [route])

  const dispatch = useDispatch()

  // Back 
  const onBackPress = useCallback((event?:any) => {
    dispatch({
      type: NavType.NAVIGATE,
      payload: { name: 'Receitas'}
    })
    return true
  }, [ dispatch ])

  // Back 
  useFocusEffect(
    useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

  const initialParams = useMemo(() =>
    ({
      receita,
      video,
      image
    })
  , [receita, onBackPress])

  const parentNavContextValue = useMemo(()=> ({ onBackPress }),[ onBackPress ])

  return (
    <ParentNavContext.Provider value={parentNavContextValue}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: (props: any) => <TabBarIcon {...props} route={route} />
        })}>
        <Tab.Screen
          name="Desc"
          component={ReceitaDescScreen}
          initialParams={initialParams} />
        <Tab.Screen
          name="Video"
          component={ReceitaVideoScreen}
          initialParams={initialParams} />
        <Tab.Screen
          name="Cadastro"
          component={CadastroClienteScreen}
          initialParams={initialParams} />
      </Tab.Navigator>
    </ParentNavContext.Provider>)
}

export const TabBarIcon = (props: any)  => {
  const { focused, color, size, route } = props
  const iconName = useMemo(() => {
    switch( route.name ) {
      case 'Desc': // <ion-icon name='information-circle-outline'></ion-icon>
        return focused
        ? 'ios-information-circle'
        : 'ios-information-circle-outline';
      case 'Video': // <ion-icon name='film-outline'></ion-icon>
        return 'ios-film';
      case 'Cadastro': // <ion-icon name='people-outline'></ion-icon>
        return focused ? 'ios-people' : 'ios-people';
      default:
        return null;
    }
  }, [route, focused])

  return (iconName? <Icon
    name={iconName}
    type='ionicon'
    size={size}
    color={color} 
  /> : null)
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center'
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)'
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center'
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center'
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center'
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7'
  }
})
