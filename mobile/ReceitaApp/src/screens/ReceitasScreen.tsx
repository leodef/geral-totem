
import * as React from 'react'
import {
  FlatList,
  Text,
  SafeAreaView, 
  StatusBar,
  StyleSheet
} from 'react-native'
import { ListItem, AvatarProps, Avatar } from 'react-native-elements'
import { useEffect, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ReceitaType } from '../types/Receita'
import { NavType } from '../types/Nav'
import TextAvatar from 'react-native-text-avatar'

export const ReceitasScreen = (props: any) => {

  // Selector
  const auth = useSelector( (state: any) => state.auth)
  const items = useSelector((state: any) => state.receita.items || [])

  // Dispatch
  const dispatch = useDispatch()

  // Others
  /*
    const cadastrar = useCallback(() =>
      dispatch({
        type: NavType.PUSH,
        payload: {
          name: 'CadastroCliente'
        }
    }), [dispatch])
  */

  // Select
  const select = useCallback((receita: any) => {
    dispatch({
      type: NavType.PUSH,
      payload: {
        name: 'Receita',
        params: { 
          receita
        }
      }
    })
  }, [dispatch])

  const onPress = useCallback((item: any) => {
      select(item)
  }, [select])


  // Fetch
  const fetch = useCallback(() => {
    if (auth) {
      dispatch({ type: ReceitaType.FETCH, payload: auth })
    }
  }, [dispatch, auth])

  useEffect( () => {
    fetch()
  }, [])

  // List Utils
  const keyExtractor = useCallback(
    (item: any, index: number) => index.toString(),
    [])

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        keyExtractor={keyExtractor}
        data={items}
        renderItem={ (itemProps: any) =>
          <ReceitasScreenItem {...itemProps} onPress={onPress} />
        }
      />
    </SafeAreaView>)
}


export const ReceitasScreenItem = (props: any) => {
  const { item, onPress } = props
  const { imagem, titulo } = (item || {} as any )
  const source: any = (imagem ?
    { uri: (imagem.uri || imagem) } :
    undefined
  ) 
  const title: string = (titulo ?
    titulo.substring(0 , 2).toUpperCase() :
    undefined)

  const ImageSrc = (source ?
    (<Avatar
      size="small"
      rounded
      title={title}
      source={source} />)
   : (
    <TextAvatar
      size={50}
      type={'circle'}>
      {item.titulo}
    </TextAvatar>)
  )
  return (<ListItem
    title={item.titulo}
    subtitle={item.desc}
    leftAvatar={ImageSrc}
    bottomDivider
    chevron
    onPress={(event: any) => onPress(item)}
  />)
}



export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});