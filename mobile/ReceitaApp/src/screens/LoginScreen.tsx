import * as React from 'react'
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  Platform,
  NativeSyntheticEvent,
  TextInputFocusEventData,
  KeyboardAvoidingView,
  StatusBar,
  Image,
  Animated,
  Keyboard,
  Dimensions,
  ImageBackground
} from 'react-native'
import { withFormik } from 'formik'
import FormTextInput from '../components/FormTextInput';
import Button from '../components/Button';
import logoImage from '../assets/images/photo.png';
import backgroundImage from '../assets/images/bg.png';
import colors from '../config/colors';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useMemo, useEffect } from 'react';
import { AuthType } from '../types/Auth';
// https://github.com/mmazzarolo/the-starter-app
import constants from '../config/constants';

export const LoginScreen = (props: any) => {

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const login = useCallback((item: any) =>
    dispatch({
      type: AuthType.AUTH,
      payload: item
    }), [dispatch])

  const Form = withFormik({
      mapPropsToValues: () => ({
        username: '',
        password: '',
      }),
      handleSubmit: (values: any) => {
        login(values)
      }
    })(LoginFormBody)
  return (<Form {...props} />)
}
export const LoginFormBody = (props: any) => {
  
  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    values 
  } = props
  

  const imageHeight = useMemo(() => new Animated.Value(constants.IMAGE_LOGO_HEIGHT), [])

  const keyboardWillShow = useCallback((event) => {
    Animated.timing(imageHeight, {
      duration: event.duration,
      toValue: constants.IMAGE_LOGO_HEIGHT_SMALL,
    }).start();
  }, [])

  const keyboardWillHide = useCallback((event) => {
    Animated.timing(imageHeight, {
      duration: event.duration,
      toValue: constants.IMAGE_LOGO_HEIGHT,
    }).start();
  }, [])

  useEffect( () => {
    const keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', keyboardWillShow);
    const keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', keyboardWillHide);
	  return () => {
      keyboardWillShowSub.remove();
      keyboardWillHideSub.remove();
	}
  }, [])

  return (
    <ImageBackground
      source={backgroundImage}
      style={styles.imageBackground}>
      <KeyboardAvoidingView
          style={styles.container}
          // On Android the keyboard behavior is handled
          // by Android itself, so we should disable it
          // by passing `undefined`.
          behavior={constants.IS_IOS ? 'padding' : undefined}
        >
          <StatusBar
            barStyle="dark-content"
            backgroundColor="#FFFFFF"
          />

          <Animated.Image
            source={logoImage}
            style={[
              styles.logo,
              { height: imageHeight }
            ]} />
          {
            /*
          <Image
            source={logoImage}
            style={styles.logo} />
            */
          }
          <View
            style={styles.form}>

        <FormTextInput
          onChangeText={handleChange('username')}
          onBlur={handleBlur('username')}
          value={values.username}
          error={errors.username}
          inputProps= {{
            style: styles.input
          }}
          placeholder="Username"
          underlineColorAndroid="transparent"
          placeholderTextColor="rgba(255, 255, 255, 0.7)"
        
        />

        <FormTextInput
          onChangeText={handleChange('password')}
          onBlur={handleBlur('password')}
          textContentType="password"
          value={values.password}
          error={errors.password}
          inputProps= {{
            style: styles.input
          }}
          placeholder="Senha"
          underlineColorAndroid="transparent"
          placeholderTextColor="rgba(255, 255, 255, 0.7)"
        />

        <Button
          onPress={handleSubmit}
          label="Login"
        />
      </View>
      </KeyboardAvoidingView>
    </ImageBackground>
  )
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: colors.WHITE,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  logo: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    width: '80%'
  },
  imageBackground: {
    flex: 1,
    width: '100%',
    height: '100%',
    // resizeMode: 'cover',
    // justifyContent: 'center'
  },
  input: {
    width: constants.WIDTH -55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    color: 'rgba(225, 225, 225, 0.7)'
  },
  button: {
    width: constants.WIDTH -55,
    height: 45,
    borderRadius: 25,
    fontSize: 16
  }
});
// paddingLeft: 45,
// marginHorizontal: 25

