import * as React from 'react'
import {
  View,
  Text,
  StyleSheet,
  BackHandler,
  KeyboardAvoidingView,
  ImageBackground,
  ScrollView
} from 'react-native'
import { withFormik, useField } from 'formik'
import {
  useContext,
  useEffect,
  useCallback,
  useRef,
  useMemo
} from 'react'
import { Card, Button } from 'react-native-elements'
import { useDispatch, useSelector } from 'react-redux'
import { useFocusEffect } from '@react-navigation/native'
import { RespostaType } from '../types/Resposta'
import FormTextInput from '../components/FormTextInput';
import { ParentNavContext } from '../contexts/ParentNavContext'
import constants from '../config/constants'
import backgroundImage from '../assets/images/bg.png';

const TIMER_DURATION = 20;
export const CadastroClienteScreen = (props: any)  => {
  const {
    route
  } = props
  const receita = useMemo(() => route.params ?  route.params.receita : null, [route])
  const auth = useSelector( (state: any) => state.auth)
  const totem = useSelector( (state: any) => state.totem.item)

  // hardwareBackPress 
  const { onBackPress } = useContext(ParentNavContext)
  useFocusEffect(
    useCallback(() => {
      const onBackPressFunc = (event?:any) => {
        if(onBackPress) {
          onBackPress(event)
          return true
        }
        return false
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPressFunc);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPressFunc);
    }, [])
  );

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const create = useCallback((cliente: any) =>
    dispatch({
      type: RespostaType.CREATE,
      payload: {
        parent: {
          ...auth,
          totem
        },
        item: {
          cliente,
          receita: (receita ? receita._id : null)
        }
      }
    }), [dispatch])

  const Form = withFormik({
    mapPropsToValues: () => ({
      nome: '',
      email: '',
      telefone: ''
    }),
    handleSubmit: (values: any) => {
      create(values)
    }
  })(CadastroClienteFormBody)
  //  onBackPress={onBackPress} 
  return (<Form
    {...props}
    receita={receita} />);
}

export function CadastroClienteFormBody (props: any) {
  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    values,
    navigation,
    receita
  } = props

  const sendLabel = useMemo( () =>
    receita ?
    'Enviar receita' :
    'Enviar informações'
  , [receita])

  return (
    <ImageBackground
      source={backgroundImage}
      style={styles.imageBackground}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={constants.IS_IOS ? 'padding' : undefined}>
        <Card title="Informações para envio">
          <View>

            <FormTextInput
              onChangeText={handleChange('nome')}
              onBlur={handleBlur('nome')}
              value={values.nome}
              error={errors.nome}
              placeholder="Nome"
            />

            <FormTextInput
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              value={values.email}
              error={errors.email}
              placeholder="Email"
            />

            <FormTextInput
              onChangeText={handleChange('telefone')}
              onBlur={handleBlur('telefone')}
              value={values.telefone}
              error={errors.telefone}
              placeholder="Telefone"
            />

          </View>
        </Card>
                
        <Card>
          <View style={{marginBottom:10}}>
            <Button
              onPress={handleSubmit}
              title={sendLabel}
              type="outline"
            />
          </View>

          <View style={{marginBottom:10}}>
            <Text>Receita enviada: </Text>
          </View>
            <Button
              onPress={(event: any) => navigation.navigate('Desc')}
              title={receita ? receita.titulo : ''}
              type="clear"
            />
        </Card>
      </KeyboardAvoidingView>

    </ImageBackground>)
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  btnEye: {
    position: 'absolute',
    top: 55,
    right: 28,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    width: '80%'
  },
  imageBackground: {
    flex: 1,
    width: '100%',
    height: '100%',
    // resizeMode: 'cover',
    // justifyContent: 'center'
  },
  button: {
    width: constants.WIDTH -55,
    height: 45,
    borderRadius: 25,
    fontSize: 16
  },
  scrollView: {
    marginHorizontal: 20,
  }
});
