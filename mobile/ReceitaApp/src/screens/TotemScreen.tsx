import * as React from 'react'
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  Platform,
  NativeSyntheticEvent,
  TextInputFocusEventData,
  KeyboardAvoidingView,
  StatusBar,
  Image,
  Animated,
  Keyboard,
  Dimensions,
  ImageBackground,
  SectionList,
  SafeAreaView,
  FlatList
} from 'react-native'
import { withFormik } from 'formik'
import FormTextInput from '../components/FormTextInput';
import Button from '../components/Button';
import logoImage from '../assets/images/photo.png';
import backgroundImage from '../assets/images/bg.png';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useMemo, useEffect, useState } from 'react';
// https://github.com/mmazzarolo/the-starter-app
import constants from '../config/constants';
import { TotemType } from '../types/Totem';
import { NavType } from '../types/Nav';
import { ListItem } from 'react-native-elements';

export const TotemScreen = (props: any) => {

  // Selector
  const auth = useSelector( (state: any) => state.auth)
  const items = useSelector((state: any) => (state.totem.items || []) )

  // Dispatch
  const dispatch = useDispatch()

  // Select
  const select = useCallback((totem: any) =>
    dispatch({
      type: TotemType.SET,
      payload: totem
  }), [dispatch])

  const onPress = useCallback((item: any) => {
      select(item)
  }, [select])

  // Fetch
  const fetch = useCallback(() => {
    if (auth) {
      dispatch({ type: TotemType.FETCH, payload: auth })
    }
  }, [dispatch, auth])

  useEffect( () => {
    fetch()
  }, [])

  // List Utils
  const keyExtractor = useCallback(
    (item: any, index: number) => index.toString(),
    [])

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        keyExtractor={keyExtractor}
        data={items}
        renderItem={ (itemProps: any) =>
          <TotemScreenItem {...itemProps} onPress={onPress} />
        }
      />
    </SafeAreaView>)
};


export const TotemScreenItem = (props: any) => {
  const { item, onPress } = props
  const { imagem, titulo } = (item || {} as any)
  return (<ListItem
    title={item.titulo}
    subtitle={item.desc}
    bottomDivider
    chevron
    onPress={(event: any) => onPress(item)}
  />)
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

