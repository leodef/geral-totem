export const user = {
  _id:'5f03e6c129dbdc5b9802c71d',
  usuario:'softpar',
  nome:'Softpar',
  status:'criado',
  empresa: {
    _id:'5eff6fd2e7530867ac4e2e35',
    nome:'Empresa Ltda',
    desc:'Empresa Ltda Desc',
    cnpj:'51.526.099/0001-09',
    createdAt:'2020-07-03T17:50:10.924Z',
    updatedAt:'2020-07-03T17:50:10.924Z'
  },
  createdAt:'2020-07-07T03:06:41.398Z',
  updatedAt:'2020-07-07T03:06:51.822Z'
};

/*
{
  '_id': {
      '$oid': '5f053fe3a0a63b5868488f8e'
  },
  'nome': 'Joao Silva',
  'cpf': '388.864.620-00',
  'email': 'joao.silva@email.com',
  'telefone': '41984774789',
  'empresa': {
      '$oid': '5eff6fd2e7530867ac4e2e35'
  },
  'totem': {
      '$oid': '5f04f582a0a63b5868488f8d'
  },
  'createdAt': {
      '$date': '2020-07-08T03:39:15.533Z'
  },
  'updatedAt': {
      '$date': '2020-07-08T03:39:15.533Z'
  },
  '__v': 0,
'empresa': {
  '_id': {
    '$oid': '5eff6fd2e7530867ac4e2e35'
  },
  'nome': 'Empresa Ltda',
  'desc': 'Empresa Ltda Desc',
  'cnpj': '51.526.099/0001-09',
  'createdAt': {
    '$date': '2020-07-03T17:50:10.924Z'
  },
  'updatedAt': {
    '$date': '2020-07-03T17:50:10.924Z'
  },
  '__v': 0
}
}
*/