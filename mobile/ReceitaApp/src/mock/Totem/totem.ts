/*
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
 */
export const totems = [
  {
    "_id": "5f04f582a0a63b5868488f8d",
    "codigo":"TJ1",
    "titulo":"Totem Jacomar 1",
    "desc":"Totem Jacomar do CIC 1",
    "status":"criado",
    "empresa": "5eff6fd2e7530867ac4e2e35" ,
    "createdAt": "2020-07-07T22:21:54.830Z",
    "updatedAt": "2020-07-07T22:21:54.830Z",
    "__v":0
 }
]