/*
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
 */
export const receitas = [
    {
      _id:'5f06878dd863408280fc73ea',
      titulo:'Bolo se carne',
      desc:'Receita de bolo se carne',
      pagina:`Rocambole de carne moída é a receita
        perfeita para um almoço rápido e gostoso!
        Ele ainda é recheado com queijo e presunto
        para uma refeição muito mais saborosa.
        Experimente! Ingredientes: 200 g de bacon 500 g
        de carne moída 1/2 cebola 1/4 de xícara de 
        cheiro-verde sal a gosto pimenta-do-reino a
        gosto 200 g de presunto 200 g de mussarela 
        molho barbecue Para conferir a receita completa,
        clique aqui: https://goo.gl/bsnbYn Siga o 
        TudoGostoso nas redes sociais:
        Facebook: http://bit.ly/261LOeh 
        Instagram: http://bit.ly/1OuDM5U
        Twitter: http://bit.ly/1Yx6Rje
        Pinterest: http://bit.ly/1OuDhZG`,
      empresa: {
        _id:'5eff6fd2e7530867ac4e2e35',
        nome:'Empresa Ltda',
        desc:'Empresa Ltda Desc',
        cnpj:'51.526.099/0001-09',
        createdAt:'2020-07-03T17:50:10.924Z',
        updatedAt:'2020-07-03T17:50:10.924Z'
      },
      image: {
        _id:'5f066c2ed863408280fc73e7',
        titulo:'Imagem bolo de carne',
        nomeArquivo:'298322_original.jpg',
        mime:'image/jpeg',
        uri:'https://img.itdg.com.br/images/recipes/000/001/598/298322/298322_original.jpg',
        extensao:'jpg',
        createdAt:'2020-07-09T01:00:30.935Z',
        updatedAt:'2020-07-09T01:00:30.935Z'
      },
      video: {
          _id:'5f066c2ed863408280fc73e8',
          titulo:'Teste',
          nomeArquivo:'5f066c2ed863408280fc73e8.mp4',
          mime:'video/mp4',
          uri:'https://youtu.be/4a0Pbx0_BBA',
          extensao:'mp4',
          createdAt:'2020-07-09T01:00:30.935Z',
          updatedAt:'2020-07-09T01:00:30.935Z'
       },
       createdAt:'2020-07-09T02:57:17.214Z',
       updatedAt:'2020-07-09T02:57:17.214Z'
    },
    {
      _id:'5f06878dd863408280fc73ea',
      titulo:'BACALHAU DIVINO',
      desc:'Descrição da receita',
      pagina:`Primeiro faça o purê de batatas, esprema as batatas, e acrescente a manteiga e o leite com as batatas ainda bem quentes, é só mexer bem e acrescentar sal a gosto.
        Coloque esse purê num refratário alto.
        E arrrume como se fosse a massa de uma torta.
        Faça o refogado com o bacalhau já dessalgado (deixar de molho na véspera e trocar no mínimo 5 vezes a água).
        Coloque para dar uma breve fervura, 5 minutos em água já fervente.
        Depois desfie o bacalhau em lascas.
        Coloque bastante azeite numa panela e frite a cebola e o alho.
        Depois juntar os pimentões, o tomate, e deixar apurar por uns 10 minutos.
        Depois juntar as azeitonas e o bacalhau e deixar apurar mais 10 minutos.
        Sem deixar secar muito, (caso seque, coloque um pouquinho de água quente).
        E coloque orégano a gosto.
        Normalmente não é preciso colocar sal, pois o bacalhau já contém o suficiente.
        Mas, se precisar coloque um pouquinho.
        Joque esse refogado de bacalhau sobre o purê de batatas.
        CREME:
        Bata todos os ingredientes no liquidificador, e despeje sobre o bacalhau.
        Leve ao forno previamente aquecido (bem quente) e deixe gratinar.
        Sirva com arroz branco e uma salada de folhas.`,
      empresa:{
         _id:'5eff6fd2e7530867ac4e2e35',
         nome:'Empresa Ltda',
         desc:'Empresa Ltda Desc',
         cnpj:'51.526.099/0001-09',
         createdAt:'2020-07-03T17:50:10.924Z',
         updatedAt:'2020-07-03T17:50:10.924Z'
      },
      image:{
         _id:'5f066c2ed863408280fc73e9',
         titulo:'FotoBacalhau Divino',
         nomeArquivo:'4869_original.jpg',
         mime:'image/jpeg',
         uri:'https://img.itdg.com.br/tdg/images/recipes/000/024/645/4869/4869_original.jpg',
         extensao:'jpg',
         createdAt:'2020-07-09T01:00:30.935Z',
         updatedAt:'2020-07-09T01:00:30.935Z'
      },
      video: {
         _id:'5f066c2ed863408280fc73ea',
         titulo:'Receita Bacalhau',
         nomeArquivo:'5f066c2ed863408280fc73ea.mp4',
         mime:'video/mp4',
         uri:'https://youtu.be/nL1QF-YTBEI',
         extensao:'mp4',
         createdAt:'2020-07-09T01:00:30.935Z',
         updatedAt:'2020-07-09T01:00:30.935Z'
      },
      createdAt:'2020-07-09T02:57:17.214Z',
      updatedAt:'2020-07-09T02:57:17.214Z'
   }
  ]