import React, { useEffect, useRef, useMemo, useCallback } from 'react'
import { Provider, useSelector, useDispatch } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import configureStore from './configureStore'
import NavigationService from './src/services/Nav'
import { LoginScreen } from './src/screens/LoginScreen'
import { ReceitasScreen } from './src/screens/ReceitasScreen'
import { ReceitaScreen } from './src/screens/ReceitaScreen'
import { TotemScreen } from './src/screens/TotemScreen'
import { CadastroClienteScreen } from './src/screens/CadastroClienteScreen'
import { AuthType } from './src/types/Auth'
import { TotemType } from './src/types/Totem'
import { NavType } from './src/types/Nav'

const preloadedState = {} as any
const store = configureStore(preloadedState)

const Stack = createStackNavigator()

export const App =(props: any) => {
  return (
    <Provider store={store}>
      <AppContent {...props}/>
    </Provider>
  );
}

export const AppContent =(props: any) => {
  const ref = useRef(null);

  // dispatch
  const dispatch = useDispatch()

  // Navigator ref
  useEffect(() => {
    NavigationService.setNavigator(ref.current);
  }, [ref])

  const navigate = useCallback((payload: any) => {
    dispatch({
      type: NavType.PUSH,
      payload
    })
  }, [dispatch])

  // Carregamento de dados iniciais
  const loadAuth = useCallback(() =>
    dispatch({
      type: AuthType.LOAD
    }), [dispatch])

  const loadTotem = useCallback(() =>
    dispatch({
      type: TotemType.LOAD
    }), [dispatch])

  useEffect(() => {
    loadAuth();
  }, [ loadAuth ])

  useEffect(() => {
    loadTotem();
  }, [ loadTotem ])

  // Auth
  const token = useSelector( (state: any) => state.auth.token)
  const totem = useSelector( (state: any) => state.totem.item)

  // Redirect
  const isLoaded = useMemo(
    () => NavigationService.isLoaded,
    [NavigationService.isLoaded])

  const redirectScreen = useMemo(() => {
    if(!token) {
      return null;
    }
    return totem ?
      { name: 'Receitas', payload: totem } :
      { name: 'Totem' }
  }, [token, totem])

  const initialRouteName = useMemo(() => (
    redirectScreen ?
      (redirectScreen.name || 'Login') :
      'Login'
    ), [redirectScreen])

  useEffect(() => {
    if(redirectScreen && isLoaded) {
      navigate(redirectScreen)
    }
  }, [redirectScreen, isLoaded])

  const initialParams = useMemo(() => ({ token, totem } as any) , [token, totem])
  return (
      <NavigationContainer ref={ref}>
        <Stack.Navigator initialRouteName={initialRouteName}>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{headerShown: false}} initialParams={initialParams} />
          <Stack.Screen
            name="Totem"
            component={TotemScreen}
            options={{headerShown: false}} initialParams={initialParams} />
          <Stack.Screen
            name="Receita"
            component={ReceitaScreen}
            options={{headerShown: false}} initialParams={initialParams} />
          <Stack.Screen
            name="Receitas"
            component={ReceitasScreen}
            options={{headerShown: false}} initialParams={initialParams} />
          <Stack.Screen
            name="CadastroCliente"
            component={CadastroClienteScreen}
            options={{headerShown: false}} initialParams={initialParams} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
