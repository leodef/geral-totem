// import { createLogger } from 'redux-logger'
import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createRootReducer from './src/reducers'
import rootSaga from './src/sagas'

/**
 * Configuração do store do redux, add middlewares
 *  Middleware são plugins que são utilizados pelo redux
 * @param {props: any } preloadedState preloadedState
 * @return {Store} Amazernagem do redux
 */
export default function configureStore (preloadedState: any) {
  const middleWare = []

  // Add Saga Middleware - Framework Saga
  const sagaMiddleware = createSagaMiddleware()
  middleWare.push(sagaMiddleware)
  // Add Logger Middleware - Framework redux-logger
  /*
    const loggerMiddleware = createLogger({
      predicate: () => process.env.NODE_ENV === 'development'
    })
    middleWare.push(loggerMiddleware)
  */

  // Create store with middewares
  const store = createStore(
    createRootReducer(),
    preloadedState,
    compose(
      applyMiddleware(
        ...middleWare
      )
    )
  )

  // Run Sagas - Framework Saga, sagaMiddleware.run(rootSaga);
  rootSaga.forEach((saga: any) =>
    sagaMiddleware.run.bind(sagaMiddleware)(saga)
  )

  return store
}
