import * as Yup from 'yup'
import { Totem } from './Totem'
import { Empresa } from './Empresa'
/*
  Cliente
    nome: string
    cpf: string
    email: string
    telefone: string
    // verificar campos cadastro
*/
export class Cliente {
  nome: String | null = null;
  cpf: String | null = null;
  email: String | null = null;
  telefone: String | null = null;
  empresa: Empresa | null = null;
  totem: Totem | null = null;
}
export const ClienteSchema = Yup.object().shape({
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .uppercase()
    .required('Required'),
  cpf: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required'),
  telefone: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required')
})

export const initialValues = {
  nome: '',
  cpf: '',
  email: '',
  telefone: ''
}

