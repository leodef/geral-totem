import { Midia } from './Midia';
import { Empresa } from './Empresa';
/*
  Receita
    titulo: string
    desc: string
    pagina: string // gravar uma pagina em html
    video: string
    image: string
*/
export class Receita {
  titulo: String | null = null;
  desc: String | null = null;
  pagina: String | null = null;
  video: Midia | null = null;
  imagem: Midia | null = null;
  empresa: Empresa | null = null;
}