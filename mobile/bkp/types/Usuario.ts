import * as Yup from 'yup'
/*
  Usuario
    usuario: string
    senha: string
    nome: string
*/
export class Usuario {
  usuario: String | null = null;
  senha: String | null = null;
  nome: String | null = null;
}

export const LoginSchema = Yup.object().shape({
  usuario: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .uppercase()
    .required('Required'),
  senha: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required'),
  totem: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required')
})

export const loginInitialValues = {
  usuario: '',
  senha: '',
  totem: '',
}
