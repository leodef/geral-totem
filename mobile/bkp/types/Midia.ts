/*
    Midia
    nome: String
    uri: String
    tipo: String
    midiaType:  'video' | 'image'
*/
export class Midia {
  nome: String
  uri: String
  tipo: String
  local: String
  extensao: String
}