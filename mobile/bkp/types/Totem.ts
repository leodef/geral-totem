import { Empresa } from './Empresa'
/*
  Totem
    codigo: string
    titulo: string
    desc: string
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' }
*/
export class Totem {
  codigo: String | null = null;
  titulo: String | null = null;
  desc: String | null = null;
  status: String | null = null;
  empresa: Empresa | null = null
}
