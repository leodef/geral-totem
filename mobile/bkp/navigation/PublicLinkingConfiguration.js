import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  initialRouteName: 'Login',
  config: {
    Login: {
      path: 'login'
    }
  },
};
