import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  initialRouteName: 'Receitas',
  config: {
    Receita: {
      path: 'receita'
    },
    Receitas: {
      path: 'receitas'
    },
    Cadastro: {
      path: 'Cadastro'
    },
  },
};
