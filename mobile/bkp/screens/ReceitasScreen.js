import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { ListItem } from 'react-native-elements'

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  // ... // more items
];

const keyExtractor = (item, index) => index.toString()

const ReceitasScreenItem = ({ item }) => {
  const onPressReceita = (item) => {
    navigation.push('Receita');
  }
  return (<ListItem
    title={item.titulo}
    subtitle={item.desc}
    leftAvatar={{ source: { uri: item.imagem } }}
    bottomDivider
    chevron
    onPress={(item) => onPressReceita(item)}
  />)
  }

const ReceitasScreen = (props) => {
  return (
    <FlatList
      keyExtractor={this.keyExtractor}
      data={list}
      renderItem={ReceitasScreenItem}
    />
  )
}