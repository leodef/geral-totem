import * as React from 'react';
import { View } from 'react-native';

const LoginScreen = (props) => (
  <View>

    <TextInput
      value={props.values.email}
      onChangeText={text => props.setFieldValue('usename', text)}
    />

    <TextInput
      value={props.values.password}
      onChangeText={text => props.setFieldValue('password', text)}
    />
  
    <TextInput
      value={props.values.password}
      onChangeText={text => props.setFieldValue('totem', text)}
    />

    <Button
      onPress={props.handleSubmit}
      title="Login"
    />
  </View>
);
LoginScreen.navigationOptions = {
  header: null,
};
export default withFormik({
  mapPropsToValues: () => ({
    username: '',
    password: '',
    totem: ''
  }),

  handleSubmit: (values) => {
    console.log(values);
    // navigation.navigate('Details')
  }
})(LoginScreen);