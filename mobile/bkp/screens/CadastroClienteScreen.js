import * as React from 'react';
import { View } from 'react-native';

const CadastroClienteScreen = (props) => (
  <View>

    <TextInput
      value={props.values.email}
      onChangeText={text => props.setFieldValue('nome', text)}
    />

    <TextInput
      value={props.values.password}
      onChangeText={text => props.setFieldValue('email', text)}
    />
  
    <TextInput
      value={props.values.password}
      onChangeText={text => props.setFieldValue('telefone', text)}
    />

    <Button
      onPress={props.handleSubmit}
      title="Login"
    />
  </View>
);
CadastroClienteScreen.navigationOptions = {
  header: null,
};
export default withFormik({
  mapPropsToValues: () => ({
    username: '',
    password: '',
    totem: ''
  }),
  handleSubmit: (values) => {
    console.log(values);
  }
})(CadastroClienteScreen);