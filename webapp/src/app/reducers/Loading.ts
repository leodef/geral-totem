import { LoadingType } from '../types/Loading'

export const initialState: any = {
  tasks: []
}

export default function reduce (state = initialState, action: any) {
  const tasks = state.tasks
  switch (action.type) {
    case LoadingType.START_LOADING:
      tasks.push((action.payload || tasks.length).toString())
      return {
        tasks
      }
    case LoadingType.STOP_LOADING:
      return {
        tasks: state.tasks.filter((task: any) =>
          task.toString() !== (action.payload || tasks.length).toString()
        )
      }
    default:
      return state
  }
}
