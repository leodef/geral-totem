import { OptionsType } from '../types/Options'

export interface OptionsState {
  items: Array<any>;
  filter: any;
  fields: any;
  loading: boolean;
  error: string | null;
  fetchLoading: boolean;
  fetched: boolean;
}
export const initialState: OptionsState = {
  items: [],
  filter: null as any,
  fields: null as any,
  loading: false as any,
  error: null as any,
  fetchLoading: false,
  fetched: false
}

export class OptionsReducer {
  config: OptionsType = new OptionsType()
  constructor (config: OptionsType = new OptionsType()) {
    this.config = config
  }

  static getReduce (config?: OptionsType) {
    const val = new OptionsReducer(config)
    return val.getReduce()
  }

  getReduce () {
    return this.reduce.bind(this)
  }

  reduce (state: OptionsState = initialState, action: any): OptionsState {
    const config = this.config
    const actionType = action.type
    switch (actionType) {
      case config.SET_FILTER_OPTIONS_ITEM:
        return {
          ...state,
          filter: action.payload.filter
        }
      case config.SET_FIELDS_OPTIONS_ITEM:
        return {
          ...state,
          fields: action.payload.fields
        }
      // FETCH
      case config.FETCH_OPTIONS_ITEM:
        return state // saga actions
      // PENDING - FETCH
      case config.FETCH_OPTIONS_ITEM_PENDING:
        return {
          ...state,
          error: null,
          loading: true,
          fetchLoading: true
        }
      // SUCCESS - FETCH
      case config.FETCH_OPTIONS_ITEM_SUCCESS:
        return {
          ...state,
          items: (action.payload.items || []),
          error: null,
          loading: false,
          fetchLoading: false,
          fetched: true
        }
      // FAILURE - FETCH
      case config.FETCH_OPTIONS_ITEM_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          loading: false,
          fetchLoading: false
        }
      default:
        return state
    }
  }
}

export interface OptionsPropsActions {
  fetch: (filter: any) => void;
}

export const getMapStateToProps: (state: any) => any = (getState: (state: any) => any) => {
  return (state: any) => {
    const config = getState ? getState(state) : state
    return config
  }
}

export const getMapDispatchToProps: (dispatch: any) => any = (config: OptionsType) => {
  return (dispatch: any) => {
    return getDispatchs(dispatch, config)
  }
}

export function getDispatchs (dispatch: any, config: OptionsType) {
  const obj = {
    fetch: (filter: any) => dispatch({ type: config.FETCH_OPTIONS_ITEM, payload: filter })
  }
  return obj
}
