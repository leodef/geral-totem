import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import moduloReducer from './Modulo'
import authReducer from './Auth'
import loadingReducer from './Loading'
import messageReducer from './Message'
import sidebarReducer from './Sidebar'
// import publicoReducer from './Publico';
// import typeaheadReducer from './Typeahead';
// import empresaReducer from './Empresa';
// import usuarioReducer from './Usuario';

const createRootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    sidebar: sidebarReducer,
    message: messageReducer,
    loading: loadingReducer,
    auth: authReducer,
    modulo: moduloReducer
    // publico: publicoReducer,
  })
export default createRootReducer
