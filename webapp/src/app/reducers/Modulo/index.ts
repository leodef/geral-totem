import { combineReducers } from 'redux'

import adminReducer from './Admin'

const moduloReducer = combineReducers({
  admin: adminReducer
})

export default moduloReducer
