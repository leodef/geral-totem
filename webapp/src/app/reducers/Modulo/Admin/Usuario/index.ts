import { combineReducers } from 'redux'

import optionsReducer from './Options'
import crudReducer from './Crud'

const adminReducer = combineReducers({
  options: optionsReducer,
  crud: crudReducer
})

export default adminReducer
