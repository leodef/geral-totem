import {
  OptionsReducer
} from '../../../Options'
import {
  optionsType
} from '../../../../types/Modulo/Admin/Totem'
export default OptionsReducer.getReduce(optionsType)
