import { combineReducers } from 'redux'

import clienteReducer from './Cliente'
import empresaReducer from './Empresa'
import midiaReducer from './Midia'
import receitaReducer from './Receita'
import tokenReducer from './Token'
import totemReducer from './Totem'
import usuarioReducer from './Usuario'
import usuarioEmpresaReducer from './UsuarioEmpresa'

const adminReducer = combineReducers({
  cliente: clienteReducer,
  empresa: empresaReducer,
  midia: midiaReducer,
  receita: receitaReducer,
  token: tokenReducer,
  totem: totemReducer,
  usuario: usuarioReducer,
  usuarioEmpresa: usuarioEmpresaReducer
})

export default adminReducer
