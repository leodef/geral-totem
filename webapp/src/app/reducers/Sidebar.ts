import { SidebarType } from '../types/Sidebar'

export const initialState: any = {
  open: false,
  anchor: 'left'
}
// 'left'
export default function reduce (state = initialState, action: any) {
  switch (action.type) {
    case SidebarType.OPEN_SIDEBAR:
      return {
        ...state,
        open: true
      }
    case SidebarType.CLOSE_SIDEBAR:
      return {
        ...state,
        open: false
      }
    case SidebarType.TOGGLE_SIDEBAR:
      return {
        ...state,
        open: !state.open
      }
    default:
      return state
  }
}
