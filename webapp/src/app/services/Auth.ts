// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios'
// import usuarioReducer from '../reducers/Usuario';
// import type { Axios } from 'axios';

const AUTH_ROOT = 'http://localhost:3000/api'
/**
 * AuthService, class representing a authentication service.
 */
export class AuthService {
  static loadHeaders (auth: any) {
    const { token } = (auth || {} as any)
    axios.defaults.headers.common = {
      ...axios.defaults.headers.common,
      Authorization: token ? `Bearer ${token}` : null
    }
    return auth
  }

  static getUser () {
    const auth = localStorage.getItem('auth')
    return Promise.resolve(auth ? JSON.parse(auth) : null)
  }

  static setUser (value: any) {
    localStorage.setItem(
      'auth',
      JSON.stringify(value)
    )
    return Promise.resolve(value)
  }

  static removeUser () {
    const auth = AuthService.getUser()
    localStorage.removeItem('auth')
    return Promise.resolve(auth)
  }

  client: AxiosInstance

  /**
   * Create a authentication service.
   * @param {AxiosInstance} client - Axios http request config.
   */
  constructor () {
    this.client = axios.create({
      baseURL: AUTH_ROOT,
      timeout: 10000,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    } as AxiosRequestConfig)
  }

  /**
   * Login, Aunthenticate in server.
   * @param {{username: string, password: string}} auth - Axios http request config.
   * @return {any} Authentication info.
   */
  login (auth: {
    username: string;
    password: string;
  }): Promise<any> | Promise<any> {
    const path = 'auth/login'
    return this.client.post(path, auth)
      .then((response: any) => {
        const user = response.data
        const token = response.headers['x-auth-token']
        return { user, token }
      })
      .then((result) => AuthService.loadHeaders(result))
      .then((result: any) =>
        (result && result.token)
          ? AuthService.setUser(result)
          : Promise.resolve(result)
      )
  }

  /**
   * LoadStorage, Use storage info for configure, if it exists.
   * @return {any} Authentication info.
   */
  load () {
    return AuthService.getUser()
      .then((auth) => AuthService.loadHeaders(auth))
  }

  /**
   * Logout, remove authenticaion info from local storage.
   */
  logout () {
    return AuthService.removeUser()
      .then((auth) => AuthService.loadHeaders(null))
  }
}

export default new AuthService()
