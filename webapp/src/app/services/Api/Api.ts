
// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios'
// import type { Axios } from 'axios';

const API_ROOT = 'http://localhost:3000/api'

export class ApiService {
  private static authToken: string
  static setAuthToken (authToken: string) {
    ApiService.authToken = authToken
    return authToken
  }

  static getAuthToken () {
    return ApiService.authToken
  }

  get client (): AxiosInstance {
    const authToken = ApiService.authToken
    return axios.create({
      baseURL: API_ROOT,
      timeout: 10000,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        ...(!authToken ? {} : {
          Authorization: `Bearer ${authToken}`
        })
      }
    } as AxiosRequestConfig)
  }

  get (path: string, params?: any, config?: any): Promise<any> | Promise<any> {
    return this.client.get(
      this.loadUrlParams(path, params),
      config
    ).then((response: any) => response.data)
  }

  post (path: string, payload: any, config?: any): Promise<any> {
    return this.client.post(path, payload, config).then((response: any) => response.data)
  }

  patch (path: string, payload: any, config?: any): Promise<any> {
    return this.client.patch(path, payload, config).then((response: any) => response.data)
  }

  put (path: string, payload: any, config?: any): Promise<any> {
    return this.client.put(path, payload).then((response: any) => response.data)
  }

  delete (path: string, config?: any): Promise<any> {
    return this.client.delete(path, config).then((response: any) => response.data)
  }

  loadUrlParams (path: string, obj?: any) {
    if (!obj) {
      return path
    }
    const keys = Object
      .keys(obj).filter(key => obj[key] !== undefined && obj[key] !== null)
    if (keys.length === 0) {
      return path
    }
    const params = keys
      .map(key => `${key}=${obj[key]}`)
      .join('&')
    return `${path}?${params}`
  }
}

export default new ApiService()
