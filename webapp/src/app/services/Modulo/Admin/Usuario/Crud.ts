import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'

export class UsuarioCrudService extends CrudService {
  protected path (parent?: any): string {
    const empresaParent = findParentByPrefix('empresa', parent)
    if (empresaParent && empresaParent.item) {
      return this.empresaPath(empresaParent.item)
    }
    return this.defaultPath(parent)
  }

  protected empresaPath (item: any): string {
    return `modulo/admin/empresa/${item._id}/usuario/`
  }

  protected defaultPath (parent?: any): string {
    return 'modulo/admin/usuario/'
  }
}

export default new UsuarioCrudService()
