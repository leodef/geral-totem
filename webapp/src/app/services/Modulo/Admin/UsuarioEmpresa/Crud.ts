import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
// import usuarioService from '../Usuario/Crud'

export class UsuarioEmpresaCrudService extends CrudService {
  protected path (parent?: any): string {
    const empresaParent = findParentByPrefix('empresa', parent)
    if (empresaParent && empresaParent.item) {
      return this.empresaPath(empresaParent.item)
    }
    return this.defaultPath(parent)
  }

  protected empresaPath (item: any): string {
    return `modulo/admin/empresa/${item._id}/usuario/`
  }

  protected defaultPath (parent?: any): string {
    return 'modulo/admin/usuario-empresa/'
  }

  /*
    create (value: any, parent?: any): Promise<any> {
      let { usuario, ...obj } = value
      console.log('UsuarioEmpresaCrudService.create value = ', value)
      return usuarioService.create(usuario, parent)
        .then((result) => (usuario = result))
        .then((result: any) => {
          console.log('UsuarioEmpresaCrudService.create usuario = ', usuario)
          return super.create({
            ...obj,
            usuario
          }, parent)
        })
    }
  */
  /*
    update (value: any, parent?: any): Promise<any> {
      const { usuario } = value
      return super.update(value, parent)
        .then((result) => (value = result))
        .then((result) => usuarioService.update(usuario, parent))
        .then((result) => (value.usuario = result))
        .then((result) => value)
    }
  */
  /*
    remove (value: any, parent?: any): Promise<any> {
      const { usuario } = value
      return super.remove(value, parent)
        .then((result) => usuarioService.remove(usuario, parent))
    }
  */
  /*
    find (id: any, parent?: any): Promise<any> {
      let item = {} as any
      return super.find(id, parent)
        .then((result) => (item = result))
        .then((result) => usuarioService.find(item.usuario, parent))
        .then((result) => (item.usuario = result))
        .then((result) => item)
    }
  */
}

export default new UsuarioEmpresaCrudService()
