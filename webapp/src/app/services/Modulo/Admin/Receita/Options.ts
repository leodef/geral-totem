import { OptionsService } from '../../../Options'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'

export class ReceitaOptionsService extends OptionsService {
  protected path (parent?: any): string {
    const empresaParent = findParentByPrefix('empresa', parent)
    if (empresaParent && empresaParent.item) {
      return this.empresaPath(empresaParent.item)
    }
    return this.defaultPath(parent)
  }

  protected empresaPath (item: any): string {
    return `modulo/admin/empresa/${item._id}/receita/`
  }

  protected defaultPath (parent?: any): string {
    return 'modulo/admin/receita/'
  }
}

export default new ReceitaOptionsService()
