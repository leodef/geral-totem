import { CrudService } from '../../../Crud'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'
import midiaService from '../Midia/Crud'

export class ReceitaCrudService extends CrudService {
  protected path (parent?: any): string {
    const empresaParent = findParentByPrefix('empresa', parent)
    if (empresaParent && empresaParent.item) {
      return this.empresaPath(empresaParent.item)
    }
    return this.defaultPath(parent)
  }

  protected empresaPath (item: any): string {
    return `modulo/admin/empresa/${item._id}/receita/`
  }

  protected defaultPath (parent?: any): string {
    return 'modulo/admin/receita/'
  }

  create (value: any, parent?: any): Promise<any> {
    let { image, video, ...obj } = value
    return midiaService.create(image, parent)
      .then((result) => (image = result))
      .then((result) => midiaService.create(video, parent))
      .then((result) => (video = result))
      .then((result: any) =>
        super.create({
          ...obj,
          image,
          video
        }, parent)
      )
  }

  update (value: any, parent?: any): Promise<any> {
    const { image, video } = value
    return super.update(value, parent)
      .then((result) => (value = result))
      .then((result) => midiaService.update(image, parent))
      .then((result) => (value.image = result))
      .then((result) => midiaService.update(video, parent))
      .then((result) => (value.video = result))
      .then((result) => value)
  }

  remove (value: any, parent?: any): Promise<any> {
    const { image, video } = value
    return super.remove(value, parent)
      .then((result) => midiaService.remove(image, parent))
      .then((result) => midiaService.remove(video, parent))
  }

  find (id: any, parent?: any): Promise<any> {
    let item = {} as any
    return super.find(id, parent)
      .then((result) => (item = result))
      .then((result) => midiaService.find(item.image, parent))
      .then((result) => (item.image = result))
      .then((result) => midiaService.find(item.video, parent))
      .then((result) => (item.video = result))
      .then((result) => item)
  }
}

export default new ReceitaCrudService()
