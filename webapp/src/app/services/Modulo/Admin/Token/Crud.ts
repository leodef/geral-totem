import { CrudService } from '../../../Crud'

export class TokenCrudService extends CrudService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/token/'
  }
}

export default new TokenCrudService()
