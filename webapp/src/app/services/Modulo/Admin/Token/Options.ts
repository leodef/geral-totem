import { OptionsService } from '../../../Options'

export class TokenOptionsService extends OptionsService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/token/'
  }
}

export default new TokenOptionsService()
