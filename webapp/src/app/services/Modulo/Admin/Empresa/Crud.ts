import { CrudService } from '../../../Crud'

export class EmpresaCrudService extends CrudService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/empresa/'
  }
}

export default new EmpresaCrudService()
