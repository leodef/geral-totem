import { OptionsService } from '../../../Options'

export class EmpresaOptionsService extends OptionsService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/empresa/'
  }
}

export default new EmpresaOptionsService()
