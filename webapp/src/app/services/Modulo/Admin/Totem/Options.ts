import { OptionsService } from '../../../Options'
import { findParentByPrefix } from '../../../../contexts/ParentCrudContext'

export class TotemOptionsService extends OptionsService {
  protected path (parent?: any): string {
    const empresaParent = findParentByPrefix('empresa', parent)
    if (empresaParent && empresaParent.item) {
      return this.empresaPath(empresaParent.item)
    }
    return this.defaultPath(parent)
  }

  protected empresaPath (item: any): string {
    return `modulo/admin/empresa/${item._id}/totem/`
  }

  protected defaultPath (parent?: any): string {
    return 'modulo/admin/totem/'
  }
}

export default new TotemOptionsService()
