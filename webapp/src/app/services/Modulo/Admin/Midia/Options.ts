import { OptionsService } from '../../../Options'

export class MidiaOptionsService extends OptionsService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/midia/'
  }
}

export default new MidiaOptionsService()
