import { CrudService } from '../../../Crud'

export class MidiaCrudService extends CrudService {
  protected defaultPath (parent?: any): string {
    return 'modulo/admin/midia/'
  }

  public loadRequestBody (value: any) {
    const { formType } = value
    if (formType === 'uri') {
      delete value.arquivo
    } else if (formType === 'arquivo') {
      delete value.uri
    }
    const formData = new FormData()
    for (const key in value) {
      const obj = value[key]
      if (obj) {
        formData.append(key, obj)
      }
    }
    return formData
  }

  find (id: any, parent?: any): Promise<any> {
    let midia = {} as any
    return super.find(id, parent)
      .then((result) => (midia = result))
      .then((result) => this.src(midia, parent))
      .then((src) => {
        return { ...midia, src }
      })
  }

  src (midia: any, parent?: any): Promise<any> {
    const { uriPath, uri, mime } = midia
    if (!uriPath) {
      return Promise.resolve(uri)
    }
    return this.api.get(uriPath, null, { responseType: 'arraybuffer' }).then((data) => {
      const image = btoa(
        new Uint8Array(data)
          .reduce((data, byte) => data + String.fromCharCode(byte), '')
      )
      return `data:${mime};base64,${image}`
    })
  }
}

export default new MidiaCrudService()
