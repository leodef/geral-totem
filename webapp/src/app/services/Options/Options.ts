import api from '../Api'
import _ from 'lodash'

export class OptionsService {
  protected defaultPath (parent?: any) {
    return '/'
  }

  protected path (parent?: any): string {
    /*
      if(parent.prefix === '') {
        return ''
      }
    */
    return this.defaultPath(parent)
  }

  protected pathSubPath (val?: string, parent?: any) {
    return (val ? `${this.path(parent)}${val}/` : this.path(parent))
  }

  private cleanRequestFilter (body: any) {
    return _.pickBy(body, _.identity)
  }

  fetchOptions (params?: any): Promise<any> {
    const { filter, fields, parent } = params
    return api.post(
      this.pathSubPath('fetchOptions', parent),
      this.cleanRequestFilter({
        filter,
        fields
      })
    ).then((response: any) => response as { items: []})
  }
}
