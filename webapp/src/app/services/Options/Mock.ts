import { OptionsService as IOptionsService } from './Options'

class OptionsService extends IOptionsService {
  static items: any = {};
  constructor () {
    super()
    OptionsService.items[this.path()] =
      OptionsService.items[this.path()] || []
  }

  private get items (): Array<any> {
    return OptionsService.items[this.path()]
  }

  private set items (items: Array<any>) {
    OptionsService.items[this.path()] = items
  }

  fetchOptions (params?: any): Promise<any> {
    // const {filter, fields} = params;
    return Promise.resolve({ items: this.items })
  }

  fetch (filter?: any, pagination?: any, fields?: any, sort?: any): Promise<any> {
    return Promise.resolve({ items: this.items })
  }

  all (filter: any): Promise<Array<any>> {
    return Promise.resolve(this.items)
  }

  create (value: any): Promise<any> {
    value._id = this.generateId()
    this.items.push(value)
    return Promise.resolve(value)
  }

  remove (value: any): Promise<any> {
    const _id = this.getId(value)
    let selected = null
    this.items = this.items.filter(item => {
      if (_id === this.getId(item)) {
        selected = item
        return false
      }
      return true
    })
    return Promise.resolve(selected)
  }

  update (value: any, id?: string): Promise<any> {
    const _id = this.getId(value, id)
    const selected = null
    this.items = this.items.map(item => {
      if (_id === this.getId(item)) {
        return { ...item, ...value }
      }
      return item
    })
    return Promise.resolve(selected)
  }

  find (value: any): Promise<any> {
    const _id = this.getId(value)
    return this.items.filter(item => _id.indexOf(this.getId(item)) >= 0)[0]
  }

  private getId (value: any, id?: string): string {
    id = id || value._id
    if (typeof value === 'string' && !id) {
      id = value
    }
    return id || ''
  }

  private generateId () {
    return ('_' + Math.random().toString(36).substr(2, 9) + new Date().getMilliseconds())
  }
}
export { OptionsService }
export default new OptionsService()
