/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import FormikMaterialUIField from './FormikMaterialUIField'
const value = FormikMaterialUIField({})
it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<div>{value}</div>, div)
  ReactDOM.unmountComponentAtNode(div)
})
