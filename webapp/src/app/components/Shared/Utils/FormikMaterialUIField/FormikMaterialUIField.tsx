import React from 'react'

// eslint-disable-next-line no-unused-vars
import MuiTextField from '@material-ui/core/TextField'

// eslint-disable-next-line no-unused-vars
import { Field, useField } from 'formik'
// useFormikContext, useField
import './FormikMaterialUIField.scss'

// https://github.com/stackworx/formik-material-ui
// https://codesandbox.io/s/915qlr56rp
/**
 * Metodo para obter o nome do campo consierando o caminho no formulário
 *   inserindo o prefixo antes do nome do campo
 * @param {string} nome Nome do campo
 * @param {any} props Propriedades
 * @return {string} Nome completo do campo
 */
export function getFieldName (name: string, props: any) {
  return [props.name, name]
    .filter(val => !!val)
    .join('.')
}

/**
 * Campo genérico contendo obrigatóriamente somente letras maiusculas
 * @param {TextFieldProps} props Propriedades
 * @return {React.Component} Componente com campo genérico contendo obrigatóriamente somente letras maiusculas
 */
export const TextField = React.memo((props: any) => {
  TextField.displayName = 'UpperCasingTextField'
  const [field] = useField(props) // , meta, helpers
  const others = {} as any
  if (props.uppercase || props.lowercase || props.formatter) {
    others.onChange =  => {
      const formatter = props.formatter || ((val: any) => val)
      const value = event.target.value
      event.target.value = value ? formatter(
        props.uppercase
          ? value.toString().toUpperCase()
          : value.toString().toLowerCase()
      ) : ''
      field.onChange(event: any)
    }
  }
  return <MuiTextField { ...props} { ...field } { ...others } />
})

export const UppercaseInput = React.memo((props: any) => {
  UppercaseInput.displayName = 'UppercaseInput'

  // const { handleChange } = useFormikContext()
  // const [field] = useField(props) // , meta, helpers

  const {
    form: { setFieldValue },
    field: { name }
  } = props

  const onChange = React.useCallback(
    event => {
      const { value } = event.target
      setFieldValue(name, value ? value.toUpperCase() : '')
    },
    [name, setFieldValue]
  )
  return (<Field
    {...props}
    onChange={onChange}
  />)
})

/**
 * Fabrica de campo da biblioteca formik encapsulando o campo da biblioteca visual Material-UI
 * @param {any} props Propriedades
 * @return {React.Component} Componente com fabrica de campo da biblioteca formik encapsulando o campo da biblioteca visual Material-UI
 */
export const FormikMaterialUIField = (config: any) => {
  const FormikMaterialUIField = React.memo((props: any) => {
    FormikMaterialUIField.displayName = 'FormikMaterialUIField'
    const onChange = (e: any) => {
      if (props.field && props.field.onChange) {
        props.field.onChange(e)
      }
      if (config.onChange) {
        config.onChange(e)
      }
    }
    const onBlur = (e: any) => {
      if (!!props.field && !!props.field.onBlur) {
        props.field.onBlur(e)
      }
      if (config.onBlur) {
        config.onBlur(e)
      }
    }
    return (
      <MuiTextField
        {...(props.field || props)}
        {...config}
        onBlur={onBlur}
        onChange={onChange}
      />
    )
  })
  return FormikMaterialUIField
}
export default FormikMaterialUIField
