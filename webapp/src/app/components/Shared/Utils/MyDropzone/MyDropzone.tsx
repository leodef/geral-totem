import React, { useCallback } from 'react'
import { useDropzone } from 'react-dropzone'

/**
 * MyDropzone area de upload de arquivos
 * @param {any} props Propriedades
 * @return {React.Component} MyDropzone area de upload de arquivos
 */
export const MyDropzone = (props: any) => {
  const onDrop = useCallback((acceptedFiles) => {
    console.log(
      'acceptedFiles',
      JSON.stringify(acceptedFiles)
    )
    /*
      acceptedFiles.forEach((file: any) => {
        const reader = new FileReader()

        reader.onabort = () => console.log('file reading was aborted')
        reader.onerror = () => console.log('file reading has failed')
        reader.onload = () => {
        // Do whatever you want with the file contents
          const binaryStr = reader.result
          console.log(binaryStr)
        }
        reader.readAsArrayBuffer(file)
      })
    */
  }, [])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive
          ? (<p>
            Drop the files here ...
          </p>)
          : (<p>
            Drag  drop some files here
            or click to select files
          </p>)
      }
    </div>
  )
}
