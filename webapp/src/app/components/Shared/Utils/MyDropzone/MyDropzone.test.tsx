/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { MyDropzone } from './MyDropzone'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MyDropzone />, div)
  ReactDOM.unmountComponentAtNode(div)
})
