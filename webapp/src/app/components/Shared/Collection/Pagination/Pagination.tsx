import React, { useMemo, useContext, useCallback } from 'react'
import MuiPagination from '@material-ui/lab/Pagination'
import { CollectionContext } from '../../../../contexts/CollectionContext'
// import * as _ from 'lodash'
import './Pagination.scss'
/**
 * Lista genérica para coleção de items
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista genérica para coleção de items
 */
export const Pagination = (props: any) => {
  // CollectionContext
  const { pagination, setPagination } = useContext(CollectionContext)
  const { limit, page, pages } = pagination
  const handleChange = useCallback((event: React.ChangeEvent<unknown>, value: number) => {
    setPagination({
      ...pagination,
      page: value
    })
  }, [pagination, setPagination])

  const body = useMemo(() =>
    (pages > 1 && limit > 0)
      ? (<MuiPagination
        count={pages}
        page={page || 1}
        showFirstButton
        showLastButton
        onChange={handleChange} />)
      : null, [pages, page, limit, handleChange])
  return body
}
