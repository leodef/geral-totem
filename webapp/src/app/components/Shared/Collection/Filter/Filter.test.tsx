import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import Filter from './Filter';

it('renders without crashing', () => {
  const [filter, setFilter] = useState('');
  const div = document.createElement('div');
  ReactDOM.render(
    <Filter
      filter={filter}
      setFilter={setFilter}
    />,
    div);
  ReactDOM.unmountComponentAtNode(div);
});
