import React, { useMemo, useContext } from 'react'
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  Grid,
  TextField
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons'
import { CollectionContext } from '../../../../contexts/CollectionContext'
import './Filter.scss'
/**
 * Formulário genérico para filtar uma coleção de items
 * @param {any} props Propriedades
 * @return {React.Component} Componente com formulário genérico para filtar uma coleção de items
 */
export const Filter = (props: any) => {
  const { filter, label } = useMemo(() => props, [props])
  const { setFilter } = useContext(CollectionContext)
  return (
    <ExpansionPanel>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="table-filter-content"
        id="table-filter-header">
        <Typography>
          {label}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <form noValidate autoComplete="off">
              <TextField
                id="filter"
                label={label}
                name="filter"
                value={filter || ''}
                onChange={ => setFilter(event, props)} />
            </form>
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}
export default Filter
