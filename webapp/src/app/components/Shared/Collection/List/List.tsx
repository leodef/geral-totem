import React, { useState, useMemo, useContext, useCallback } from 'react'
import { withStyles } from '@material-ui/core/styles'
import {
  List as ListMui,
  ListItem as ListItemMui,
  ListItemAvatar as ListItemAvatarMui,
  ListSubheader as ListSubheaderMui,
  ListItemText,
  Avatar,
  IconButton,
  ListItemSecondaryAction,
  Menu,
  MenuItem,
  ListItemIcon,
  // eslint-disable-next-line no-unused-vars
  MenuProps,
  Container,
  Fab,
  Grid,
  TextField,
  InputAdornment
} from '@material-ui/core'
import {
  MoreVert as MoreVertIcon,
  Visibility as VisibilityIcon,
  Edit as EditIcon,
  Delete as DeleteIcon,
  Add as AddIcon,
  Search as SearchIcon
} from '@material-ui/icons'
import Skeleton from '@material-ui/lab/Skeleton'
import { CollectionContext } from '../../../../contexts/CollectionContext'
import { useDispatch } from 'react-redux'
import { ActionTypeEnum } from '../../../../types/Crud'
import { Pagination } from '../Pagination/Pagination'
import './List.scss'

/**
 * Lista genérica para coleção de items
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com lista genérica para coleção de items
 */
export const List = (props: any) => {
  // CollectionContext
  const context = useContext(CollectionContext)
  const {
    config,
    items,
    actions,
    types,
    loading,
    pagination,
    filter,
    setFilter
  } = context
  const { dense, subheader, fields } = config
  const limit = useMemo(() => pagination ? pagination.limit : null, [pagination])

  // dispatch
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toNew = useCallback(() => {
    setAction(ActionTypeEnum.NEW, {})
  }, [setAction])

  const filterInput = useMemo(() => filter ? Object.values(filter)[0] || '' : '', [filter])

  const setFilterInput = useCallback( => {
    const filterInputVal = (event.target.value)
    const filterVal = {} as any
    if (fields) {
      fields.forEach((field: any) => {
        filterVal[field.name] = filterInputVal
      })
    }
    setFilter(filterVal)
  }, [setFilter, fields])

  const body = useMemo(() => (
    loading ? (
      <React.Fragment>{
        Array.from({ length: (limit && limit <= 0 ? 1 : limit) }, (val: any, key: number) =>
          (<WaitingListItem key={key} />)
        )
      }</React.Fragment>
    ) : (
      <React.Fragment>
        {
          items.filter((item: any) => item).map((item: any, key: number) =>
            (<ListItem item={item} key={key} />)
          )
        }
      </React.Fragment>)
  ), [loading, items, limit])

  return (<Container>
    <ListMui
      aria-labelledby="nested-list-subheader"
      subheader={
        (subheader || setFilter) ? (
          <ListSubheaderMui component="div" id="nested-list-subheader">
            <Grid
              container
              spacing={2}
              direction="row"
              justify="flex-end"
              alignItems="center" >
              {(subheader)
                ? (<Grid item>{subheader}</Grid>)
                : null
              }
              {(setFilter)
                ? (<Grid item>
                  <TextField
                    id="input-with-icon-grid"
                    value={filterInput}
                    onChange={(event: any) => setFilterInput(event: any)}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SearchIcon />
                        </InputAdornment>
                      )
                    }} />
                </Grid>)
                : null
              }
            </Grid>
          </ListSubheaderMui>)
          : undefined
      }
      dense={dense}>
      {body}
    </ListMui>
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center" >
      <Grid item >
        <Pagination />
      </Grid>
    </Grid>
    {(actions.toNew)
      ? (<Fab color="primary" size="medium" aria-label="Novo" onClick={() => toNew()}>
        <AddIcon />
      </Fab>)
      : null
    }
  </Container>)
}

export const WaitingListItem = (props: { key?: any, id?: any }) => {
  // CollectionContext
  return (
    <ListItemMui>
      <ListItemAvatarMui>
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
      </ListItemAvatarMui>
      <ListItemText primary={(<Skeleton animation="wave" height={10} width="80%" />)} />
    </ListItemMui>
  )
}

/**
 * Item da lista genérica para coleção de items
 *   List.ListItem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com item da lista genérica para coleção de items
 */
export const ListItem = (props: { item: any, key?: any, id?: any }) => {
  const { item, id } = props
  // CollectionContext
  const { actions, config } = useContext(CollectionContext)
  const { getListItem } = config

  const showActions = useMemo(() => (
    actions && (
      actions.toRemove ||
      actions.toEdit ||
      actions.toShow
    )
  ), [actions])

  // item
  const { primary, secondary, avatar } = useMemo(
    () => getListItem(item, id),
    [item, id, getListItem])

  return (
    <ListItemMui>
      {
        avatar ? (
          <ListItemAvatarMui>
            <Avatar>
              {avatar}
            </Avatar>
          </ListItemAvatarMui>
        ) : null
      }

      <ListItemText
        primary={primary}
        secondary={
          secondary || null
        } />
      {(showActions)
        ? (<ListItemActions
          {...props} />) : null
      }
    </ListItemMui>
  )
}

/**
 * Menu customizado
 * @param {MenuProps} props Propriedades
 * @return {React.Component} Componente com menu customizado
 */
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5'
  }
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center'
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center'
    }}
    {...props}
  />
))

/**
 * Fabrica de item de menu customizado
 * @param {any} theme Propriedades
 * @return {method} Componente com fabrica de item de menu customizado
 */
const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white
      }
    }
  }
}))(MenuItem)

/**
 * Botões do item da lista genérica para coleção de items
 *   List.ListItem.ListItemActions
 * @param {item: ListPropsItem, index: number, config: ListConfig} props Propriedades
 * @return {React.Component} Componente com botões do item da lista genérica para coleção de items
 */
export const ListItemActions = (props: { item: any }) => {
  // CollectionContext
  const { actions, types } = useContext(CollectionContext)

  // item
  const { item } = props

  // dispatch
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toShow = useCallback((item: any) => {
    setAction(ActionTypeEnum.SHOW, item)
  }, [setAction])

  const toEdit = useCallback((item: any) => {
    setAction(ActionTypeEnum.EDIT, item)
  }, [setAction])

  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])

  // Menu control
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  const showButton = useMemo(() => actions.toShow
    ? (<StyledMenuItem onClick={() => {
      toShow(item)
      handleClose()
    }}>
      <ListItemIcon>
        <VisibilityIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary="Visualizar" />
    </StyledMenuItem>)
    : null, [toShow, actions.toShow, item])

  const editButton = useMemo(() => actions.toEdit
    ? (<StyledMenuItem onClick={() => {
      toEdit(item)
      handleClose()
    }}>
      <ListItemIcon>
        <EditIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary="Editar" />
    </StyledMenuItem>)
    : null, [toEdit, actions.toEdit, item])

  const removeButton = useMemo(() => actions.toRemove
    ? (<StyledMenuItem onClick={() => {
      toRemove(item)
      handleClose()
    }}>
      <ListItemIcon>
        <DeleteIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText primary="Remover" />
    </StyledMenuItem>)
    : null, [toRemove, actions.toRemove, item])

  return (
    <ListItemSecondaryAction>
      <div>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <StyledMenu
          id="customized-menu"
          anchorEl={anchorEl}
          keepMounted
          open={!!anchorEl}
          onClose={handleClose} >
          {showButton}
          {editButton}
          {removeButton}
        </StyledMenu>
      </div>
    </ListItemSecondaryAction>
  )
}
