/* ##!!
import { useSelector } from 'react-redux';

function isValido (usuario: any, posicao: any) {
  return (!usuario ? false : (
    !posicao ?
      true :
      (
        !!usuario.tipo &&
        Number(usuario.tipo.posicao) === Number(posicao)
      )
  ));
}
*/

/**
 * Mostra componentes filhos apenas se o usuário autenticado tive permissão
 * @param {any} props Propriedades
 * @return {React.Component} Componentes filhos passados via propriedade child
 */
export function ProtectedComponent (props: any) {
  const {
    // ##!! posicao,
    children,
    other
  } = props

  const valido = true
  // ##!! const valido = useMemo(() => isValido(usuario, posicao), [usuario, posicao]);
  return valido ? (children) : (other || null)
}
