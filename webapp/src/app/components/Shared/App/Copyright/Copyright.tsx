import React from 'react'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'

/**
 * Copyright informando proprietário do sistema
 * @param {any} props Propriedades
 * @return {React.Component} Componente com copyright informando proprietário do sistema
 */
export const Copyright = React.memo((props: any) => {
  Copyright.displayName = 'Copyright'
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://facebook.com/paradoxicalPatterns">
        Paradoxical Patterns
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
})
export default Copyright
