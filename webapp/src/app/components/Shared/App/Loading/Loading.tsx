import React, { useMemo } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useSelector } from 'react-redux'
import './Loading.scss'

function getMainTasks (props: any): Array<any> {
  const tasks = props.tasks || []
  return tasks.filter((task: any) => !!task && !task.local)
}

/**
 * Informativo de progresso em tela
 * @param {any} props Propriedades
 * @return {React.Component} Componente com informativo de progresso
 */
export const Loading = (props: any) => {
  const loading = useSelector((state: any) => state.loading)
  const tasks = useMemo(() => getMainTasks(loading), [loading])
  return tasks.length === 0
    ? null
    : (<CircularProgress />)
}
export default Loading
