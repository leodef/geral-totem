import React, { useMemo } from 'react'
// import { MessageVariant } from '../../../types/Message';
import Snackbar from '@material-ui/core/Snackbar'
// eslint-disable-next-line no-unused-vars
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'
import { useSelector } from 'react-redux'
import './Message.scss'

function getMessagesList (props: any): Array<any> {
  const list = props.messages || []
  return list.filter((message: any) => !message.local)
}
function isOpen (messages: Array<any>): boolean {
  return messages.length > 0
}

/**
 * Encapsulamento de um alerta do framework MaterialUI
 * @param {any} props Propriedades
 * @return {React.Component} Componente com encapsulamento de um alerta do framework MaterialUI
 */
export const Alert = React.memo((props: AlertProps) => {
  Alert.displayName = 'Alert'
  return <MuiAlert elevation={6} variant="filled" {...props} />
})

/**
 * Gera o componente para uma mensagem
 * @param {any} props Propriedades
 * @return {React.Component} Componente para mensagem
 */
const MessageAlert = React.memo((props: {message: any, key: number}) => {
  MessageAlert.displayName = 'MessageAlert'
  const { message, key } = props
  return (
    <Alert severity={message.variant} key={`app-alert-${key}`}>
      {(message.message || message).toString()}
    </Alert>
  )
})

/**
 * Encapsulamento dos alertas em tela
 * @param {any} props Propriedades
 * @return {React.Component} Componente com encapsulamento dos alertas em tela
 */
export const Message = (props: any) => {
  const message = useSelector((state: any) => state.message)
  const { messages, open } = useMemo(() => {
    const messages = getMessagesList(message)
    return {
      messages,
      open: isOpen(messages)
    }
  }, [message])
  // autoHideDuration={6000} onClose={handleClose}
  return (
    <Snackbar open={open} >
      <React.Fragment>
        { messages.map((message: any, index: number) =>
          <MessageAlert
            key={index}
            message={message} />)
        }
      </React.Fragment>
    </Snackbar>
  )
}
