import React, { useContext, useCallback, useMemo, useEffect } from 'react'
import _ from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import MuiAutocomplete from '@material-ui/lab/Autocomplete'
import { TextField, CircularProgress } from '@material-ui/core'
import { AutocompleteContext } from '../../../contexts/AutocompleteContext'
import { CollectionUtils } from '../../../utils/CollectionUtils'
import { ParentCrudContext } from '../../../contexts/ParentCrudContext'
import './Autocomplete.scss'

/**
 * Autocomplete genérico
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com autocomplete genérico
 */
export const Autocomplete = (props: any) => {
  const {
    onChange,
    value
  } = useMemo(() => props, [props])
  // CrudContext
  const {
    getState,
    types,
    getOptionLabel,
    getOptionId,
    fields,
    label,
    resolve
  } = useContext(AutocompleteContext)
  const parentCrudContextValue = useContext(ParentCrudContext)

  // selector
  const {
    filter,
    items,
    loading,
    fetched
  } = useSelector((state: any) => getState(state))

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const fetch = useCallback((params?: {filter?: any, fields?: any, parent?: any}) => {
    dispatch({
      type: types.FETCH_OPTIONS_ITEM,
      payload: params
    })
  }, [dispatch, types])

  // Metodo para atualizar informações do filtro
  const setFilter = useCallback((pFilter: any) => {
    if (!pFilter || _.isEmpty(pFilter)) { return }
    dispatch({
      type: types.SET_FILTER_OPTIONS_ITEM,
      payload: {
        filter: pFilter
      }
    })
  }, [dispatch, types])

  const filteredItems = useMemo(
    () => {
      if (resolve === 'FRONT') {
        const list = CollectionUtils.resolveCollection(
          items,
          {
            filter,
            pagination: null,
            fields,
            sort: null
          })
        return list
      } else {
        return items
      }
    }, [
      items,
      resolve,
      filter,
      fields
    ])

  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (resolve === 'FRONT' && !fetched) {
      fetch()
    }
  }, [
    fetch,
    resolve,
    fetched
  ])

  useEffect(() => {
    if (!resolve || resolve === 'BACK') {
      fetch({
        filter: filter,
        fields: fields,
        parent: parentCrudContextValue
      })
    }
  }, [
    fetch,
    resolve,
    filter,
    fields,
    parentCrudContextValue
  ])
  // label={label}
  return (<MuiAutocomplete
    {...props}
    getOptionSelected={(option: any, value: any) => {
      return filteredItems.find((item: any) => getOptionId(item) === getOptionId(option))
    }}
    onInputChange={(event: any, newInputValue: any) => {
      setFilter(newInputValue)
    }}
    onChange={(event:any, inputValue: string) =>
      onChange ? onChange(inputValue) : null
    }
    value={value}
    getOptionLabel={getOptionLabel}
    options={filteredItems}
    loading={loading}
    renderInput={(params: any) => (
      <TextField
        {...params}
        label={label}
        variant="outlined"
        InputProps={{
          ...params.InputProps,
          endAdornment: (
            <React.Fragment>
              {loading ? <CircularProgress color="inherit" size={20} /> : null}
              {params.InputProps.endAdornment}
            </React.Fragment>
          )
        }}
      />
    )}
  />)
}
