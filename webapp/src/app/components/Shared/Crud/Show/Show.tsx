import React, { useContext, useCallback, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import {
  Card,
  CardActions,
  CardContent,
  Typography,
  Grid,
  Button
} from '@material-ui/core'
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon
} from '@material-ui/icons'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import './Show.scss'

const useStyles = makeStyles({
  root: {
    minWidth: 300, // 275,
    minHeight: 300
  },
  title: {
    fontSize: 14
  }
})

/**
 * Visualização de item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item
 */
export const Show = (props: any) => {
  const { actions } = props
  const title = useMemo(() => props.title, [props])
  const classes = useStyles()
  // selector
  return (
    <Card className={classes.root}>
      <CardContent>
        {title
          ? (<Typography className={classes.title} color="textSecondary" gutterBottom>
            {title}
          </Typography>) : null
        }
        <ShowBody {...props} />
      </CardContent>
      <CardActions>
        { actions ? actions(props) : <ShowActions {...props} />}
      </CardActions>
    </Card>
  )
}

const ShowBody = (props: any) => {
  const { body, ...bodyProps } = useMemo(() => props, [props])
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  const parentCrudContextValue = useContext(ParentCrudContext)
  // selector
  const { item, action } = useSelector((state: any) => getState(state))
  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const find = useCallback((pItem: any) =>
    dispatch({
      type: types.FIND_ITEM,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    }), [dispatch, types, parentCrudContextValue])

  const itemId = useMemo(() => item ? item._id : null, [item])
  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (action === ActionTypeEnum.SHOW && itemId) {
      find({ _id: itemId })
    }
  }, [itemId, action, find])

  return (<React.Fragment>{body({ ...bodyProps, item })}</React.Fragment>)
}

const ShowActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const item = useSelector((state: any) => getState(state).item)
  // dispatch
  const dispatch = useDispatch()

  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])

  const toEdit = useCallback((pItem: any) => {
    setAction(ActionTypeEnum.EDIT, pItem)
  }, [setAction])

  const toRemove = useCallback((pItem: any) => {
    setAction(ActionTypeEnum.REMOVE, pItem)
  }, [setAction])

  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const removeButton = useMemo(() => {
    return (actions.toRemove)
      ? (<Button
        variant="contained"
        color="primary"
        startIcon={<DeleteIcon />}
        onClick={ => toRemove(item)}>
        Remover
      </Button>) : null
  }, [actions, toRemove, item])

  const editButton = useMemo(() => {
    return actions.toEdit
      ? (<Button
        aria-label="Editar"
        variant="contained"
        color="primary"
        startIcon={<EditIcon />}
        onClick={() => toEdit(item)}>
          Editar
      </Button>)
      : null
  }, [actions, toEdit, item])

  const backButton = useMemo(() => {
    return actions.toBack
      ? (<Button
        variant="contained"
        color="primary"
        startIcon={<ArrowBackIcon />}
        onClick={ => back()}>
          Voltar
      </Button>)
      : null
  }, [actions, back])

  return (
    <Grid container>
      <Grid item>
        {removeButton}
      </Grid>
      <Grid item>
        {editButton}
      </Grid>
      <Grid item>
        {backButton}
      </Grid>
    </Grid>
  )
}
