import React, { useCallback, useContext, useMemo, useEffect } from 'react'
import {
  Grid,
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
  makeStyles
} from '@material-ui/core'
import {
  Delete as DeleteIcon,
  ArrowBack as ArrowBackIcon,
  Send as SendIcon
} from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import { withFormik } from 'formik'
import _ from 'lodash'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { LoadingButton } from '../../Utils/LoadingButton/LoadingButton'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import './Form.scss'

const useStyles = makeStyles({
  root: {
    minWidth: 300, // 275,
    minHeight: 300
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
})

/**
 * Formulário
 * @param {FormProps} props Propriedades
 * @return {React.Component} Componente com formulário
 */
export const Form = (props: any) => {
  const { initialValues, schema } = useMemo(() => props, [props])

  // CrudContext
  const { getState, types } = useContext(CrudContext)
  const parentCrudContextValue = useContext(ParentCrudContext)

  // selector
  const {
    item,
    action
  } = useSelector((state: any) => getState(state))

  const formInitialValues = useMemo(() =>
    (initialValues && initialValues[action])
      ? initialValues[action]
      : initialValues,
  [initialValues, action])
  const formSchema = useMemo(() =>
    (schema && schema[action])
      ? schema[action]
      : schema,
  [schema, action])

  // dispatch
  const dispatch = useDispatch()

  // Metodo de consulta
  const find = useCallback((pItem: any) =>
    dispatch({
      type: types.FIND_ITEM,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    }), [dispatch, types, parentCrudContextValue])

  const itemId = useMemo(() => item ? item._id : null, [item])
  // lifecycle
  // Executar metodo de consulta
  useEffect(() => {
    if (
      (
        action === ActionTypeEnum.NEW ||
        action === ActionTypeEnum.EDIT
      ) && itemId) {
      find({ _id: itemId })
    }
  }, [itemId, action, find])

  // dispatch
  const save = useCallback((pItem: any) => {
    const type = pItem._id ? types.UPDATE_ITEM : types.CREATE_ITEM
    dispatch({
      type,
      payload: {
        item: pItem,
        parent: parentCrudContextValue
      }
    })
  }, [dispatch, types, parentCrudContextValue])

  const convertToFormData = useCallback((values: any) => {
    const formData = new FormData()
    for (const key in values) {
      const obj = values[key]
      formData.append(key, obj)
    }
    return formData
  }, [])

  const convertSaveValue = useCallback((values: any) => {
    const val = props.convertSaveValue
      ? props.convertSaveValue(values)
      : values
    return props.convertToFormData ? convertToFormData(val) : val
  }, [props, convertToFormData])

  const WithFormik = withFormik({
    mapPropsToValues: (props: any) => _.defaults(item, formInitialValues),
    handleSubmit: (values, params) => {
      if (props.handleSubmit) {
        return props.handleSubmit(values, params)
      }
      const { setSubmitting } = params
      setSubmitting(false)
      save(
        convertSaveValue(values)
      )
    },
    validationSchema: formSchema,
    displayName: 'CrudForm'
  })(FormBody)
  return (<WithFormik item={item} {...props} />)
}

const FormBody = React.memo((props: any) => {
  FormBody.displayName = 'FormBody'
  const { title, formProps, handleSubmit, actions } = props
  const { body, ...bodyProps } = props
  const classes = useStyles()
  return (<form {...(formProps || {})} onSubmit={handleSubmit}>
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom>
          {title}
        </Typography>
        {
          body(bodyProps)
        }
      </CardContent>
      <CardActions>
        { actions ? actions(props) : <FormActions {...props} />}
      </CardActions>
    </Card>
  </form>)
})

export const RemoveButton = React.memo((props: any) => {
  RemoveButton.displayName = 'RemoveButton'
  const { actions, itemId, toRemove, item } = props
  return (actions.toRemove && itemId)
    ? (<Button
      variant="contained"
      color="primary"
      startIcon={<DeleteIcon />}
      onClick={ => toRemove(item)}>
      Remover
    </Button>) : null
})

export const SubmitButton = React.memo((props: any) => {
  SubmitButton.displayName = 'SubmitButton'
  const { loading, actions } = props
  const button = {
    type: 'submit',
    variant: 'contained',
    color: 'primary',
    startIcon: (<SendIcon />)
  }
  return actions.submit
    ? (<LoadingButton
      loading={loading}
      button={button}>
      Salvar
    </LoadingButton>) : null
})

export const BackButton = React.memo((props: any) => {
  BackButton.displayName = 'BackButton'
  const { actions, back } = props
  return actions.toBack
    ? (<Button
      variant="contained"
      color="primary"
      startIcon={<ArrowBackIcon />}
      onClick={ => back()}>
        Voltar
    </Button>) : null
})

export const FormActions = (props: any) => {
  // CrudContext
  const { getState, actions, types } = useContext(CrudContext)
  // selector
  const { item, createLoading, updateLoading } = useSelector((state: any) => getState(state))
  const itemId = useMemo(() => item ? item._id : null, [item])

  const loading = useMemo(() =>
    (createLoading || updateLoading),
  [createLoading, updateLoading])
  // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])
  const toRemove = useCallback((item: any) => {
    setAction(ActionTypeEnum.REMOVE, item)
  }, [setAction])
  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  // render
  return (
    <Grid container>
      <Grid item>
        <RemoveButton
          actions={actions}
          itemId={itemId}
          toRemove={toRemove}
          item={item}
        />
      </Grid>
      <Grid item>
        <SubmitButton
          actions={actions}
          loading={loading}
        />
      </Grid>
      <Grid item>
        <BackButton
          actions={actions}
          back={back}
        />
      </Grid>
    </Grid>
  )
}
