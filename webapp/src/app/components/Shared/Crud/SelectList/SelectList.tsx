import React, { useCallback, useMemo } from 'react'
import {
  Paper,
  List,
  ListItem,
  ListItemIcon,
  Checkbox,
  ListItemText,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import './SelectList.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: 'auto'
    },
    paper: {
      width: 200,
      height: 230,
      overflow: 'auto'
    },
    button: {
      margin: theme.spacing(0.5, 0)
    }
  })
)

/**
 * Area de inputs para Substancia
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Substancia
 */
export const SelectList = (props: any) => {
  const classes = useStyles()
  const {
    checked, //  Itens selecionados
    items, // Itens totais
    compare
  } = props

  const compareItens = useCallback((item: any, other: any) => {
    return (
      compare
        ? compare(item, other)
        : (item &&
          other &&
          (
            (item._id && item._id === other._id) ||
            (item.index && item.index === other.index)
          )
        )
    )
  }, [compare])
  const exclude = useCallback((value: any, index: number) =>
    props.exclude
      ? props.exclude(value, index)
      : false,
  [props])

  const getLabel = useCallback((value: any, index: number) =>
    props.getLabel
      ? props.getLabel(value, index)
      : `${index} - ${value}`,
  [props])

  const setChecked = useCallback((check: any) => {
    if (props.setChecked) {
      props.setChecked(check)
    }
  },
  [props])

  const filteredItems = useMemo(() =>
    items.filter(
      (value: any, index: number) => !exclude(value, index)
    ),
  [items, exclude])

  const isChecked = useCallback((value: any, index: number) =>
    props.isChecked
      ? props.isChecked(value, index, checked)
      : checked.indexOf(value) !== -1,
  [props, checked])

  const handleToggle = useCallback((value: any, index: number) => {
    let findIndex = null
    const result = [...checked]
    const find = result.find((item: any, findex: number) => {
      const result = compareItens(item, value)
      if (result) { findIndex = findex }
      return result
    })
    if (find && findIndex !== null) {
      result.splice(findIndex, 1)
    } else {
      result.push(value)
    }
    setChecked(result)
  }, [checked, setChecked, compareItens])

  return (
    <Paper className={classes.paper}>
      <List dense component="div" role="list">
        {
          filteredItems.filter((item: any) => item).map((value: any, index: number) =>
            (<SelectListItem
              {...props}
              isChecked={isChecked}
              handleToggle={handleToggle}
              getLabel={getLabel}
              value={value}
              key={index}
              index={index} />)
          )
        }
      </List>
    </Paper>
  )
}

export const SelectListItem = (props: any) => {
  const {
    index,
    value,
    isChecked,
    handleToggle,
    getLabel,
    getSecondary
  } = props

  const arialLabel = useMemo(() => `transfer-list-item-${index}-label`, [index])
  const inputProps = useMemo(() => {
    return {
      'aria-labelledby': arialLabel
    }
  }, [arialLabel])
  const label = useMemo(() =>
    getLabel ? getLabel(value, index) : null,
  [value, index, getLabel])
  const secondary = useMemo(() =>
    getSecondary ? getSecondary(value, index) : null,
  [value, index, getLabel])
  const checked = useMemo(() => Boolean(isChecked(value, index)), [isChecked, value, index])
  return (<ListItem
    role="listitem"
    button
    onClick={ (event: any) => handleToggle(value, index) }>
    <ListItemIcon>
      <Checkbox
        checked={checked}
        value={checked}
        tabIndex={-1}
        disableRipple
        inputProps={inputProps}
      />
    </ListItemIcon>
    <ListItemText
      id={arialLabel}
      primary={label}
      secondary={secondary}
    />
  </ListItem>)
}
