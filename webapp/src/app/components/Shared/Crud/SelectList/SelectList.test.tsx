/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { SelectList } from './SelectList'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SelectList />, div)
  ReactDOM.unmountComponentAtNode(div)
})
