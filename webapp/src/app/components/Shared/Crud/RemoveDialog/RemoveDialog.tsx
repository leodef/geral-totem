import React, { useContext, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  Button,
  DialogActions,
  DialogTitle
} from '@material-ui/core'
import { CrudContext } from '../../../../contexts/CrudContext'
import { ActionTypeEnum } from '../../../../types/Crud'
import { LoadingButton } from '../../Utils/LoadingButton/LoadingButton'
import { ParentCrudContext } from '../../../../contexts/ParentCrudContext'
import './RemoveDialog.scss'

/**
 * Dialogo para remoção do item
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com dialogo para remoção do item
 */
export const RemoveDialog = (props: any) => {
  const title = useMemo(() => props.title || props.body || 'Deseja realmente remover?', [props])
  // CrudContext
  const { getState, types } = useContext(CrudContext)
  const parentCrudContextValue = useContext(ParentCrudContext)
  // selector
  const { item, deleteLoading } = useSelector((state: any) => getState(state))

  const loading = useMemo(() => (deleteLoading),
    [deleteLoading])

  // dispatch
  const dispatch = useDispatch()
  const setAction = useCallback((action: ActionTypeEnum, item: any) =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action,
        item
      }
    }), [dispatch, types])
  const remove = useCallback((item: any) =>
    dispatch({
      type: types.DELETE_ITEM,
      payload: {
        item,
        parent: parentCrudContextValue
      }
    }), [dispatch, types, parentCrudContextValue])

  const back = useCallback(() => {
    setAction(ActionTypeEnum.LIST, null)
  }, [setAction])

  const button = useMemo(() => {
    return {
      onClick: () => remove(item),
      color: 'primary',
      autoFocus: true
    }
  }, [remove, item])

  return (<React.Fragment>
    <DialogTitle id="remove-dialog-title">{title}</DialogTitle>
    <DialogActions>
      <Button onClick={() => back()} color="primary">
          Não
      </Button>
      <LoadingButton
        button={button}
        loading={loading}>
        Sim
      </LoadingButton>
    </DialogActions>
  </React.Fragment>)
}
