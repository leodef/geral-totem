import React, { useEffect, useMemo, useCallback, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Modal,
  Dialog,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core'
import * as _ from 'lodash'
import { CrudContext } from '../../../contexts/CrudContext'
import { CollectionTypeEnum, ActionTypeEnum } from '../../../types/Crud'
import { Show } from './Show/Show'
import { Form } from './Form/Form'
import { Collection } from './Collection/Collection'
import { RemoveDialog } from './RemoveDialog/RemoveDialog'
import './Crud.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)

/**
 * Tela de gerenciameto de Crud
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Crud
 */
export const Crud = (props: any) => {
  const { form, show, remove, child } = props

  const classes = useStyles()
  // CrudContext
  const { getState, types } = useContext(CrudContext)

  // collection merge defaults values
  const collection = useMemo(() => {
    const result = {
      type: CollectionTypeEnum.TABLE,
      pagination: { limit: 10 }
    }
    if (props.collection) {
      return _.defaultsDeep(props.collection, result)
    }
    return result
  }, [props.collection])

  const action = useSelector(
    (state: any) => getState(state).action)
  const dispatch = useDispatch()
  const toList = useCallback(() =>
    dispatch({
      type: types.SET_ACTION_ITEM,
      payload: {
        action: ActionTypeEnum.LIST
      }
    }), [dispatch, types])
  const clear = useCallback(() =>
    dispatch({
      type: types.CLEAR_ITEM
    }), [dispatch, types])
  // dialog
  const onClose =  => toList()
  const isShowModalOpen = useMemo(() =>
    (action === ActionTypeEnum.SHOW), [action])
  const isFormModalOpen = useMemo(() =>
    (action === ActionTypeEnum.NEW || action === ActionTypeEnum.EDIT), [action])
  const isRemoveDialogOpen = useMemo(() =>
    (action === ActionTypeEnum.REMOVE), [action])
  // lifecycle
  useEffect(() => {
    return () => { clear() }
  }, [clear])
  // render
  return (<React.Fragment>
    {/* Collection */}
    <Collection {...collection} />
    {/* Form */}
    <Modal
      open={isFormModalOpen}
      onClose={onClose}
      aria-labelledby="Formulário"
      aria-describedby="Formulário">
      <div className={classes.paper}>
        <Form {...form} />
        {((child && child.body) ? child.body : null)}
      </div>
    </Modal>
    {/* Show */}
    <Modal
      open={isShowModalOpen}
      onClose={onClose}
      aria-labelledby="Visualizar"
      aria-describedby="Visualizar">
      <div className={classes.paper}>
        <Show {...show}/>
        {((child && child.body) ? child.body : null)}
      </div>
    </Modal>
    <Dialog
      open={isRemoveDialogOpen}
      aria-labelledby="Remover"
      aria-describedby="Remover">
      <RemoveDialog {...remove}/>
    </Dialog>
  </React.Fragment>)
}
/*
  collection: {
    type,
    config
  }
  show: {
    title,
    body
  },
  form: {
    title,
    body
  },
  remove: {
    title,
    body
  },
  child: {
    body
  }
 */
