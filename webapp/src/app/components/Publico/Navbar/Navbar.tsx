import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import {
  AppBar,
  Toolbar,
  Typography,
  Button
} from '@material-ui/core'

/**
 * Barra de navegação superior para usuários não logados
 * @param {any} props Propriedades
 * @return {React.Component} Componente com barra de navegação superior para usuários não logados
 */
export const Navbar = React.memo((props: any) => {
  Navbar.displayName = 'Navbar'
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">
          ASSAP
        </Typography>
        <Button color="inherit" component={RouterLink} to="/">
            Login
        </Button>
      </Toolbar>
    </AppBar>
  )
})
export default Navbar
