import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
// eslint-disable-next-line no-unused-vars
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import {
  Drawer,
  List,
  ListSubheader
} from '@material-ui/core'
import {
  Dashboard as DashboardIcon
} from '@material-ui/icons'
import { SidebarType } from '../../../types/Sidebar'
import { ListItemLink } from '../../Shared/Utils/ListItemLink/ListItemLink'
import { AdminSideBarListItem } from './AdminSideBarListItem/AdminSideBarListItem'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: 250
    },
    nested: {
      paddingLeft: theme.spacing(4)
    }
  })
)

/**
 * Lista de navegação superior para usuários logados
 * @param {any} props Propriedades
 * @return {React.Component} Componente com lista de navegação superior para usuários logados
 */
export const Sidebar = function (props: any) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { open, anchor } = useSelector((state: any) => state.sidebar)
  const toggleSidebar = () => dispatch({ type: SidebarType.TOGGLE_SIDEBAR })
  return (
    <Drawer
      anchor={anchor}
      open={open}
      onClose={() => toggleSidebar()}>
      <div
        className={classes.list}
        role="presentation"
        onKeyDown={() => toggleSidebar()}>
        <List component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader component="div" id="nested-list-subheader">
              ASSAP
            </ListSubheader>
          }>
          <ListItemLink
            to="/dashboard"
            primary="dashboard"
            icon={<DashboardIcon />}
            className={classes.nested} />
          <AdminSideBarListItem toggleSidebar={toggleSidebar} />
          {/* <Divider /> */}
        </List>
      </div>
    </Drawer>
  )
}
export default Sidebar
