import
React, {
  useMemo,
  useContext,
  useEffect,
  useCallback,
  useRef
} from 'react'
import {
  useField
} from 'formik'
import {
  // eslint-disable-next-line no-unused-vars
  Grid, makeStyles, Theme, Card, CardContent, Typography
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import JoditEditor from 'jodit-react'
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  findParentByPrefix,
  ParentCrudContext
} from '../../../../../contexts/ParentCrudContext'
import {
  Autocomplete as EmpresaAutocomplete
} from '../../Empresa/Autocomplete/Autocomplete'
import {
  Fieldset as MidiaFieldset
} from '../../Midia/Fieldset/Fieldset'
import './Fieldset.scss'

/*
  Receita :
    titulo: String,
    desc: String,
    pagina: String,
    video: { type: Schema.Types.ObjectId, ref: 'Midia' },
    image: { type: Schema.Types.ObjectId, ref: 'Midia' },
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' }
*/

/**
 * Area de inputs para Receita
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Receita
 */
export const Fieldset = (props: any) => {
  const classes = useStyles()
  const { item } = props
  const { video, image, pagina } = useMemo(
    () => (item || {} as any),
    [item])

  // Pagina input
  const editor = useRef(null)
  const config = useMemo(() => ({
    readonly: false // all options from https://xdsoft.net/jodit/doc/
  }) as any, []) as any

  const parentCrudContextValue = useContext(ParentCrudContext)

  const empresaParent = useMemo(
    () => {
      const parent = findParentByPrefix('empresa', parentCrudContextValue)
      return (parent ? parent.item : null)
    }, [parentCrudContextValue])
  const hideEmpresaParent = useMemo(
    () => Boolean(empresaParent),
    [empresaParent])

  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('titulo', props)
      },
      desc: {
        name: getFieldName('desc', props)
      },
      pagina: {
        name: getFieldName('pagina', props)
      },
      video: {
        name: getFieldName('video', props)
      },
      image: {
        name: getFieldName('image', props)
      },
      empresa: {
        name: getFieldName('empresa', props)
      }
    }
  }, [props])

  const [, , paginaHelpers] = useField(fields.pagina.name)
  const [, , empresaHelpers] = useField(fields.empresa.name)
  const setEmpresa = useCallback(
    (empresa?: any) => {
      const empresaId = empresa ? empresa._id : null
      const parentId = (empresaParent && empresaParent._id)
        ? empresaParent._id
        : null
      empresaHelpers.setValue(parentId || empresaId || '')
    }, // eslint-disable-next-line
    [empresaParent]
  )

  useEffect(() => setEmpresa(), [empresaParent, setEmpresa])

  return (
    <Grid
      container
      spacing={2}
      className={classes.root}>

      <Grid item xs={12} hidden={hideEmpresaParent}>

        <EmpresaAutocomplete
          onChange={(selected: any) => setEmpresa(selected)}
          value={empresaParent} />

      </Grid>

      <Grid container item xs={12}>

        <Grid item xs={6}>
          <TextField
            name={fields.titulo.name}
            type="text"
            label="Titulo"
          />
        </Grid>

        <Grid item xs={6}>
          <TextField
            name={fields.desc.name}
            type="text"
            label="Descrição"
            multiline
          />
        </Grid>
      </Grid>
      <Grid container item xs={12} >
        <Grid item xs={6}>
          <Card>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom>Video
              </Typography>
              <MidiaFieldset
                {...props}
                item={video}
                name={fields.video.name}
                fileType="video" />
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom>Imagem
              </Typography>
              <MidiaFieldset
                {...props}
                item={image}
                name={fields.image.name}
                fileType="image"/>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <Grid item xs={12}>
        <JoditEditor
          ref={editor}
          value={pagina}
          config={config}
          onBlur={(newContent: any) => paginaHelpers.setValue(newContent)} // preferred to use only this option to update the content for performance reasons
          onChange={newContent => {}}
        />
      </Grid>
    </Grid>
  )
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    maxHeight: 500,
    overflow: 'scroll'
  },
  title: {
    fontSize: 14
  }
}))
