import React, { useMemo } from 'react'
import {
  Typography,
  Grid,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme
} from '@material-ui/core'
import {
  Resume as MidiaResume
} from '../../Midia/Resume/Resume'
import './Resume.scss'

/*
  Receita :
    titulo: String,
    desc: String,
    pagina: String,
    video: { type: Schema.Types.ObjectId, ref: 'Midia' },
    image: { type: Schema.Types.ObjectId, ref: 'Midia' },
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' }
*/

/**
 * Visualização de item do tipo Receita
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Receita
 */
export const Resume = (props: any) => {
  const classes = useStyles()
  const { item } = props
  const { pagina } = useMemo(
    () => (item || {} as any),
    [item])
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>

    <div
      dangerouslySetInnerHTML={{ __html: pagina }}
      className={classes.pagina}>
    </div>

    <Grid container spacing={2}>

      <Grid item xs={6}>
        <MidiaResume
          item={item.video}
          width="60"
          height="60"
        />
      </Grid>

      <Grid item xs={6}>
        <MidiaResume
          item={item.image}
          width="60"
          height="60"
        />
      </Grid>

    </Grid>

  </React.Fragment>)
}

const useStyles = makeStyles((theme: Theme) => ({
  pagina: {
    height: 300,
    overflow: 'scroll'
  }
}))
