/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { UsuarioEmpresa } from './UsuarioEmpresa'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<UsuarioEmpresa />, div)
  ReactDOM.unmountComponentAtNode(div)
})
