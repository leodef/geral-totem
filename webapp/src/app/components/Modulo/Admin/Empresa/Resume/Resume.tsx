import React from 'react'
import {
  Typography
} from '@material-ui/core'

import './Resume.scss'

/*
  Empresa :
    nome: String,
    cnpj: String,
    desc: String,
*/

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const Resume = (props: any) => {
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.nome}
    </Typography>
    <Typography variant="h5" component="h2">
      {item.cnpj}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
  </React.Fragment>)
}
