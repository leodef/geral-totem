import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Empresa'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/*
  Empresa :
    nome: String,
    cnpj: String,
    desc: String,
*/

/**
 * Visualização de item do tipo Empresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Empresa
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.empresa.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['nome', 'cnpj'], label: 'Empresa' }
  }, [])
  const getOptionLabel = (option: any) => option
    ? `${option.nome} - ${option.cnpj}`
    : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
