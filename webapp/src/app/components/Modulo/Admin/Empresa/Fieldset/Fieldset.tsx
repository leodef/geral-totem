import
React, {
  useMemo
} from 'react'
import {
  Grid
} from '@material-ui/core'
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'

/*
  Empresa :
    nome: String,
    cnpj: String,
    desc: String,
*/

/**
 * Area de inputs para Empresa
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Empresa
 */
export const Fieldset = (props: any) => {
  const fields = useMemo(() => {
    return {
      nome: {
        name: getFieldName('nome', props)
      },
      cnpj: {
        name: getFieldName('cnpj', props)
      },
      desc: {
        name: getFieldName('desc', props)
      }
    }
  }, [props])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.nome.name}
          type="text"
          label="Nome"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.cnpj.name}
          type="text"
          label="CNPJ"
        />
      </Grid>

      <Grid item xs={12}>
        { /* component={TextField} */ }
        <TextField
          name={fields.desc.name}
          type="text"
          label="Descrição"
          multiline
        />
      </Grid>

    </Grid>
  )
}
