import
React, {
  useContext,
  useMemo,
  useCallback
} from 'react'
import './ChildTab.scss'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  AppBar,
  Tabs,
  Tab
} from '@material-ui/core'
import {
  Cliente
} from '../Cliente/Cliente'
import {
  UsuarioEmpresa
} from '../UsuarioEmpresa/UsuarioEmpresa'
import {
  Receita
} from '../Receita/Receita'
import {
  Totem
} from '../Totem/Totem'
import {
  CrudContext
} from '../../../../../contexts/CrudContext'
import {
  useSelector
} from 'react-redux'

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function a11yProps (index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  }
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  tabPanel: {
    maxHeight: 200,
    height: 200,
    overflow: 'auto'
  }
}))

const TabPanel = React.memo((props: TabPanelProps) => {
  TabPanel.displayName = 'TabPanel'
  const classes = useStyles()
  const { children, value, index, ...other } = props

  return (
    <div
      className={classes.tabPanel}
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (<React.Fragment>{children}</React.Fragment>)}
    </div>
  )
})

/**
 * Gerenciamento de childTab
 * @param {any} props Propriedades
 * @return {React.Component} Componente com gerenciamento de childTab
 */
export const ChildTab = (props: any) => {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)
  const { getState } = useContext(CrudContext)
  const { item } = useSelector(state => getState(state))

  const handleChange = useCallback((event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }, [])

  return useMemo(
    () => (item && item._id)
      ? (<div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="empresa child tab">
            <Tab label="Cliente" {...a11yProps(0)} />
            <Tab label="Receita" {...a11yProps(1)} />
            <Tab label="Totem" {...a11yProps(2)} />
            <Tab label="Usuario" {...a11yProps(3)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Cliente />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Receita />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Totem />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <UsuarioEmpresa />
        </TabPanel>
      </div>) : null, [item, value, handleChange, classes])
}
