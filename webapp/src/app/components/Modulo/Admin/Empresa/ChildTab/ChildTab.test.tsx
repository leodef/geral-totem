/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ChildTab } from './ChildTab'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ChildTab />, div)
  ReactDOM.unmountComponentAtNode(div)
})
