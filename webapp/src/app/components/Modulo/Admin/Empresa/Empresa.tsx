import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import {
  Crud
} from '../../../Shared/Crud/Crud'
import {
  TablePropsField
} from '../../../Shared/Collection/Table/Table'
import {
  Resume
} from './Resume/Resume'
import {
  Fieldset
} from './Fieldset/Fieldset'
import {
  EmpresaSchema,
  initialValues
} from '../../../../types/Modulo/Admin/Empresa'
import './Empresa.scss'
import { Grid } from '@material-ui/core'
import { ChildTab } from './ChildTab/ChildTab'

export const Empresa = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})

  const fields = useMemo(() => [
    new TablePropsField('nome', 'Nome'),
    new TablePropsField('cnpj', 'CNPJ')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Grid container direction="column">
      <Grid item>
        <Resume {...props} />
      </Grid>
      <Grid item>
        <ChildTab />
      </Grid>
    </Grid>)
  }, [])

  const FormBody = useCallback((props: any) => {
    return (<Grid container direction="column">
      <Grid item>
        <Fieldset {...props} />
      </Grid>
      <Grid item>
        <ChildTab />
      </Grid>
    </Grid>)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Empresa',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Empresa',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.nome ? value.nome.toString() : ''),
          secondary: null,
          avatar: (value.nome ? value.nome.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])

  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Empresa',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Empresa',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: EmpresaSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover a empresa ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
