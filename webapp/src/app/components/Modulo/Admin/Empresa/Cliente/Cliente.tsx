import
React, {
  useContext,
  useState,
  useMemo,
  useCallback
} from 'react'
import {
  useSelector
} from 'react-redux'
import {
  Modal,
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles,
  Button
} from '@material-ui/core'
import BallotIcon from '@material-ui/icons/Ballot'
// eslint-disable-next-line no-unused-vars
import {
  Parent,
  getParentContextValue,
  ParentCrudContext
} from '../../../../../contexts/ParentCrudContext'
import {
  CrudContext
} from '../../../../../contexts/CrudContext'
import {
  Cliente as ClienteCrudComponent
} from '../../Cliente/Cliente'
import {
  crudType as types
} from '../../../../../types/Modulo/Admin/Cliente'
import {
  CollectionTypeEnum
} from '../../../../../types/Crud'
import './Cliente.scss'

/**
 * Gerenciamento de cliente
 * @param {any} props Propriedades
 * @return {React.Component} Componente com gerenciamento de cliente
 */
export const ClienteModal = (props: any) => {
  const { actions } = useContext(CrudContext)
  const classes = useStyles()
  const [isOpen, setOpen] = useState(false)

  const onClose = useCallback(() => {
    setOpen(false)
  }, [setOpen])
  const toggleOpen = useCallback(() => {
    setOpen(!isOpen)
  }, [setOpen, isOpen])

  const button = useMemo(() => {
    return actions.cliente
      ? (<Button
        variant="contained"
        color="primary"
        startIcon={<BallotIcon />}
        onClick={ => toggleOpen()}>
        Clientes
      </Button>) : null
  }, [actions, toggleOpen])

  return (<React.Fragment>
    {button}
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="Clientes"
      aria-describedby="Clientes">
      <div className={classes.paper}>
        <Cliente {...props} />
      </div>
    </Modal>
  </React.Fragment>)
}

export const ClienteCrud = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.cliente.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'cliente'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 3 }
  }
  return (<CrudContext.Provider value={crudContextValue}>
    <ClienteCrudComponent collection={collection}/>
  </CrudContext.Provider>)
}
export const Cliente = (props: any) => {
  const { getState, prefix } = useContext(CrudContext)
  const { item, action } = useSelector(state => getState(state))
  const parentValue = useMemo(
    () => new Parent(
      item, // item
      action, // action
      prefix// prefix
    ), [item, action, prefix])
  const historyValue = useContext(ParentCrudContext)
  const parentCrudContextValue = useMemo(
    () => getParentContextValue(parentValue, historyValue)
    , [parentValue, historyValue])
  return useMemo(
    () => (item && item._id)
      ? (<ParentCrudContext.Provider value={parentCrudContextValue}>
        <ClienteCrud />
      </ParentCrudContext.Provider>)
      : null,
    [item, parentCrudContextValue])
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      minWidth: 200,
      minHeight: 200,
      // boxShadow: theme.shadows[5],
      // padding: theme.spacing(2, 4, 3),
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    }
  })
)
