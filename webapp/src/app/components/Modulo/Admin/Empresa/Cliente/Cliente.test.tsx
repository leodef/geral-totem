/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Cliente } from './Cliente'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Cliente />, div)
  ReactDOM.unmountComponentAtNode(div)
})
