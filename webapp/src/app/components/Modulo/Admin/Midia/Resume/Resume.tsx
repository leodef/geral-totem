import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/*
  Midia
    nome: String,
    uri: String,
    tipo: String,
    local: String,
    extensao: String
*/

/**
 * Visualização de item do tipo Midia
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Midia
 */
export const Resume = (props: any) => {
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Video {...props} />
    <Image {...props} />
  </React.Fragment>)
}

export const Image = (props: any) => {
  const { item, ...other } = props
  const { mime, src } = item || {}
  if (!item || !mime || !mime.includes('image')) {
    return null
  }
  return (<img
    width="240"
    height="240"
    {...other}
    src={src}
    alt={item.titulo} />)
}

export const Video = (props: any) => {
  const { item, source, ...other } = props
  const { mime, src } = item || {}
  if (!item || !mime || !src || !mime.includes('video')) {
    return null
  }
  return (<video
    width="240"
    height="240"
    {...other}
    controls>
    <source
      {...source || {}}
      src={src}
      type={mime} />
    Video não suportado
  </video>)
}
