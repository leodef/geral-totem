import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Midia'
import './Autocomplete.scss'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'

/*
  Midia
    nome: String,
    uri: String,
    tipo: String,
    local: String,
    extensao: String
*/

/**
 * Visualização de item do tipo Midia
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Midia
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.midia.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['titulo', 'uri'], label: 'Midia' }
  }, [])
  const getOptionLabel = (option: any) => option
    ? `${option.nome} - ${option.uri}`
    : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
