import
React,
{
  useMemo,
  useState,
  // useContext,
  useCallback,
  useEffect
} from 'react'
import {
  Grid, Switch, FormControlLabel
} from '@material-ui/core'
import {
  getFieldName, TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'
import {
  Field
} from 'formik'
/*
  Midia
    titulo: String,
    uri: String,
    tipo: String,
    local: String,
    extensao: String
*/

/**
 * Area de inputs para Midia
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Midia
 */
export const Fieldset = (props: any) => {
  const uri = useMemo(
    () => props.item ? props.item.uri : null,
    [props])
  const { setFieldValue, hideTitle, formType } = props

  const [checked, setChecked] = useState(!uri)
  const formTypeValue = useMemo(
    () => formType || (checked ? 'arquivo' : 'uri'),
    [checked, formType])
  // const [selectedFile, setSelectedFile] = useState(null as any)
  const fields = useMemo(() => {
    return {
      titulo: {
        name: getFieldName('titulo', props)
      },
      formType: {
        name: getFieldName('formType', props)
      }
    }
  }, [props])

  useEffect(
    () => {
      setFieldValue(fields.formType.name, formTypeValue)
    },
    [setFieldValue, fields.formType.name, formTypeValue])

  const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.checked
    setChecked(value)
  }, [setChecked])

  const FieldsetBody = useMemo(
    () => checked ? UploadFieldset : URIFieldset,
    [checked])

  const TitleInput = useMemo(() => {
    return hideTitle ? null : (
      <Grid item container xs={9}>
        <TextField
          name={fields.titulo.name}
          type="text"
          label="Nome"
        />
      </Grid>)
  }, [hideTitle, fields])

  const FormTypeInput = useMemo(() => {
    const switchLabel = checked ? 'Upload' : 'URI'
    return formType ? null : (
      <Grid item xs={3}>
        <FormControlLabel
          control={
            <Switch
              checked={checked}
              onChange={handleChange}
              name="upload"
              color="primary"
            />
          }
          label={switchLabel}
        />
      </Grid>)
  }, [checked, handleChange, formType])

  return (
    <Grid container spacing={2} >
      <Field
        name={fields.formType.name}
        type="hidden"
        value={formTypeValue}
      />
      <Grid item xs={12}>
        {TitleInput}
        {FormTypeInput}
      </Grid>

      <Grid item xs={12}>
        <FieldsetBody
          {...props}
        />
      </Grid>

    </Grid>
  )
}

export const URIFieldset = (props: any) => {
  const fields = useMemo(() => {
    return {
      uri: {
        name: getFieldName('uri', props)
      }
    }
  }, [props])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12}>
        <TextField
          name={fields.uri.name}
          type="text"
          label="URI"
        />
      </Grid>
    </Grid>
  )
}

export const UploadFieldset = (props: any) => {
  const {
    setFieldValue
  } = props

  const fields = useMemo(() => {
    return {
      upload: {
        name: getFieldName('upload', props)
      },
      arquivo: {
        name: getFieldName('arquivo', props)
      }
    }
  }, [props])

  const handleFileChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const file = (
      e && e.target && e.target.files ? e.target.files[0] : null
    )
    setFieldValue(fields.arquivo.name, file)
  }, [setFieldValue, fields.arquivo.name])

  const accept = useMemo(() => {
    if (props.accept) {
      return props.accept
    }
    if (props.fileType) {
      switch (props.fileType) {
        case 'image':
          return 'image/*'
        case 'video':
          return 'video/*'
        default:
          return null
      }
    }
    return null
  }, [props])

  return (
    <Grid container spacing={2} >
      <Grid item xs={12} sm={6}>
        <Field
          type="file"
          name={fields.upload.name}
          accept={accept}
          onChange={ => handleFileChange(event: any) }
        />
      </Grid>
    </Grid>
  )
}
