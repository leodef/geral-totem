/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Midia } from './Midia'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Midia />, div)
  ReactDOM.unmountComponentAtNode(div)
})
