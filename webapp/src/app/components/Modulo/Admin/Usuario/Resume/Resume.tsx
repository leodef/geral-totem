import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/*
  Usuario :
    usuario: String,
    senha: String,
    nome: String,
    status: String
*/

/**
 * Visualização de item do tipo Usuario
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Usuario
 */
export const Resume = (props: any) => {
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.usuario}
    </Typography>
    <Typography variant="h5" component="h2">
      {item.nome}
    </Typography>
    <Typography variant="body2" component="p">
      {item.status}
    </Typography>
  </React.Fragment>)
}
