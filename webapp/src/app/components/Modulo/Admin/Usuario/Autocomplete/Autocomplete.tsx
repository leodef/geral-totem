import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Usuario'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/*
  Usuario:
    usuario: String,
    senha: String,
    nome: String,
    status: String
*/

/**
 * Visualização de item do tipo Usuario
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Usuario
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.usuario.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['nome', 'usuario'], label: 'Usuario' }
  }, [])
  const getOptionLabel = (option: any) => option
    ? `${option.nome} - ${option.usuario}`
    : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
