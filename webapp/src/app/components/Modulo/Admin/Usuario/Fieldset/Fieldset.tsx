import
React, {
  useMemo, useContext
} from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import './Fieldset.scss'
import { CrudContext } from '../../../../../contexts/CrudContext'
import { useSelector } from 'react-redux'
import { ActionTypeEnum } from '../../../../../types/Crud'

/*
  Usuario:
    usuario: String,
    senha: String,
    nome: String,
    status: String
*/

/**
 * Area de inputs para Usuario
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Usuario
 */
export const Fieldset = (props: any) => {

  const { getState } = useContext(CrudContext)

  // selector
  const {
    action
  } = useSelector((state: any) => getState(state))

  const fields = useMemo(() => {
    return {
      usuario: {
        name: getFieldName('usuario', props)
      },
      senha: {
        name: getFieldName('senha', props)
      },
      nome: {
        name: getFieldName('nome', props)
      },
      status: {
        name: getFieldName('status', props)
      }
    }
  }, [props])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.usuario.name}
          type="text"
          label="Usuário"
        />
      </Grid>
      { (action === ActionTypeEnum.NEW)
        ? (<Grid item xs={12} sm={6}>

          <TextField
            name={fields.senha.name}
            type="password"
            label="Senha"
          />

        </Grid>) : null
      }
      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.nome.name}
          type="text"
          label="Nome"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.status.name}
          type="text"
          label="Status"
        />
      </Grid>

    </Grid>
  )
}
