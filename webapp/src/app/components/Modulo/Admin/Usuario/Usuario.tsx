import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum, ActionTypeEnum
} from '../../../../types/Crud'
import {
  Crud
} from '../../../Shared/Crud/Crud'
import {
  TablePropsField
} from '../../../Shared/Collection/Table/Table'
import {
  Fieldset
} from './Fieldset/Fieldset'
import {
  CreateUsuarioSchema,
  EditUsuarioSchema,
  createInitialValues,
  editInitialValues
} from '../../../../types/Modulo/Admin/Usuario'
import {
  Resume
} from './Resume/Resume'
import './Usuario.scss'

/*
  Usuario :
    usuario: String,
    senha: String,
    nome: String,
    status: String
*/

export const Usuario = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})

  const fields = useMemo(() => [
    new TablePropsField('nome', 'Nome'),
    new TablePropsField('status', 'Status')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Resume {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Usuário',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Usuário',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.nome ? value.nome.toString() : ''),
          secondary: null,
          avatar: (value.nome ? value.nome.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Usuário',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Usuário',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues: {
          [ActionTypeEnum.NEW]: createInitialValues,
          [ActionTypeEnum.EDIT]: editInitialValues
        },
        schema: {
          [ActionTypeEnum.NEW]: CreateUsuarioSchema,
          [ActionTypeEnum.EDIT]: EditUsuarioSchema
        },
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o usuário ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
