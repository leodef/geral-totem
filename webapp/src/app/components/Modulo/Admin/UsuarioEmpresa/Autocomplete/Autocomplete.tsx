import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/UsuarioEmpresa'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/**
 * Visualização de item do tipo UsuarioEmpresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo UsuarioEmpresa
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.usuarioEmpresa.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['titulo'], label: 'UsuarioEmpresa' }
  }, [])
  const getOptionLabel = (option: any) => option ? option.titulo : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
