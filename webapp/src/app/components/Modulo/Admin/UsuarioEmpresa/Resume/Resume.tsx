import React from 'react'
import { Grid } from '@material-ui/core'
import { Resume as EmpresaResume } from '../../Empresa/Resume/Resume'
import { Resume as UsuarioResume } from '../../Usuario/Resume/Resume'
import './Resume.scss'

/*
  UsuarioEmpresa :
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
*/

/**
 * Visualização de item do tipo UsuarioEmpresa
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo UsuarioEmpresa
 */
export const Resume = (props: any) => {
  const { item } = props
  const { empresa, usuario } = item
  return (<Grid container>
    <Grid item>
      <EmpresaResume {...props} item={empresa} />
    </Grid>
    <Grid item>
      <UsuarioResume {...props} item={usuario} />
    </Grid>
  </Grid>)
}
