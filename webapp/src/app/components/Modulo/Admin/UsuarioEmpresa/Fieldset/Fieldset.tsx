import
React, {
  useMemo,
  useContext,
  useCallback,
  useEffect
} from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName, TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  Autocomplete as EmpresaAutocomplete
} from '../../Empresa/Autocomplete/Autocomplete'
import {
  ParentCrudContext,
  findParentByPrefix
} from '../../../../../contexts/ParentCrudContext'
import {
  useField
} from 'formik'
import {
  Fieldset as UsuarioFieldset
} from '../../Usuario/Fieldset/Fieldset'
import './Fieldset.scss'

/*
  UsuarioEmpresa :
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
*/

/**
 * Area de inputs para UsuarioEmpresa
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para UsuarioEmpresa
 */
export const Fieldset = (props: any) => {
  const { item } = props
  const { usuario } = (item || {})

  const parentCrudContextValue = useContext(ParentCrudContext)

  const empresaParent = useMemo(
    () => {
      const parent = findParentByPrefix('empresa', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])
  const hideEmpresaParent = useMemo(() => Boolean(empresaParent), [empresaParent])

  const fields = useMemo(() => {
    return {
      empresa: {
        name: getFieldName('empresa', props)
      },
      usuario: {
        name: getFieldName('usuario', props)
      },
      tipo: {
        name: getFieldName('tipo', props)
      }
    }
  }, [props])

  const [, , empresaHelpers] = useField(fields.empresa.name)

  const setEmpresa = useCallback(
    (empresa?: any) => {
      const empresaId = empresa ? empresa._id : null
      const parentId = (empresaParent && empresaParent._id)
        ? empresaParent._id
        : null
      empresaHelpers.setValue(parentId || empresaId || '')
    }, // eslint-disable-next-line
    [empresaParent]
  )

  useEffect(() => setEmpresa(), [empresaParent, setEmpresa])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} hidden={hideEmpresaParent}>

        <EmpresaAutocomplete
          onChange={(selected: any) => setEmpresa(selected)}
          value={empresaParent} />

      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.tipo.name}
          type="text"
          label="Tipo de usuário"
        />
      </Grid>

      <Grid item xs={12}>
        <UsuarioFieldset
          {...props}
          item={usuario}
          name={fields.usuario.name} />
      </Grid>

    </Grid>
  )
}
