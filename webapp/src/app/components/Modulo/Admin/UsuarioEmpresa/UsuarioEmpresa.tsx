import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum, ActionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import { TablePropsField } from '../../../Shared/Collection/Table/Table'
import { Fieldset } from './Fieldset/Fieldset'
import {
  CreateUsuarioEmpresaSchema,
  EditUsuarioEmpresaSchema,
  createInitialValues,
  editInitialValues
} from '../../../../types/Modulo/Admin/UsuarioEmpresa'
import { Resume } from './Resume/Resume'
import './UsuarioEmpresa.scss'

/*
  UsuarioEmpresa :
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
*/

export const UsuarioEmpresa = (props: any) => {
  const { collection, form, show, remove, origin } = props
  const { type } = (collection || {})

  const fields = useMemo(() => [
    new TablePropsField('empresa.nome', 'Empresa'),
    new TablePropsField('usuario.nome', 'Usuário')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Resume {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'UsuarioEmpresa',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'UsuarioEmpresa',
      fields,
      getListItem: (value: any, index: number) => {
        const titulo = (origin === 'usuario')
          ? value.empresa.nome
          : value.usuario.nome
        return {
          primary: (titulo ? titulo.toString() : ''),
          secondary: null,
          avatar: (titulo ? titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields, origin])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Usuario',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Usuario',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues: {
          [ActionTypeEnum.NEW]: createInitialValues,
          [ActionTypeEnum.EDIT]: editInitialValues
        },
        schema: {
          [ActionTypeEnum.NEW]: CreateUsuarioEmpresaSchema,
          [ActionTypeEnum.EDIT]: EditUsuarioEmpresaSchema
        },
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o usuário ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
