import
React, {
  useMemo,
  useContext,
  useCallback,
  useEffect
} from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  ParentCrudContext,
  findParentByPrefix
} from '../../../../../contexts/ParentCrudContext'
import {
  useField
} from 'formik'
import {
  Autocomplete as EmpresaAutocomplete
} from '../../Empresa/Autocomplete/Autocomplete'
import './Fieldset.scss'

/*
  Totem :
    codigo: String,
    titulo: String,
    desc: String,
    status: String
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' }
*/

/**
 * Area de inputs para Totem
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Totem
 */
export const Fieldset = (props: any) => {
  const parentCrudContextValue = useContext(ParentCrudContext)

  const empresaParent = useMemo(
    () => {
      const parent = findParentByPrefix('empresa', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])
  const hideEmpresaParent = useMemo(() => Boolean(empresaParent), [empresaParent])

  const fields = useMemo(() => {
    return {
      codigo: {
        name: getFieldName('codigo', props)
      },
      titulo: {
        name: getFieldName('titulo', props)
      },
      status: {
        name: getFieldName('status', props)
      },
      desc: {
        name: getFieldName('desc', props)
      },
      empresa: {
        name: getFieldName('empresa', props)
      }
    }
  }, [props])

  const [, , empresaHelpers] = useField(fields.empresa.name)

  const setEmpresa = useCallback(
    (empresa?: any) => {
      const empresaId = empresa ? empresa._id : null
      const parentId = (empresaParent && empresaParent._id)
        ? empresaParent._id
        : null
      empresaHelpers.setValue(parentId || empresaId || '')
    }, // eslint-disable-next-line
    [empresaParent]
  )

  useEffect(() => setEmpresa(), [empresaParent, setEmpresa])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} hidden={hideEmpresaParent}>

        <EmpresaAutocomplete
          onChange={(selected: any) => setEmpresa(selected)}
          value={empresaParent} />

      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.codigo.name}
          type="text"
          label="Código"
        />
      </Grid>

      <Grid item xs={12} sm={6}>

        <TextField
          name={fields.titulo.name}
          type="text"
          label="Titulo"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.status.name}
          type="text"
          label="Status"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.desc.name}
          type="text"
          label="Descrição"
          multiline
        />
      </Grid>

    </Grid>
  )
}
