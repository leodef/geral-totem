import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Totem'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/*
  Totem :
    codigo: String,
    titulo: String,
    desc: String,
    status: String
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' }
*/

/**
 * Visualização de item do tipo Totem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Totem
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.totem.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['titulo', 'codigo'], label: 'Totem' }
  }, [])
  const getOptionLabel = (option: any) => {
    return option ? `${option.titulo} - ${option.codigo}` : ''
  }
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
