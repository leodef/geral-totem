import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/**
 * Visualização de item do tipo Totem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Totem
 */
export const Resume = (props: any) => {
  /*
    Totem :
      codigo: String,
      titulo: String,
      desc: String,
      status: String
  */
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.codigo}
    </Typography>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.status}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
  </React.Fragment>)
}
