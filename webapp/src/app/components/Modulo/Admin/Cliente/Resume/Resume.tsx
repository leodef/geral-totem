import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/*
  Cliente :
    nome: String,
    cpf: String,
    email: String,
    telefone: String,
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
*/

/**
 * Visualização de item do tipo Cliente
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Cliente
 */
export const Resume = (props: any) => {
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.nome}
    </Typography>
    <Typography variant="h5" component="h2">
      {item.cpf}
    </Typography>
    <Typography variant="body2" component="p">
      {item.email}
    </Typography>
    <Typography variant="body2" component="p">
      {item.telefone}
    </Typography>
    <Typography variant="body2" component="p">
      {item.empresa.nome}
    </Typography>
    <Typography variant="body2" component="p">
      {item.totem.titulo}
    </Typography>
  </React.Fragment>)
}
