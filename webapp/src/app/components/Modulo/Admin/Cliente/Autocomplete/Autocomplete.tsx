import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Receita'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/*
  Cliente :
    nome: String,
    cpf: String,
    email: String,
    telefone: String,
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
*/

/**
 * Visualização de item do tipo Cliente
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Cliente
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.cliente.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['nome', 'cpf'], label: 'Cliente' }
  }, [])
  const getOptionLabel = (option: any) => option
    ? `${option.nome} - ${option.cpf}`
    : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
