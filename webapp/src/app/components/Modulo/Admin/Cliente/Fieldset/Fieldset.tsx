import
React, {
  useMemo,
  useEffect,
  useCallback,
  useContext,
  useState
} from 'react'
import {
  useField
} from 'formik'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'

import {
  findParentByPrefix,
  ParentCrudContext
} from '../../../../../contexts/ParentCrudContext'
import {
  Autocomplete as EmpresaAutocomplete
} from '../../Empresa/Autocomplete/Autocomplete'
import {
  Autocomplete as TotemAutocomplete
} from '../../Totem/Autocomplete/Autocomplete'
import './Fieldset.scss'
/*
  Cliente :
    nome: String,
    cpf: String,
    email: String,
    telefone: String,
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
*/

/**
 * Area de inputs para Cliente
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Cliente
 */
export const Fieldset = (props: any) => {
  const item = useMemo(() => props.item, [props.item])
  const itemTotem = useMemo(() => item ? item.totem : null, [item])
  const [totem, setTotem] = useState(itemTotem)
  const parentCrudContextValue = useContext(ParentCrudContext)

  const empresaParent = useMemo(
    () => {
      const parent = findParentByPrefix('empresa', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])
  const hideEmpresaParent = useMemo(() => Boolean(empresaParent), [empresaParent])

  const fields = useMemo(() => {
    return {
      nome: {
        name: getFieldName('nome', props)
      },
      cpf: {
        name: getFieldName('cpf', props)
      },
      email: {
        name: getFieldName('email', props)
      },
      telefone: {
        name: getFieldName('telefone', props)
      },
      empresa: {
        name: getFieldName('empresa', props)
      },
      totem: {
        name: getFieldName('totem', props)
      }
    }
  }, [props])

  const [, , empresaHelpers] = useField(fields.empresa.name)
  const [, , totemHelpers] = useField(fields.totem.name)

  const setEmpresa = useCallback(
    (empresa?: any) => {
      const empresaId = empresa ? empresa._id : null
      const parentId = (empresaParent && empresaParent._id)
        ? empresaParent._id
        : null
      empresaHelpers.setValue(parentId || empresaId || '')
    }, // eslint-disable-next-line
    [empresaParent]
  )
  const setTotemInput = useCallback(
    (totem?: any) => {
      const totemId = totem ? totem._id : null
      setTotem(totem)
      totemHelpers.setValue(totemId || '')
    }, // eslint-disable-next-line
    []
  )
  useEffect(() => setEmpresa(), [empresaParent, setEmpresa])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12} hidden={hideEmpresaParent}>

        <EmpresaAutocomplete
          onChange={(selected: any) => setEmpresa(selected)}
          value={empresaParent} />

      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.nome.name}
          type="text"
          label="Nome"
        />
      </Grid>

      <Grid item xs={12} sm={6} >
        <TextField
          name={fields.cpf.name}
          type="text"
          label="CPF"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.email.name}
          type="text"
          label="Email"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.telefone.name}
          type="text"
          label="Telefone"
        />
      </Grid>

      <Grid item xs={12}>

        <TotemAutocomplete
          onChange={(selected: any) => setTotemInput(selected)}
          value={totem}
        />

      </Grid>

    </Grid>
  )
}
