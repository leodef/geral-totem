import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import {
  Crud
} from '../../../Shared/Crud/Crud'
import {
  TablePropsField
} from '../../../Shared/Collection/Table/Table'
import {
  Resume
} from './Resume/Resume'
import {
  Fieldset
} from './Fieldset/Fieldset'
import {
  ClienteSchema,
  initialValues
} from '../../../../types/Modulo/Admin/Cliente'
import './Cliente.scss'

/*
  Cliente :
    nome: String,
    cpf: String,
    email: String,
    telefone: String,
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    totem: { type: Schema.Types.ObjectId, ref: 'Totem' },
*/

export const Cliente = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})
  const fields = useMemo(() => [
    new TablePropsField('nome', 'Nome'),
    new TablePropsField('cpf', 'CPF'),
    new TablePropsField('email', 'Email')
  ], [])

  const ShowBody = useCallback((props: any) => {
    return (<Resume {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset {...props} />)
  }, [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Cliente',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Cliente',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.nome ? value.nome.toString() : ''),
          secondary: null,
          avatar: (value.nome ? value.nome.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Cliente',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Cliente',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: ClienteSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o cliente ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
