import React from 'react'
import { Grid } from '@material-ui/core'
import {
  Resume as ReceitaResume
} from '../../Receita/Resume/Resume'
import {
  Resume as ClienteResume
} from '../../Cliente/Resume/Resume'
import {
  Resume as TotemResume
} from '../../Totem/Resume/Resume'
import './Resume.scss'

/**
 * Visualização de item do tipo Totem
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Totem
 */
export const Resume = (props: any) => {
/*
  RespostaTotem :
    totem: Totem
    cliente: Cliente
*/
  const {
    item,
    hideTotem,
    hideCliente
  } = props
  const { cliente, totem, receita } = item
  return (<Grid container>
    {hideCliente
      ? (<Grid item>
        <ClienteResume {...props} item={cliente} />
      </Grid>)
      : null}
    {hideTotem
      ? (<Grid item>
        <TotemResume {...props} item={totem} />
      </Grid>)
      : null}
    {receita
      ? (
        <Grid item>
          <ReceitaResume {...props} item={receita} />
        </Grid>)
      : null}
  </Grid>)
}
