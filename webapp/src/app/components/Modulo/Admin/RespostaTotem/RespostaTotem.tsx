import
React, {
  useMemo, useCallback, useContext
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import { Crud } from '../../../Shared/Crud/Crud'
import {
  TablePropsField
} from '../../../Shared/Collection/Table/Table'
import './RespostaTotem.scss'
import {
  ParentCrudContext,
  findParentByPrefix
} from '../../../../contexts/ParentCrudContext'
import { Grid } from '@material-ui/core'
import { Resume } from './Resume/Resume'

/*
  RespostaTotem :
    totem: Totem
    cliente: Cliente
*/

export const Totem = (props: any) => {
  const {
    collection,
    show
  } = props
  const { type } = (collection || {})

  const parentCrudContextValue = useContext(ParentCrudContext)
  const totemParent = useMemo(
    () => {
      const parent = findParentByPrefix('totem', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])
  const clienteParent = useMemo(
    () => {
      const parent = findParentByPrefix('cliente', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])

  const hideTotem = useMemo(
    () => Boolean(totemParent),
    [totemParent])

  const hideCliente = useMemo(
    () => Boolean(clienteParent),
    [clienteParent])

  const ShowBody = useCallback((props: any) => {
    return (<Grid container direction="column">
      <Grid item>
        <Resume
          {...props}
          hideTotem={hideTotem}
          hideCliente={hideCliente}
        />
      </Grid>
    </Grid>)
  }, [])

  const fields = useMemo(() => [
    ...(hideTotem
      ? []
      : [
        new TablePropsField('totem.titulo', 'Totem')
      ]
    ),
    ...(hideCliente
      ? []
      : [
        new TablePropsField('cliente.nome', 'Cliente')
      ]
    ),
    new TablePropsField({
      label: 'Volores',
      callback:
        (
          item: any,
          index: number,
          field: TablePropsField) => item && item.receita
          ? `Receita: ${item.receita.titulo}`
          : ''
    })
  ], [])

  const tableConfig = useMemo(() => {
    return {
      label: 'Totem',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Respostas',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST)
    ? listConfig
    : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Empresa',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      }
    }
    return params
  }, [
    collection,
    config,
    type,
    show,
    ShowBody
  ])
  return <Crud {...params} />
}
