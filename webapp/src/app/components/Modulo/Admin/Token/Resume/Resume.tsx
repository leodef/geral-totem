import React from 'react'
import {
  Typography
} from '@material-ui/core'
import './Resume.scss'

/*
  Token :
    titulo: String,
    desc: String,
    tipo: String,
    marca: String
*/

/**
 * Visualização de item do tipo Token
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Token
 */
export const Resume = (props: any) => {
  const { item } = props
  return (<React.Fragment>
    <Typography variant="h5" component="h2">
      {item.titulo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.desc}
    </Typography>
    <Typography variant="body2" component="p">
      {item.tipo}
    </Typography>
    <Typography variant="body2" component="p">
      {item.marca}
    </Typography>
  </React.Fragment>)
}
