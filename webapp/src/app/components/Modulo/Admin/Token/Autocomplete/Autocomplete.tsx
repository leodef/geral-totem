import React, { useCallback, useMemo } from 'react'
import { optionsType as types } from '../../../../../types/Modulo/Admin/Token'
import { AutocompleteContext } from '../../../../../contexts/AutocompleteContext'
import { Autocomplete as SharedAutocomplete } from '../../../../Shared/Autocomplete/Autocomplete'
import './Autocomplete.scss'

/*
  Token :
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    token: String,
    tipo: String,
    dataExpiracao: Date
*/

/**
 * Visualização de item do tipo Token
 * @param {ListProps} props Propriedades
 * @return {React.Component} Componente com visualização de item do tipo Token
 */
export const Autocomplete = (props: any) => {
  const getState = useCallback((state: any) => state.modulo.admin.token.options, [])
  const { fields, label } = useMemo(() => {
    return { fields: ['token', 'tipo'], label: 'Token' }
  }, [])
  const getOptionLabel = (option: any) => option
    ? `${option.tipo} - ${option.token}`
    : ''
  const getOptionId = (val: any) => val ? val._id : null
  const autocompleteContextValue = { getState, types, getOptionLabel, getOptionId, fields, label }

  return (<AutocompleteContext.Provider value={autocompleteContextValue}>
    <SharedAutocomplete {...props} />
  </AutocompleteContext.Provider>)
}
