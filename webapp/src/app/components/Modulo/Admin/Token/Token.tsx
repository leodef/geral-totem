import
React, {
  useMemo, useCallback
} from 'react'
import {
  Assignment as AssignmentIcon
} from '@material-ui/icons'
import {
  CollectionTypeEnum
} from '../../../../types/Crud'
import {
  Crud
} from '../../../Shared/Crud/Crud'
import {
  TablePropsField
} from '../../../Shared/Collection/Table/Table'
import {
  Fieldset
} from './Fieldset/Fieldset'
import {
  TokenSchema,
  initialValues
} from '../../../../types/Modulo/Admin/Token'
import {
  Resume
} from './Resume/Resume'
import './Token.scss'

/*
  Token :
    titulo: String,
    desc: String,
    tipo: String,
    marca: String
*/

export const Token = (props: any) => {
  const { collection, form, show, remove } = props
  const { type } = (collection || {})

  const fields = useMemo(() => [
    new TablePropsField('titulo', 'Título'),
    new TablePropsField('desc', 'Descrição')
  ], [])
  const ShowBody = useCallback((props: any) => {
    return (<Resume {...props} />)
  }, [])
  const FormBody = useCallback((props: any) => {
    return (<Fieldset {...props} />)
  }, [])
  const tableConfig = useMemo(() => {
    return {
      label: 'Token',
      align: 'center',
      actionsLabel: 'Ações',
      fields
    }
  }, [fields])
  const listConfig = useMemo(() => {
    return {
      dense: true,
      subheader: 'Token',
      fields,
      getListItem: (value: any, index: number) => {
        return {
          primary: (value.titulo ? value.titulo.toString() : ''),
          secondary: null,
          avatar: (value.titulo ? value.titulo.substr(0, 2).toUpperCase() : (<AssignmentIcon />))
        }
      }
    }
  }, [fields])
  const config = (type === CollectionTypeEnum.LIST) ? listConfig : tableConfig
  const params = useMemo(() => {
    const params = {
      collection: {
        type,
        config,
        // resolve: 'FRONT' // 'BACK'
        ...(collection || {})
      },
      show: {
        title: 'Token',
        // eslint-disable-next-line react/display-name
        body: ShowBody,
        ...(show || {})
      },
      form: {
        title: 'Token',
        // eslint-disable-next-line react/display-name
        body: FormBody,
        initialValues,
        schema: TokenSchema,
        ...(form || [])
      },
      remove: {
        title: 'Deseja realmente remover o token ?',
        ...(remove || {})
      }
    }
    return params
  }, [
    collection,
    config,
    form,
    remove,
    show,
    type,
    FormBody,
    ShowBody
  ])
  return <Crud {...params} />
}
