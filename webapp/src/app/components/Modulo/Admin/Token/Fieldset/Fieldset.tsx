import
React, {
  useMemo,
  useContext,
  useCallback,
  useEffect
} from 'react'
import {
  Grid
} from '@material-ui/core'
// eslint-disable-next-line no-unused-vars
import {
  getFieldName,
  TextField
} from '../../../../Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'
import {
  findParentByPrefix,
  ParentCrudContext
} from '../../../../../contexts/ParentCrudContext'
import {
  Autocomplete as UsuarioAutocomplete
} from '../../Usuario/Autocomplete/Autocomplete'

import './Fieldset.scss'
import { useField } from 'formik'

/*
  Token :
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    token: String,
    tipo: String,
    dataExpiracao: Date
*/

/**
 * Area de inputs para Token
 * @param {any} props Propriedades
 * @return {React.Component} Componente com area de inputs para Token
 */
export const Fieldset = (props: any) => {
  const parentCrudContextValue = useContext(ParentCrudContext)
  const usuarioParent = useMemo(
    () => {
      const parent = findParentByPrefix('usuario', parentCrudContextValue)
      return parent ? parent.item : null
    }, [parentCrudContextValue])

  const fields = useMemo(() => {
    return {
      usuario: {
        name: getFieldName('usuario', props)
      },
      token: {
        name: getFieldName('token', props)
      },
      tipo: {
        name: getFieldName('tipo', props)
      },
      dataExpiracao: {
        name: getFieldName('dataExpiracao', props)
      }
    }
  }, [props])
  const [, , usuarioHelpers] = useField(fields.usuario.name)
  const setUsuario = useCallback(
    (usuario?: any) => {
      const usuarioId = usuario ? usuario._id : null
      const parentId = (usuarioParent && usuarioParent._id)
        ? usuarioParent._id
        : null
      usuarioHelpers.setValue(parentId || usuarioId || '')
    }, // eslint-disable-next-line
    [usuarioParent]
  )
  useEffect(() => setUsuario(), [usuarioParent, setUsuario])

  return (
    <Grid container spacing={2} >

      <Grid item xs={12}>

        <UsuarioAutocomplete
          onChange={(selected: any) => setUsuario(selected)}
          value={usuarioParent} />

      </Grid>

      <Grid item xs={12} sm={6}>

        <TextField
          name={fields.token.name}
          type="text"
          label="Token"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.tipo.name}
          type="text"
          label="Tipo"
        />
      </Grid>

      <Grid item xs={12} sm={6}>
        <TextField
          name={fields.dataExpiracao.name}
          type="text"
          label="Data de Expiracao"
        />
      </Grid>

    </Grid>
  )
}
