import React, { useMemo, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Publico } from './Publico/Publico'
import { Modulo } from './Modulo/Modulo'
import { AuthType } from '../types/Auth'
import './App.scss'

/**
 * Componente principal da aplicação
 *   Faz a validação se o usuario esta autenticado para acessar o componente correto
 * @param {any}  props Propriedades
 * @return {React.Component} Componente que envolve toda a aplicação
 */
export const App = (props: any) => {
  const { token } = useSelector((state: any) => (state.auth || {} as any))
  const isAuth = useMemo(() => !!token, [token])
  // dispatch
  const dispatch = useDispatch()
  // Carregamento de dados iniciais
  const loadAuth = useCallback(() =>
    dispatch({
      type: AuthType.LOAD
    }), [dispatch])

  useEffect(() => {
    loadAuth()
  }, [loadAuth])

  return isAuth ? (<Modulo />) : (<Publico />)
}

export default App
