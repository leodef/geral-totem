import React from 'react'
import { useDispatch } from 'react-redux'
import {
  withFormik
} from 'formik'
import {
  Avatar,
  Button,
  CssBaseline,
  FormControlLabel,
  Checkbox,
  Grid,
  Box,
  Typography,
  Container,
  Link
} from '@material-ui/core'
import {
  LockOutlined as LockOutlinedIcon
} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { AuthType } from '../../../types/Auth'
import { Copyright } from '../../../components/Shared/App/Copyright/Copyright'
import {
  LoginSchema,
  initialValues
} from '../../../types/Publico/Login'
import {
  TextField
} from '../../../components/Shared/Utils/FormikMaterialUIField/FormikMaterialUIField'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

/**
 * Tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com tela de login
 */
export default function SignIn (props: any) {
  const classes = useStyles()
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <LoginHeader />
        <LoginForm />
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  )
}

/**
 * Cabeçalho da tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com cabeçalho da tela de login
 */
const LoginHeader = React.memo((props: any) => {
  LoginHeader.displayName = 'LoginHeader'
  const classes = useStyles()
  return (
    <React.Fragment>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
    </React.Fragment>
  )
})

/**
 * Formulário da tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com formulário da tela de login
 */
function LoginForm (props: any) {
  const dispatch = useDispatch()
  const login = (payload: any) => dispatch({ type: AuthType.AUTH, payload })

  const WithFormik = withFormik({
    mapPropsToValues: (props: any) => initialValues,
    // LoginSchema
    handleSubmit: (values: any, params: any) => {
      const { setSubmitting } = params
      setSubmitting(false)
      login(values)
    },
    validationSchema: LoginSchema,
    displayName: 'LoginForm'
  })(LoginFormBody)
  return (<WithFormik {...props} />)
}

/**
 * Conteudo do formulário da tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com conteudo do formulário da tela de login
 */
const LoginFormBody = React.memo((props: any) => {
  LoginFormBody.displayName = 'LoginFormBody'
  const { handleSubmit } = props
  const classes = useStyles()
  return (<form onSubmit={handleSubmit}>
    <Fieldset />
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="primary"
      className={classes.submit}
    >
      Login
    </Button>
    <LoginLinks />
  </form>
  )
})

/**
 * Campos do formulário da tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com campos do formulário da tela de login
 */
const Fieldset = React.memo((props: any) => {
  Fieldset.displayName = 'Fieldset'
  return (
    <fieldset>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="username"
        label="Usuário"
        name="username"
        autoComplete="username"
        autoFocus
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Senha"
        type="password"
        id="password"
        autoComplete="current-password"
      />
      <FormControlLabel
        control={<Checkbox value="remember" color="primary" />}
        label="Remember me"
      />
    </fieldset>
  )
})

/**
 * Botões do formulário da tela de login
 * @param {any} props Propriedades
 * @return {React.Component} Componente com botões do formulário da tela de login
 */
const LoginLinks = React.memo((props: any) => {
  LoginLinks.displayName = 'LoginLinks'
  return (
    <Grid container>
      <Grid item xs>
        <Link href="#" variant="body2">
          Esqueceu a senha?
        </Link>
      </Grid>
      <Grid item>
        <Link href="#" variant="body2">
          {'Ainda não tem conta? Cadastre-se'}
        </Link>
      </Grid>
    </Grid>
  )
})
