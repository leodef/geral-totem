import React from 'react'
import { Switch, Route } from 'react-router-dom'
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Button
} from '@material-ui/core'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Menu as MenuIcon
} from '@material-ui/icons'
import Login from './Login/Login'
import './Publico.scss'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    }
  })
)

/**
 * Roteamento para telas abertas do sistema
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas abertas do sistema
 */
export const Publico = (props: any) => {
  const classes = useStyles()
  return (
    <React.Fragment>
      <AppBar
        position="relative">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu">
            <MenuIcon></MenuIcon>
          </IconButton>
          <Typography
            variant="h6"
            className={classes.title}>
            News
          </Typography>
          <Button
            color="inherit">
              Login
          </Button>
        </Toolbar>
      </AppBar>
      <main>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </main>
    </React.Fragment>)
}
export default Publico
