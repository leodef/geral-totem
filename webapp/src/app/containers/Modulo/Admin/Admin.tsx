import React from 'react'
import { ProtectedRoute } from '../../../components/Shared/App/ProtectedRoute/ProtectedRoute'
import { Empresa } from './Empresa/Empresa'
import { Midia } from './Midia/Midia'
import { Usuario } from './Usuario/Usuario'
import './Admin.scss'

/**
 * Roteamento para telas do modulo admin
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com roteamento para telas do modulo admin
 */
export const Admin = React.memo((props: any) => {
  Admin.displayName = 'Admin'
  return (
    <React.Fragment>
      <ProtectedRoute exact path="/admin/empresa" component={Empresa} />
      <ProtectedRoute exact path="/admin/usuario" component={Usuario} />
      <ProtectedRoute exact path="/admin/midia" component={Midia} />
    </React.Fragment>
  )
})
