/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Receita } from './Receita'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Receita />, div)
  ReactDOM.unmountComponentAtNode(div)
})
