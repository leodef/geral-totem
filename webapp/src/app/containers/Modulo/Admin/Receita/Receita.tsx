import React, { useMemo, useCallback } from 'react'
import { Receita as ReceitaComponent } from '../../../../components/Modulo/Admin/Receita/Receita'
import { crudType as types } from '../../../../types/Modulo/Admin/Receita'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Receita.scss'

/**
 * Tela de gerenciameto de Receita
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Receita
 */
export const Receita = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.receita.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'receita'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ReceitaComponent collection={collection} />
  </CrudContext.Provider>)
}
