import React, { useMemo, useCallback } from 'react'
import {
  Usuario as UsuarioComponent
} from '../../../../components/Modulo/Admin/Usuario/Usuario'
import { crudType as types } from '../../../../types/Modulo/Admin/Usuario'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Usuario.scss'

/**
 * Tela de gerenciameto de Usuario
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Usuario
 */
export const Usuario = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.usuario.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'usuario'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <UsuarioComponent collection={collection} />
  </CrudContext.Provider>)
}
