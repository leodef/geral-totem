import React, { useMemo, useCallback } from 'react'
import { Cliente as ClienteComponent } from '../../../../components/Modulo/Admin/Cliente/Cliente'
import { crudType as types } from '../../../../types/Modulo/Admin/Cliente'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Cliente.scss'

/**
 * Tela de gerenciameto de Cliente
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Cliente
 */
export const Cliente = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.cliente.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'cliente'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <ClienteComponent collection={collection} />
  </CrudContext.Provider>)
}
