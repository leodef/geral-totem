import React, { useMemo, useCallback } from 'react'
import { Totem as TotemComponent } from '../../../../components/Modulo/Admin/Totem/Totem'
import { crudType as types } from '../../../../types/Modulo/Admin/Totem'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Totem.scss'

/**
 * Tela de gerenciameto de Totem
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Totem
 */
export const Totem = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.totem.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'totem'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <TotemComponent collection={collection} />
  </CrudContext.Provider>)
}
