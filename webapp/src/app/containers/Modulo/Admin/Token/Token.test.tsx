/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { Token } from './Token'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Token />, div)
  ReactDOM.unmountComponentAtNode(div)
})
