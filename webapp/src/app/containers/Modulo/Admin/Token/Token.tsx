import React, { useMemo, useCallback } from 'react'
import { Token as TokenComponent } from '../../../../components/Modulo/Admin/Token/Token'
import { crudType as types } from '../../../../types/Modulo/Admin/Token'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Token.scss'

/**
 * Tela de gerenciameto de Token
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Token
 */
export const Token = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.token.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'token'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <TokenComponent collection={collection} />
  </CrudContext.Provider>)
}
