import React, { useMemo, useCallback } from 'react'
import { UsuarioEmpresa as UsuarioEmpresaComponent } from '../../../../components/Modulo/Admin/UsuarioEmpresa/UsuarioEmpresa'
import { crudType as types } from '../../../../types/Modulo/Admin/UsuarioEmpresa'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './UsuarioEmpresa.scss'

/**
 * Tela de gerenciameto de UsuarioEmpresa
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de UsuarioEmpresa
 */
export const UsuarioEmpresa = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.usuarioEmpresa.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'usuarioEmpresa'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <UsuarioEmpresaComponent collection={collection} />
  </CrudContext.Provider>)
}
