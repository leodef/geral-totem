import React, { useMemo, useCallback } from 'react'
import { Midia as MidiaComponent } from '../../../../components/Modulo/Admin/Midia/Midia'
import { crudType as types } from '../../../../types/Modulo/Admin/Midia'
import { CrudContext } from '../../../../contexts/CrudContext'
import { CollectionTypeEnum } from '../../../../types/Crud'
import './Midia.scss'

/**
 * Tela de gerenciameto de Midia
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com tela de gerenciameto de Midia
 */
export const Midia = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.midia.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'midia'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    pagination: { limit: 10 },
    type: CollectionTypeEnum.TABLE
    // resolve: 'FRONT'
  }
  // render
  return (<CrudContext.Provider value={crudContextValue}>
    <MidiaComponent collection={collection} />
  </CrudContext.Provider>)
}
