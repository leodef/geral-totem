import React from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons/'
import { AdminGrid } from './AdminGrid/AdminGrid'
import './Dashboard.scss'

const panelUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular
    }
  })
)

/**
 * Painel de controle para modulos
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com painel de controle para modulos
 */
export const Dashboard = React.memo((props: any) => {
  Dashboard.displayName = 'Dashboard'
  const classes = panelUseStyles()

  return (
    <div className={classes.root}>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>
            Area Administrativa
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <AdminGrid />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  )
})
