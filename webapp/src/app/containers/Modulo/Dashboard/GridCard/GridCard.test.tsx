/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { GridCard } from './GridCard'
import { red } from '@material-ui/core/colors'

it('renders without crashing', () => {
  const props = {
    avatar: 'TS',
    title: 'Test',
    subheader: 'Test',
    content: 'Test',
    to: '/',
    color: red
  }
  const div = document.createElement('div')
  ReactDOM.render(<GridCard {...props} />, div)
  ReactDOM.unmountComponentAtNode(div)
})
