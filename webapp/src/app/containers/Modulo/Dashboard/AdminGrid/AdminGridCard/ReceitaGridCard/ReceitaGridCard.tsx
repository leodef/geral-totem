import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Receita'
import { Receita } from '../../../../../../components/Modulo/Admin/Receita/Receita'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ReceitaGridCard.scss'

export const ReceitaGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.receita.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'receita'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="RE"
    title="Receita"
    subheader="Gerenciamento de receita"
    content="Gerenciamento de receita"
    to="/admin/receita"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Receita collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
