import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Empresa'
import { Empresa } from '../../../../../../components/Modulo/Admin/Empresa/Empresa'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './EmpresaGridCard.scss'

export const EmpresaGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.empresa.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'empresa'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="EM"
    title="Empresa"
    subheader="Gerenciamento de empresas"
    content="Gerenciamento de empresas"
    to="/admin/empresa"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Empresa collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
