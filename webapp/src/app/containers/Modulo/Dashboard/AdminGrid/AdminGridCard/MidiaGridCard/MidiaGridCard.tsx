import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Midia'
import { Midia } from '../../../../../../components/Modulo/Admin/Midia/Midia'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './MidiaGridCard.scss'

export const MidiaGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.midia.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'midia'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="MI"
    title="Midia"
    subheader="Gerenciamento de midia"
    content="Gerenciamento de midia"
    to="/admin/midia"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Midia collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
