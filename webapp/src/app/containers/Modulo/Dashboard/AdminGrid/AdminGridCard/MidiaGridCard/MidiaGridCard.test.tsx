/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { MidiaGridCard } from './MidiaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MidiaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
