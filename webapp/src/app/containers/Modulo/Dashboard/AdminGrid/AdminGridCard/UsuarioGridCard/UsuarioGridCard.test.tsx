/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { UsuarioGridCard } from './UsuarioGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<UsuarioGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
