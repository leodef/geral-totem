/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ClienteGridCard } from './ClienteGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ClienteGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
