/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { ReceitaGridCard } from './ReceitaGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ReceitaGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
