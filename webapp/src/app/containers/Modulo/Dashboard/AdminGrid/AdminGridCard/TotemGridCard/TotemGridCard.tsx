import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Receita'
import { Totem } from '../../../../../../components/Modulo/Admin/Totem/Totem'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './TotemGridCard.scss'

export const TotemGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.totem.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'totem'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="TT"
    title="Totens"
    subheader="Gerenciamento de totem"
    content="Gerenciamento de totem"
    to="/admin/totem"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Totem collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
