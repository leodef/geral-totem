import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Cliente'
import { Cliente } from '../../../../../../components/Modulo/Admin/Cliente/Cliente'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './ClienteGridCard.scss'

export const ClienteGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.cliente.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'cliente'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="CL"
    title="Clientes"
    subheader="Gerenciamento de clientes"
    content="Gerenciamento de clientes"
    to="/admin/cliente"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Cliente collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
