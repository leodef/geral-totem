/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TokenGridCard } from './TokenGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TokenGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
