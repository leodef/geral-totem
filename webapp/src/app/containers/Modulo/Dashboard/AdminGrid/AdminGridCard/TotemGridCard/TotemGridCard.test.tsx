/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { TotemGridCard } from './TotemGridCard'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TotemGridCard />, div)
  ReactDOM.unmountComponentAtNode(div)
})
