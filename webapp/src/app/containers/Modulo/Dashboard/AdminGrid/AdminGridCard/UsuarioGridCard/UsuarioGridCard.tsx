import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Receita'
import { Usuario } from '../../../../../../components/Modulo/Admin/Usuario/Usuario'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './UsuarioGridCard.scss'

export const UsuarioGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.usuario.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'usuario'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="US"
    title="Usuários"
    subheader="Gerenciamento de usuário"
    content="Gerenciamento de usuário"
    to="/admin/usuario"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Usuario collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
