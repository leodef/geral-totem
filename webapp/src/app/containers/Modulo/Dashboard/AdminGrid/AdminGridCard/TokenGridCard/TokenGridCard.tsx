import React, { useCallback, useMemo } from 'react'
import {
  red
} from '@material-ui/core/colors'
import { CollectionTypeEnum } from '../../../../../../types/Crud'
import { GridCard } from '../../../GridCard/GridCard'
import { crudType as types } from '../../../../../../types/Modulo/Admin/Receita'
import { Token } from '../../../../../../components/Modulo/Admin/Token/Token'
import { CrudContext } from '../../../../../../contexts/CrudContext'
import './TokenGridCard.scss'

export const TokenGridCard = (props: any) => {
  // CrudContext
  const getState = useCallback((state: any) => state.modulo.admin.token.crud, [])
  const actions = React.useMemo(() => { return { toEdit: true, toRemove: true, toNew: true, toShow: true, toBack: true, submit: true } }, [])
  const prefix = 'token'
  const crudContextValue = useMemo(() => {
    return { getState, actions, types, prefix }
  },
  [getState, actions])
  const collection = {
    type: CollectionTypeEnum.LIST,
    pagination: { limit: 5 }
  }
  // render
  return (<GridCard
    avatar="TK"
    title="Tokens"
    subheader="Gerenciamento de tokens"
    content="Gerenciamento de tokens"
    to="/admin/token"
    color={red}>
    <CrudContext.Provider value={crudContextValue}>
      <Token collection={collection}/>
    </CrudContext.Provider>
  </GridCard>)
}
