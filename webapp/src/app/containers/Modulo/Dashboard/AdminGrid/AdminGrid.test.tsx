/* eslint-disable no-undef */
import React from 'react'
import ReactDOM from 'react-dom'
import { AdminGrid } from './AdminGrid'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AdminGrid />, div)
  ReactDOM.unmountComponentAtNode(div)
})
