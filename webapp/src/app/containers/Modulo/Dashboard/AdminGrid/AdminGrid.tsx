import React from 'react'
import {
  makeStyles,
  // eslint-disable-next-line no-unused-vars
  Theme,
  createStyles
} from '@material-ui/core/styles'
import {
  Grid
} from '@material-ui/core'
import {
  EmpresaGridCard
} from './AdminGridCard/EmpresaGridCard/EmpresaGridCard'
import {
  UsuarioGridCard
} from './AdminGridCard/UsuarioGridCard/UsuarioGridCard'
import {
  MidiaGridCard
} from './AdminGridCard/MidiaGridCard/MidiaGridCard'
import './AdminGrid.scss'

const gridUseStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    }
  })
)
/** Area administrativa do painel de controle
 * Area administrativa do painel de controle
 * @param {any}  props Propriedades
 * @return {React.Component} Componente com area administrativa do painel de controle
 */
export const AdminGrid = React.memo((props: any) => {
  AdminGrid.displayName = 'AdminGrid'
  const classes = gridUseStyles()
  return (
    <Grid
      container
      justify="center"
      className={classes.root} spacing={4}>
      <Grid item>
        <EmpresaGridCard />
      </Grid>
      <Grid item>
        <UsuarioGridCard />
      </Grid>
      <Grid item>
        <MidiaGridCard />
      </Grid>
    </Grid>
  )
})
