import React from 'react'
const crudContextDefaultValue = {
  getState: null as any,
  actions: null as any,
  types: null as any,
  prefix: ''
} as any
const CrudContext = React.createContext(crudContextDefaultValue)
//  Provider Consumer
export { CrudContext }
