import React from 'react'

const autocompleteContextDefaultValue = {
  getState: null as any,
  types: null as any,
  getOptionLabel: null as any,
  getOptionId: (val: any): any => val ? val._id : null,
  fields: [],
  label: null as any,
  resolve: 'FRONT',
  onChange: null as any,
  value: null as any
} as any
const AutocompleteContext = React.createContext(autocompleteContextDefaultValue)
//  Provider Consumer
export { AutocompleteContext }
