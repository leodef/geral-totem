import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'
// eslint-disable-next-line no-unused-vars
import { Empresa } from './Empresa'
// eslint-disable-next-line no-unused-vars
import { Totem } from './Totem'

/*
  Cliente
    nome: string
    cpf: string
    email: string
    telefone: string
    empresa: Empresa
    totem: Totem
    // verificar campos cadastro
*/

export const crudType = new CrudType('CLIENTE')
export const optionsType = new OptionsType('CLIENTE')

export class Cliente {
    nome: String | null = null;
    cpf: String | null = null;
    email: String | null = null;
    telefone: String | null = null;
    empresa: Empresa | null = null;
    totem: Totem | null = null;
}

export const ClienteSchema = Yup.object().shape({
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  cpf: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  telefone: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  empresa: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  totem: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
})

export const initialValues = {
  nome: '',
  cpf: '',
  email: '',
  telefone: '',
  empresa: '',
  totem: ''
}
