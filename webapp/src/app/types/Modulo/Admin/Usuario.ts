import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'

/*
  Usuario
    usuario: string
    senha: string
    nome: string
    status: string
*/
export const crudType = new CrudType('USUARIO')
export const optionsType = new OptionsType('USUARIO')

export class Usuario {
  usuario: String | null = null;
  senha: String | null = null;
  nome: String | null = null;
  status: String | null = null;
}

export const CreateUsuarioSchema = Yup.object().shape({
  usuario: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  senha: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  status: Yup.string()
})

export const EditUsuarioSchema = Yup.object().shape({
  usuario: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  status: Yup.string()
})

export const createInitialValues = {
  usuario: '',
  senha: '',
  nome: '',
  status: ''
}

export const editInitialValues = {
  usuario: '',
  nome: '',
  status: ''
}
