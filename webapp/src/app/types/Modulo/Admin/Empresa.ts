import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'
/*
  Empresa
    nome: string
    desc: string
    cnpj: string
*/
export const crudType = new CrudType('EMPRESA')
export const optionsType = new OptionsType('EMPRESA')

export class Empresa {
  nome: String | null = null;
  desc: String | null = null;
  cnpj: String | null = null;
}

export const EmpresaSchema = Yup.object().shape({
  nome: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!'),
  cnpj: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
})

export const initialValues = {
  nome: '',
  desc: '',
  cnpj: ''
}
