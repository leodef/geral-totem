import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'
// eslint-disable-next-line no-unused-vars
import { Midia } from './Midia'
// eslint-disable-next-line no-unused-vars
import { Empresa } from './Empresa'

/*
  Receita
    titulo: string
    desc: string
    pagina: string // gravar uma pagina em html
    video: Midia
    image: Midia
    empresa: Empresa
*/
export const crudType = new CrudType('RECEITA')
export const optionsType = new OptionsType('RECEITA')

export class Receita {
  titulo: String | null = null;
  desc: String | null = null;
  pagina: String | null = null;
  video: Midia | null = null;
  image: Midia | null = null;
  empresa: Empresa | null = null;
}

export const ReceitaSchema = Yup.object().shape({
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!'),
  pagina: Yup.string()
    .min(2, 'Too Short!')
    .required('Required'),
  video: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required'),
  image: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!')
    .required('Required'),
  empresa: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required')
})

export const initialValues = {
  titulo: '',
  desc: '',
  pagina: '',
  video: '',
  image: '',
  empresa: ''
}
