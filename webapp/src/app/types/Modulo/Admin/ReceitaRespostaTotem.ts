import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'
import {
  // eslint-disable-next-line no-unused-vars
  Receita
} from './Receita'
// eslint-disable-next-line no-unused-vars
import {
  // eslint-disable-next-line no-unused-vars
  RespostaTotem
} from './RespostaTotem'

/*
  ReceitaRespostaRespostaTotem
    resposta: RespostaRespostaTotem
    receita: Receita
*/
export const crudType = new CrudType('RECEITA_RESPOSTA_TOTEM')
export const optionsType = new OptionsType('RECEITA_RESPOSTA_TOTEM')

export class ReceitaRespostaRespostaTotem {
  resposta?: RespostaTotem
  receita?: Receita
}

export const initialValues = {
  resposta: '',
  receita: ''
}
