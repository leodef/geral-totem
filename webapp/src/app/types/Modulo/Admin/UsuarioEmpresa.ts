import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'
// eslint-disable-next-line no-unused-vars
import { Empresa } from './Empresa'
import {
  // eslint-disable-next-line no-unused-vars
  Usuario,
  CreateUsuarioSchema,
  EditUsuarioSchema,
  createInitialValues as createUsuarioInitialValues,
  editInitialValues as editUsuarioInitialValues
} from './Usuario'

/*
  UsuarioEmpresa
    empresa: Usuario
    usuario: Empresa
    tipo: string
*/
export const crudType = new CrudType('USUARIO_EMPRESA')
export const optionsType = new OptionsType('USUARIO_EMPRESA')

export class UsuarioEmpresa {
  empresa: Empresa | null = null;
  usuario: Usuario | null = null;
  tipo: String | null = null;
}

export const CreateUsuarioEmpresaSchema = Yup.object().shape({
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  empresa: Yup.string(),
  usuario: CreateUsuarioSchema
})
export const EditUsuarioEmpresaSchema = Yup.object().shape({
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  empresa: Yup.string(),
  usuario: EditUsuarioSchema
})

export const createInitialValues = {
  empresa: '',
  usuario: createUsuarioInitialValues,
  tipo: ''
}
export const editInitialValues = {
  empresa: '',
  usuario: editUsuarioInitialValues,
  tipo: ''
}
