import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'

/*
  Totem
    codigo: string
    titulo: string
    desc: string
    status: string
    empresa: string
*/
export const crudType = new CrudType('TOTEM')
export const optionsType = new OptionsType('TOTEM')

export class Totem {
  codigo: String | null = null;
  titulo: String | null = null;
  desc: String | null = null;
  status: Number | null = null;
  empresa: String | null = null
}

export const TotemSchema = Yup.object().shape({
  codigo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  desc: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!'),
  status: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  empresa: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
})

export const initialValues = {
  codigo: '',
  titulo: '',
  desc: '',
  status: '',
  empresa: ''
}
