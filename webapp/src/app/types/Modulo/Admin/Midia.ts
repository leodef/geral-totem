import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'

/*
  Midia
      formType: 'URI' | 'Upload'
      titulo: String,
      arquivo: Buffer
      uri: String

      encoding: String, // encoding: '7bit',
      mime: String, // mimetype: 'image/jpeg',
      pasta: String, //destination: './public/data/uploads/undefined',
      nomeArquivo: String, // filename: '1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      local :String, // path: 'public\\data\\uploads\\undefined\\1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
      tamanho: Number, // size: 5348
      extensao: String,
*/

export const crudType = new CrudType('MIDIA')
export const optionsType = new OptionsType('MIDIA')

export class Midia {
  formType: 'URI' | 'Upload' | null = null;
  titulo: String | null = null;
  arquivo: Buffer | null =null
  uri: String | null = null;
  encoding: String | null = null; // encoding: '7bit',
  mime: String | null = null; // mimetype: 'image/jpeg',
  pasta: String | null = null; // destination: './public/data/uploads/undefined',
  nomeArquivo: String | null = null; // filename: '1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
  local: String | null = null; // path: 'public\\data\\uploads\\undefined\\1593448998711-5f96a013667302e00f6efecc28a9e98dfad96450.jpg',
  tamanho: String | null = null; // size: 5348
  extensao: String | null = null;
  src?: any;
}

export const MidiaSchema = Yup.object().shape({
  formType: Yup.string(),
  titulo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  uri: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Too Long!'),
  arquivo: Yup.mixed()
})

export const initialValues = {
  formType: '',
  titulo: '',
  uri: '',
  arquivo: ''
}
