import * as Yup from 'yup'
import { CrudType } from '../../Crud'
import { OptionsType } from '../../Options'

/*
Token
  usuario: Usuario
  token: string
  tipo: stirng
  dataExpiracao: Date
*/
export const crudType = new CrudType('TOKEN')
export const optionsType = new OptionsType('TOKEN')

export class Token {
  usuario: String | null = null;
  token: String | null = null;
  tipo: String | null = null;
  dataExpiracao: Date | null = null;
}

export const TokenSchema = Yup.object().shape({
  usuario: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  token: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  tipo: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  dataExpiracao: Yup.date()
    .required('Required')
})

export const initialValues = {
  usuario: '',
  token: '',
  tipo: '',
  dataExpiracao: new Date()
}
