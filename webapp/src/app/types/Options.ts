
export class OptionsType {
  constructor (
    public config: any = null,
    public name: string = 'OPTIONS_ITEM',
    public SET_FILTER_OPTIONS_ITEM: string = 'SET_FILTER_OPTIONS_ITEM',
    public SET_FIELDS_OPTIONS_ITEM: string = 'SET_FIELDS_OPTIONS_ITEM',
    public CLEAR_OPTIONS_ITEM: string = 'CLEAR_OPTIONS_ITEM',

    // FETCH
    public FETCH_OPTIONS_ITEM: string = 'FETCH_OPTIONS_ITEM',

    // PENDING - FETCH
    public FETCH_OPTIONS_ITEM_PENDING: string = 'FETCH_OPTIONS_ITEM_PENDING',

    // SUCCESS - FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM_SUCCESS: string = 'FETCH_OPTIONS_ITEM_SUCCESS',

    // FAILURE - FETCH CREATE DELETE UPDATE
    public FETCH_OPTIONS_ITEM_FAILURE: string = 'FETCH_OPTIONS_ITEM_FAILURE'

  ) {
    if (config) {
      this.load(config)
    }
  }

  load (config: any = {}) {
    let name = 'ITEM'
    if (config && typeof config === 'string') {
      name = config.toUpperCase()
      config = { name }
    }
    this.name = name
    this.config = config
    this.SET_FILTER_OPTIONS_ITEM = (
      config.SET_FILTER_OPTIONS_ITEM ||
      `SET_FILTER_OPTIONS_${name}`)
    this.SET_FIELDS_OPTIONS_ITEM = (
      config.SET_FIELDS_OPTIONS_ITEM ||
      `SET_FIELDS_OPTIONS_${name}`)
    this.CLEAR_OPTIONS_ITEM = (
      config.CLEAR_OPTIONS_ITEM ||
      `CLEAR_OPTIONS_${name}`)

    // FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM = (
      config.FETCH_OPTIONS_ITEM ||
      `FETCH_OPTIONS_${name}`)

    // PENDING - FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM_PENDING = (
      config.FETCH_OPTIONS_ITEM_PENDING ||
      `FETCH_OPTIONS_${name}_PENDING`)

    // SUCCESS - FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM_SUCCESS = (
      config.FETCH_OPTIONS_ITEM_SUCCESS ||
      `FETCH_OPTIONS_${name}_SUCCESS`)

    // FAILURE - FETCH CREATE DELETE UPDATE
    this.FETCH_OPTIONS_ITEM_FAILURE = (
      config.FETCH_OPTIONS_ITEM_FAILURE ||
      `FETCH_OPTIONS_${name}_FAILURE`)

    return this
  }
}
