/* eslint-disable no-unused-vars */

export enum MessageType {
    SHOW_MESSAGE = 'SHOW_MESSAGE',
    ADD_MESSAGE = 'ADD_MESSAGE',
    REMOVE_MESSAGE = 'REMOVE_MESSAGE'
}

export enum MessageVariant {
    primary = 'primary',
    secondary = 'secondary',
    success = 'success',
    danger = 'danger',
    warning = 'warning',
    info = 'info',
    light = 'light',
    dark = 'dark',
}
