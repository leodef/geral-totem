


export enum StatusEntrega {
    'ABERTA' = 1,
    'FECHADA' = 2,
    'INICIADA' = 3,
    'FINALIZADA' = 4
}