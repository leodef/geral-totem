/* eslint-disable no-unused-vars */
export enum AuthType {
    AUTH = 'AUTH',
    LOAD = 'AUTH_LOAD',
    LOAD_SUCCESS = 'LOAD_SUCCESS',
    AUTH_PENDING = 'AUTH_PENDING',
    AUTH_SUCCESS = 'AUTH_SUCCESS',
    AUTH_FAILURE = 'AUTH_FAILURE',
    AUTH_LOGOUT = 'AUTH_LOGOUT'
}

export const loginNav = '/login'
export const logoutNav = '/login'
export const authNav = '/empresa'