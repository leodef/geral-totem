import { takeLatest, put, call, all } from 'redux-saga/effects' // select
import { AuthType, authNav, logoutNav } from '../types/Auth'
import { AuthService } from '../services/Auth'
import { push } from 'connected-react-router'
import { LoadingType } from '../types/Loading'
import { MessageVariant, MessageType } from '../types/Message'

export class AuthSaga {
  static getSagasFunction () {
    const obj = new AuthSaga()
    return obj.getSagasFunction()
  }

  static getSagas () {
    const obj = new AuthSaga()
    return obj.getSagas()
  }

  // eslint-disable-next-line no-useless-constructor
  constructor (
    public service: AuthService = new AuthService()
  ) { }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    yield all([
      takeLatest(AuthType.AUTH, context.login.bind(context)),
      takeLatest(AuthType.LOAD, context.load.bind(context)),
      takeLatest([
        AuthType.AUTH_SUCCESS,
        AuthType.AUTH_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public * msg (action: any) {
    let msg = null
    let variant = MessageVariant.primary
    switch (action.type) {
      case AuthType.AUTH_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      case AuthType.AUTH_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW_MESSAGE, payload })
    yield call(console.log, msg)
  }

  public * login (action: any) {
    const context = this
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'login' })
    yield put({ type: AuthType.AUTH_PENDING, payload: action.payload })
    try {
      const postsFromApi = yield call(service.login.bind(service), action.payload)
      console.log('postsFromApi', postsFromApi)
      yield put({ type: AuthType.AUTH_SUCCESS, payload: postsFromApi })
      // ##!! se admin
      yield put(push(authNav))
    } catch (error) {
      yield put({ type: AuthType.AUTH_FAILURE, payload: [], error })
      console.error(error); // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'login' })
    }
  }

  public * logout () {
    yield put({ type: AuthType.AUTH_LOGOUT })
    yield put(push(logoutNav))
  }

  public * load () {
    const context = this
    const service = context.service

    const data = yield call(service.load.bind(service))
    console.log('load data', data)
    yield put({ type: AuthType.LOAD_SUCCESS, payload: data })
    if (data && data.token) {
      yield put(push(authNav))
    }
    return null
  }
}
export default AuthSaga.getSagasFunction()
