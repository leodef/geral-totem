import { takeLatest, put, call, all, debounce } from 'redux-saga/effects' // select
// eslint-disable-next-line no-unused-vars
import { OptionsType } from '../types/Options'
// eslint-disable-next-line no-unused-vars
import { OptionsService } from '../services/Options'
import { MessageVariant, MessageType } from '../types/Message'
import { LoadingType } from '../types/Loading'

export class OptionsSaga {
  config: OptionsType
  service: OptionsService

  static getSagasFunction (
    config: OptionsType,
    service: OptionsService) {
    const obj = new OptionsSaga(config, service)
    return obj.getSagasFunction()
  }

  static getSagas (
    config: OptionsType,
    service: OptionsService) {
    const obj = new OptionsSaga(config, service)
    return obj.getSagas()
  }

  constructor (
    config: OptionsType,
    service: OptionsService) {
    this.config = config
    this.service = service
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    const config = context.config
    yield all([
      debounce(600, config.FETCH_OPTIONS_ITEM, context.fetchOptions.bind(context)),
      takeLatest([
        // SUCCESS
        config.FETCH_OPTIONS_ITEM_SUCCESS,
        // FAILURE
        config.FETCH_OPTIONS_ITEM_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public getItem (action: any) {
    const { item } = action.payload
    return item
  }

  public * msg (action: any) {
    const context = this
    const config = context.config
    let variant = MessageVariant.primary
    let msg = null
    switch (action.type) {
      case config.FETCH_OPTIONS_ITEM_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case config.FETCH_OPTIONS_ITEM_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW_MESSAGE, payload })
    yield call(console.log, msg)
  }

  public * fetchOptions (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'fetchOptions' })
    yield put({ type: config.FETCH_OPTIONS_ITEM_PENDING })
    try {
      const postsFromApi = yield call(
        service.fetchOptions.bind(service), action.payload)
      yield put({
        type: config.FETCH_OPTIONS_ITEM_SUCCESS,
        payload: postsFromApi
      })
    } catch (error) {
      yield put({ type: config.FETCH_OPTIONS_ITEM_FAILURE, payload: { items: [], error } })
      console.error(error); // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'fetchOptions' })
    }
  }
}
