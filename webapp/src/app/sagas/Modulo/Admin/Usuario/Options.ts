import service from '../../../../services/Modulo/Admin/Usuario/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Usuario'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
