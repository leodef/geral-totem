import service from '../../../../services/Modulo/Admin/Usuario/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Usuario'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
