import service from '../../../../services/Modulo/Admin/Token/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Token'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
