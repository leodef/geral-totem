import service from '../../../../services/Modulo/Admin/Token/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Token'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
