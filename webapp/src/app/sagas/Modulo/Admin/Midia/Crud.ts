// eslint-disable-next-line no-unused-vars
import service from '../../../../services/Modulo/Admin/Midia/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Midia'
import { CrudSaga } from '../../../Crud'

export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
