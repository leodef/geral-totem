import service from '../../../../services/Modulo/Admin/Midia/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Midia'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
