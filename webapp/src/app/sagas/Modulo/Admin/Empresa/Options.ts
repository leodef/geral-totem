import service from '../../../../services/Modulo/Admin/Empresa/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Empresa'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
