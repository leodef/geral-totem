import service from '../../../../services/Modulo/Admin/Empresa/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Empresa'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
