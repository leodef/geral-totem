import service from '../../../../services/Modulo/Admin/UsuarioEmpresa/Options'
import { optionsType } from '../../../../types/Modulo/Admin/UsuarioEmpresa'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
