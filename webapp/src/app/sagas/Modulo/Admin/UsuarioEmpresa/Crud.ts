import service from '../../../../services/Modulo/Admin/UsuarioEmpresa/Crud'
import { crudType } from '../../../../types/Modulo/Admin/UsuarioEmpresa'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
