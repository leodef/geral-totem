import service from '../../../../services/Modulo/Admin/Receita/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Receita'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
