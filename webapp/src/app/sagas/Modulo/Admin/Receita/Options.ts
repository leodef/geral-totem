import service from '../../../../services/Modulo/Admin/Receita/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Receita'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
