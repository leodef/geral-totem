import service from '../../../../services/Modulo/Admin/Cliente/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Cliente'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
