import service from '../../../../services/Modulo/Admin/Cliente/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Cliente'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
