import service from '../../../../services/Modulo/Admin/Totem/Crud'
import { crudType } from '../../../../types/Modulo/Admin/Totem'
import { CrudSaga } from '../../../Crud'
export default new CrudSaga(
  crudType,
  service
).getSagasFunction()
