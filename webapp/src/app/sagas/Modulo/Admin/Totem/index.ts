import crudSaga from './Crud'
import optionsSaga from './Options'

const adminSagas = [
  crudSaga,
  optionsSaga
]

export default adminSagas
