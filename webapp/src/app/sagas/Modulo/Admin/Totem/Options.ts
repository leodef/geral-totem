import service from '../../../../services/Modulo/Admin/Totem/Options'
import { optionsType } from '../../../../types/Modulo/Admin/Totem'
import { OptionsSaga } from '../../../Options'
export default new OptionsSaga(
  optionsType,
  service
).getSagasFunction()
