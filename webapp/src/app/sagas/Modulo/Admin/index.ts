import clienteSaga from './Cliente'
import empresaSaga from './Empresa'
import midiaSaga from './Midia'
import receitaSaga from './Receita'
import tokenSaga from './Token'
import totemSaga from './Totem'
import usuarioSaga from './Usuario'
import usuarioEmpresaSaga from './UsuarioEmpresa'

const adminSagas = [
  ...clienteSaga,
  ...empresaSaga,
  ...midiaSaga,
  ...receitaSaga,
  ...tokenSaga,
  ...totemSaga,
  ...usuarioSaga,
  ...usuarioEmpresaSaga
]

export default adminSagas
