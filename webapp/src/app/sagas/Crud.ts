import { takeLatest, put, call, all, debounce } from 'redux-saga/effects' // select
// eslint-disable-next-line no-unused-vars
import { CrudType, ActionTypeEnum } from '../types/Crud'
// eslint-disable-next-line no-unused-vars
import { CrudService } from '../services/Crud'
import { MessageVariant, MessageType } from '../types/Message'
import { LoadingType } from '../types/Loading'

export class CrudSaga {
  config: CrudType
  service: CrudService
  params: any

  static getSagasFunction (
    config: CrudType,
    service: CrudService,
    params: any) {
    const obj = new CrudSaga(config, service, params)
    return obj.getSagasFunction()
  }

  static getSagas (
    config: CrudType,
    service: CrudService,
    params: any) {
    const obj = new CrudSaga(config, service, params)
    return obj.getSagas()
  }

  constructor (
    config: CrudType,
    service: CrudService,
    params: any = null) {
    this.config = config
    this.service = service
    this.params = params
  }

  public getSagasFunction () {
    return this.getSagas.bind(this)
  }

  public * getSagas () {
    const context = this
    const config = context.config
    yield all([
      debounce(500, config.FETCH_ITEM, context.fetch.bind(context)),
      takeLatest(config.FIND_ITEM, context.find.bind(context)),
      takeLatest(config.CREATE_ITEM, context.create.bind(context)),
      takeLatest(config.DELETE_ITEM, context.remove.bind(context)),
      takeLatest(config.UPDATE_ITEM, context.update.bind(context)),
      takeLatest([
        // SUCCESS
        config.FIND_ITEM_SUCCESS,
        config.FETCH_ITEM_SUCCESS,
        config.CREATE_ITEM_SUCCESS,
        config.DELETE_ITEM_SUCCESS,
        config.UPDATE_ITEM_SUCCESS,
        // FAILURE
        config.FIND_ITEM_FAILURE,
        config.FETCH_ITEM_FAILURE,
        config.CREATE_ITEM_FAILURE,
        config.DELETE_ITEM_FAILURE,
        config.UPDATE_ITEM_FAILURE
      ], context.msg.bind(context))
    ])
  }

  public getItem (action: any) {
    const { item } = action.payload
    return item
  }

  public * msg (action: any) {
    const context = this
    const config = context.config
    let variant = MessageVariant.primary
    let msg = null
    switch (action.type) {
      case config.CREATE_ITEM_SUCCESS:
        msg = 'Criado com sucesso'
        variant = MessageVariant.success
        break
      case config.DELETE_ITEM_SUCCESS:
        msg = 'Deletado com sucesso'
        variant = MessageVariant.success
        break
      case config.UPDATE_ITEM_SUCCESS:
        msg = 'Atualizado com sucesso'
        variant = MessageVariant.success
        break
      case config.FIND_ITEM_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case config.FIND_ITEM_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
      case config.FETCH_ITEM_SUCCESS:
        msg = 'Carregado com sucesso'
        variant = MessageVariant.success
        break
      // FAILURE
      case config.FETCH_ITEM_FAILURE:
        msg = 'Erro ao carregar'
        variant = MessageVariant.danger
        break
      case config.CREATE_ITEM_FAILURE:
        msg = 'Erro ao criar'
        variant = MessageVariant.danger
        break
      case config.DELETE_ITEM_FAILURE:
        msg = 'Erro ao deletar'
        variant = MessageVariant.danger
        break
      case config.UPDATE_ITEM_FAILURE:
        msg = 'Erro ao atualizar'
        variant = MessageVariant.danger
        break
    }
    // chamar servico de mensagem
    const payload = { message: msg, variant }
    yield put({ type: MessageType.SHOW_MESSAGE, payload })
    yield call(console.log, msg)
  }

  public * create (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'create' })
    yield put({ type: config.CREATE_ITEM_PENDING })
    try {
      const { parent } = action.payload
      const item = context.getItem(action)
      const create = yield call(service.create.bind(service), item, parent)
      yield put({ type: config.CREATE_ITEM_SUCCESS, payload: { item: create } })
      yield put({ type: config.SET_ACTION_ITEM, payload: { action: ActionTypeEnum.LIST, item: null } })
    } catch (error) {
      yield put({ type: config.CREATE_ITEM_FAILURE, payload: { error } })
      console.error(error) // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'create' })
    }
  }

  public * remove (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'remove' })
    yield put({ type: config.DELETE_ITEM_PENDING })
    try {
      const { parent } = action.payload
      const item = context.getItem(action)
      yield call(service.remove.bind(service), item, parent)
      // if (count !== 1) throw new Error('API delete request failed');
      yield put({ type: config.DELETE_ITEM_SUCCESS })
      yield put({ type: config.SET_ACTION_ITEM, payload: { action: ActionTypeEnum.LIST, item: null } })
    } catch (error) {
      yield put({ type: config.DELETE_ITEM_FAILURE, payload: { error } })
      console.error(error) // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'remove' })
    }
  }

  public * update (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'update' })
    yield put({ type: config.UPDATE_ITEM_PENDING })
    try {
      const { parent } = action.payload
      const item = context.getItem(action)
      const updatedPost = yield call(service.update.bind(service), item, parent)
      yield put({ type: config.UPDATE_ITEM_SUCCESS, payload: { item: updatedPost } })
      yield put({ type: config.SET_ACTION_ITEM, payload: { action: ActionTypeEnum.LIST, item: null } })
    } catch (error) {
      yield put({ type: config.UPDATE_ITEM_FAILURE, payload: { error } })
      console.error(error) // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'update' })
    }
  }

  public * find (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'find' })
    yield put({ type: config.FIND_ITEM_PENDING })
    try {
      const { parent } = action.payload
      const item = context.getItem(action)
      const findResult = yield call(service.find.bind(service), item, parent)
      yield put({ type: config.FIND_ITEM_SUCCESS, payload: { item: findResult } })
    } catch (error) {
      yield put({ type: config.FIND_ITEM_FAILURE, payload: { error } })
      console.error(error) // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'find' })
    }
  }

  public * fetch (action: any) {
    const context = this
    const config = context.config
    const service = context.service
    yield put({ type: LoadingType.START_LOADING, payload: 'fetch' })
    yield put({ type: config.FETCH_ITEM_PENDING })
    try {
      const postsFromApi = yield call(
        service.fetch.bind(service), action.payload)
      yield put({
        type: config.FETCH_ITEM_SUCCESS,
        payload: postsFromApi
      })
    } catch (error) {
      yield put({ type: config.FETCH_ITEM_FAILURE, payload: { items: [], error } })
      console.error(error); // eslint-disable-line
    } finally {
      yield put({ type: LoadingType.STOP_LOADING, payload: 'fetch' })
    }
  }
}
