// import { all } from 'redux-saga/effects';
import moduloSaga from './Modulo'
import authSaga from './Auth'
import messageSaga from './Message'

const rootSaga = [
  ...moduloSaga,
  authSaga,
  messageSaga
]
export default rootSaga
